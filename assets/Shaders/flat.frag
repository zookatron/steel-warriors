#version 330 core

out vec4 Color;

in vec2 TexCoord;

uniform sampler2D Texture;

void main() {
	Color = vec4(0, 0, 0, texture(Texture, TexCoord).a);
}