#version 330 core

in vec3 VertexPosition;
in vec2 VertexTexCoord;
in vec3 VertexNormal;

out vec2 TexCoord;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

void main() {
	gl_Position = ViewMatrix * ProjectionMatrix * ModelMatrix * vec4(VertexPosition, 1);
	TexCoord = VertexTexCoord;
}

