#version 330 core

out vec4 Color;

in vec2 TexCoord;
in vec3 NormalOut;
in float Alpha;
in float Depth;

uniform int Level;

uniform sampler2D Texture;
uniform sampler2D Texture1;
uniform sampler2D Texture2;



void main() {
	vec3 SunVec = normalize(vec3(1.0, 5.0, 1.0));
	Color = mix(texture(Texture2, TexCoord*500.0).rgba, texture(Texture1, TexCoord*500.0).rgba, Alpha)*dot(NormalOut, SunVec); //vec4(vec3(Alpha), 1);
	Color = mix(vec4(vec3(135, 206, 235)/255.0, 1.0f), Color, Depth);
	Color.a = 1.0;
}