// Vertex Shader - file "base.vert"

#version 150 core

in vec3 VertexPosition;
in vec2 VertexTexCoord;
in vec3 VertexNormal;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

out vec3 worldSpacePosition;
out float Distance;
out vec2 TexCoord;

void main(void)
{
	vec4 Temp = ViewMatrix * ProjectionMatrix * ModelMatrix * vec4(VertexPosition, 1);
	gl_Position = Temp;
	worldSpacePosition = VertexPosition;//(ModelMatrix * vec4(VertexPosition, 1)).xyz;
	Distance = Temp.z;
	TexCoord = VertexTexCoord;
}


