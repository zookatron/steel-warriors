#version 330 core

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

uniform mat4 ModelMatrix;
uniform mat4 TextureMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

void main() {
	gl_Position = ModelMatrix * vec4(VertexPosition, 1);
	gl_Position.y *= -1;
	TexCoord = (TextureMatrix * vec4(VertexTexCoord, 1, 1)).xy;
}
