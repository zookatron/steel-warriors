#version 330 core

in vec3 worldSpacePosition;
in float Distance;

uniform vec3 cameraPosition;
uniform vec3 sunDirection;

out vec4 Color;

const float sunAngularDiameterCos = 0.999956676946448443553574619906976478926848692873900859324f;

void main() {
	vec3 ViewDirection = normalize(worldSpacePosition);
	float CosSunAngle = dot(normalize(ViewDirection), normalize(sunDirection));
	float zenithAngle = max(0, normalize(worldSpacePosition).y);

	vec3 Sky = vec3(49, 71, 110)/255.0;
	vec3 LightBlue = vec3(135, 206, 235)/255.0;
	float SunHeight = normalize(sunDirection).y;

	vec3 MieInscatter = vec3(CosSunAngle > 0 ? pow(CosSunAngle, 20)*0.5 : 0);
	vec3 RayleighInscatter = mix(Sky, LightBlue, pow(1-zenithAngle, 10));

	Color = vec4(RayleighInscatter+MieInscatter, 1);
	if(CosSunAngle > sunAngularDiameterCos) { Color = vec4(1); }
}