#version 330 core

out vec4 Color;

in vec2 TexCoord;

uniform sampler2D Texture;

void main() {
	Color = texture(Texture, TexCoord).rgba;
}