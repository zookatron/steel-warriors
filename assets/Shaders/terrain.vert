#version 330 core

in vec3 VertexPosition;
in vec2 VertexTexCoord;
in vec3 VertexNormal;

out vec2 TexCoord;
out vec3 NormalOut;
out float Alpha;
out float Depth;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform int Level;
uniform vec2 OneOverTextureWidth;
uniform vec2 HalfPixel;
uniform vec2 ViewerPos;
uniform vec2 AlphaOffset;
uniform vec2 OneOverWidth;
uniform float HeightScaleFactor;

uniform sampler2D Texture;
uniform sampler2D Texture3;

const float GridSize = 1.0;

float get_slope(vec3 Normal) {
	float Start = 0.7;
	float End = 0.8;
	return (clamp(Normal.y, Start, End) - Start) * (1.0/(End-Start));
}

/*vec3 get_normal(vec2 MyTexCoord, float MyAlpha) {
	float MyLevel = Level;
	vec2 MyHalfPixel = (HalfPixel*exp2(float(MyLevel)));
	vec2 MyHalfPixel2 = (HalfPixel*exp2(float(MyLevel+1)));

	float Move = GridSize * exp2(float(MyLevel)) * OneOverTextureWidth;
	float ParentMove = GridSize * exp2(float(MyLevel+1)) * OneOverTextureWidth;

	float Height1 = textureLod(Texture, MyTexCoord + MyHalfPixel+vec2(0, -Move), MyLevel) * HeightScaleFactor;
	float ParentHeight1 = textureLod(Texture, MyTexCoord + MyHalfPixel2+vec2(0, -ParentMove), MyLevel+1) * HeightScaleFactor;

	float Height2 = textureLod(Texture, MyTexCoord + MyHalfPixel+vec2(-Move, 0), MyLevel) * HeightScaleFactor;
	float ParentHeight2 = textureLod(Texture, MyTexCoord + MyHalfPixel2+vec2(-ParentMove, 0), MyLevel+1) * HeightScaleFactor;

	float Height3 = textureLod(Texture, MyTexCoord + MyHalfPixel+vec2(Move, 0), MyLevel) * HeightScaleFactor;
	float ParentHeight3 = textureLod(Texture, MyTexCoord + MyHalfPixel2+vec2(ParentMove, 0), MyLevel+1) * HeightScaleFactor;

	float Height4 = textureLod(Texture, MyTexCoord + MyHalfPixel+vec2(0, Move), MyLevel) * HeightScaleFactor;
	float ParentHeight4 = textureLod(Texture, MyTexCoord + MyHalfPixel2+vec2(0, ParentMove), MyLevel+1) * HeightScaleFactor;

	vec3 Normal;
	Normal.z = Height1 - Height4;
	Normal.x = Height2 - Height3;
	Normal.y = 2*GridSize*exp2(float(MyLevel));

	vec3 ParentNormal;
	ParentNormal.z = ParentHeight1 - ParentHeight4;
	ParentNormal.x = ParentHeight2 - ParentHeight3;
	ParentNormal.y = 2*GridSize*exp2(float(MyLevel+1));

	return mix(normalize(Normal), normalize(ParentNormal), MyAlpha);
}*/

vec3 normalate(vec4 input) {
	return ((input-vec4(0.5, 0.5, 0.5, 0))*2.0).rgb;
}

void main() {
	float LevelPower = exp2(float(Level));

	vec2 WorldVertexPosition = (ModelMatrix * vec4(VertexPosition, 1)).xz;
	TexCoord = WorldVertexPosition * OneOverTextureWidth;

	float Height = textureLod(Texture, TexCoord + (HalfPixel*LevelPower), Level).x;
	float ParentHeight = textureLod(Texture, TexCoord + (HalfPixel*exp2(float(Level+1))), Level+1).x;

	vec3 Normal = normalate(textureLod(Texture3, TexCoord + (HalfPixel*LevelPower), Level));
	vec3 ParentNormal = normalate(textureLod(Texture3, TexCoord + (HalfPixel*exp2(float(Level+1))), Level+1));

	vec2 AlphaVec = clamp((abs(WorldVertexPosition - ViewerPos) / LevelPower - AlphaOffset) * OneOverWidth, 0, 1);
	float MyAlpha = max(AlphaVec.x, AlphaVec.y);

	vec4 Temp = ViewMatrix * ProjectionMatrix * vec4(WorldVertexPosition.x, mix(Height, ParentHeight, MyAlpha) * HeightScaleFactor, WorldVertexPosition.y, 1);
	gl_Position = Temp;
	//vec3 Normaly = get_normal(TexCoord, MyAlpha);
	vec3 Normaly = mix(Normal, ParentNormal, MyAlpha);
	Alpha = get_slope(Normaly);
	NormalOut = Normaly;
	Depth = 1.0f-Temp.z/100000.0f;
}



/*
float Distance = WorldVertexPosition.z;

vec3 SunDirection;
vec3 SunIrradiance;
float one_over_pi = 0.3183;
vec3 Eccentricity;
vec3 RayleighCoefficient;
vec3 MieCoefficient;

vec3 ViewDirection = normalize(eyePos.xyz - worldPos.xyz);
vec3 RayleighMieCoefficient = RayleighCoefficient+MieCoefficient;

float CosSunAngle = dot(ViewDirection, SunDirection);
float RayleighScattering = 0.1875*one_over_pi*RayleighCoefficient*(1.0+CosSunAngle*CosSunAngle);
float MieScattering = 0.25*one_over_pi*MieCoefficient*((1.0-Eccentricity)*(1.0-Eccentricity))/pow(1.0+Eccentricity*Eccentricity-2.0*Eccentricity*CosSunAngle, 1.5);

float Extinction = exp(-RayleighMieCoefficient*Distance);
vec4 InScattering = (RayleighScattering+MieScattering)/RayleighMieCoefficient * SunIrradiance* (1 - Extinction);

vec4 InColor;
vec4 OutColor = InColor * Extinction + InScattering;
*/