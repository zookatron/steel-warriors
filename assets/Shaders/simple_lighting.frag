#version 330 core

out vec4 Color;

in vec2 TexCoord;
in vec3 NormalOut;

uniform sampler2D Texture;

void main() {
	vec3 SunVec = normalize(vec3(1.0, 5.0, 1.0));
	Color = texture(Texture, TexCoord).rgba*dot(NormalOut, SunVec);
	Color.a = 1.0;
}