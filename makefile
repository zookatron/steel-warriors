
# paths
BUILD=./build/
SOURCE=./src/

# flags
COMPILEFLAGS=-target x86_64-pc-windows-gnu -c -g -Wall -Werror
LINKFLAGS=-target x86_64-pc-windows-gnu -g
LIBS=-lgdi32 -lopengl32 -lwsock32 -lwinmm

# binaries
COMPILER=clang
LINKER=clang

# files
SOURCES=$(shell find $(SOURCE) -name '*.c')
SOURCES_NONVENDOR=$(shell find $(SOURCE) -name '*.c' -not -path "$(SOURCE)vendor*")
OBJECTS=$(patsubst $(SOURCE)%.c,$(BUILD)%.o,$(SOURCES))
EXECUTABLE=$(BUILD)program.exe

# rules
all: build
	$(EXECUTABLE)

build: $(SOURCES) $(EXECUTABLE)

cleanup:
	rm -rf $(BUILD)

clean: cleanup build

format:
	clang-format -style=file -i $(SOURCES)
	# Needed to fix bug in clang 3.8
	find src -name '*.TMP' | xargs rm

tidy: $(BUILD)compile_commands.json
	# Script shim needed until we can get config file working
	clang-tidy.sh $(SOURCES_NONVENDOR)

$(BUILD)compile_commands.json:
	gen-compile-commands.sh $(SOURCES_NONVENDOR) > $(BUILD)compile_commands.json

$(EXECUTABLE): $(OBJECTS)
	$(LINKER) $(LINKFLAGS) $(OBJECTS) $(LIBS) -o $@

$(BUILD)%.o: $(SOURCE)%.c
	mkdir -p $(dir $@)
	$(COMPILER) $(COMPILEFLAGS) $< -o $@
