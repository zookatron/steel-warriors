cwd=`pwd`
dir=`path -w ${cwd}`
files=( "$@" )
len=${#files[@]}

echo "["
for i in "${!files[@]}"
do
	f=${files[i]}
	full_f="${cwd}/${f}"
	full_f_w=`path -w ${full_f}`

	echo "{"
	echo "\"directory\": \"${dir}\","
	echo "\"command\": \"clang -target x86_64-pc-windows-gnu -c -g -Wall -Werror ${f} -o ${f}.o\","
	echo "\"file\": \"${full_f_w}\""
	echo "}"

	if [ $i != $((${len}-1)) ]
	then
		echo ","
	fi
done
echo "]"
