#include "TTerrain.h"
#include "math.h"

void TTerrain_Initialize(TTerrain* Terrain, TRenderer* Renderer) {
	Terrain->GridWidth = 127;
	Terrain->GridSize = 1;
	Terrain->MinRingLevel = 1;
	Terrain->MaxRingLevel = 10;

	TTextureID TextureID = TRenderer_Load_Heightmap_Texture(Renderer, "assets/Textures/heightmap.png");  // TRenderer_Create_HeightmapTest_Texture(Renderer);
	TTextureID NormalTextureID = TRenderer_Create_Heightmap_NormalMap_Texture(Renderer, TextureID);
	TShaderID TShaderID = TRenderer_Load_Shader(Renderer, "assets/Shaders/terrain.vert", "assets/Shaders/terrain.frag");
	TGeometryID GeometryID = TRenderer_Create_Plane_Geometry(Renderer, Terrain->GridWidth, Terrain->GridWidth, Terrain->GridSize);
	TMaterialID MaterialID = TRenderer_Create_Material(Renderer, TShaderID, TextureID);
	TMeshID MeshID = TRenderer_Create_Mesh(Renderer, GeometryID, MaterialID);
	Terrain->MaterialID = MaterialID;
	Terrain->Grid = MeshID;

	// TODO: Destroy
	TextureID = TRenderer_Load_Texture(Renderer, "assets/Textures/grass.png");
	TRenderer_Set_Material_Uniform(Renderer, MaterialID, "Texture1", &TextureID);
	TextureID = TRenderer_Load_Texture(Renderer, "assets/Textures/stone.png");
	TRenderer_Set_Material_Uniform(Renderer, MaterialID, "Texture2", &TextureID);

	TRenderer_Set_Material_Uniform(Renderer, MaterialID, "Texture", &TextureID);
	TRenderer_Set_Material_Uniform(Renderer, MaterialID, "Texture3", &NormalTextureID);

	TGeometryID RingFixupPart1, RingFixupPart2, RingFixupPart3, RingFixupPart4;
	TTransform Transform;

	RingFixupPart1 = TRenderer_Create_Plane_Geometry(Renderer, Terrain->GridWidth, 2, Terrain->GridSize);
	RingFixupPart3 = TRenderer_Copy_Geometry(Renderer, RingFixupPart1);
	RingFixupPart2 = TRenderer_Create_Plane_Geometry(Renderer, 2, Terrain->GridWidth, Terrain->GridSize);
	RingFixupPart4 = TRenderer_Copy_Geometry(Renderer, RingFixupPart2);

	TTransform_Identity(&Transform);
	TTransform_Translate_X(&Transform, (Terrain->GridWidth + 2) * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, RingFixupPart1, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Translate_Z(&Transform, (Terrain->GridWidth + 2) * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, RingFixupPart2, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Translate_X(&Transform, -2 * Terrain->GridWidth * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, RingFixupPart3, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Translate_Z(&Transform, -2 * Terrain->GridWidth * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, RingFixupPart4, &Transform);

	TRenderer_Add_Geometry(Renderer, RingFixupPart1, RingFixupPart2);
	TRenderer_Add_Geometry(Renderer, RingFixupPart1, RingFixupPart3);
	TRenderer_Add_Geometry(Renderer, RingFixupPart1, RingFixupPart4);

	TRenderer_Release_Geometry(Renderer, RingFixupPart2);
	TRenderer_Release_Geometry(Renderer, RingFixupPart3);
	TRenderer_Release_Geometry(Renderer, RingFixupPart4);

	Terrain->RingFixup = TRenderer_Create_Mesh(Renderer, RingFixupPart1, MaterialID);

	TGeometryID InnerFixupPart1, InnerFixupPart2;

	InnerFixupPart1 = TRenderer_Create_Plane_Geometry(Renderer, Terrain->GridWidth * 2 + 1, 1, Terrain->GridSize);
	InnerFixupPart2 = TRenderer_Create_Plane_Geometry(Renderer, 1, Terrain->GridWidth * 2, Terrain->GridSize);

	TTransform_Identity(&Transform);
	TTransform_Translate_X(&Transform, -Terrain->GridWidth * Terrain->GridSize);
	TTransform_Translate_Z(&Transform, Terrain->GridWidth * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, InnerFixupPart1, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Translate_X(&Transform, Terrain->GridWidth * Terrain->GridSize);
	TTransform_Translate_Z(&Transform, -Terrain->GridWidth * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, InnerFixupPart2, &Transform);

	TRenderer_Add_Geometry(Renderer, InnerFixupPart1, InnerFixupPart2);
	TRenderer_Release_Geometry(Renderer, InnerFixupPart2);

	Terrain->InnerFixup = TRenderer_Create_Mesh(Renderer, InnerFixupPart1, MaterialID);

	Terrain->RingLXPiece = TRenderer_Create_Mesh(Renderer, TRenderer_Create_Plane_Geometry(Renderer, Terrain->GridWidth * 2 + 2, 1, Terrain->GridSize), MaterialID);

	Terrain->RingLYPiece = TRenderer_Create_Mesh(Renderer, TRenderer_Create_Plane_Geometry(Renderer, 1, Terrain->GridWidth * 2 + 1, Terrain->GridSize), MaterialID);

	TArray Vertices, Indices;
	TArray_Initialize(&Vertices, sizeof(TVector3D));
	TArray_Initialize(&Indices, sizeof(unsigned short));

	int X;
	TVector3D TempVertex;
	int I1, I2, I3;

	for (X = -(Terrain->GridWidth + 1) * 2 + 2; X <= (Terrain->GridWidth + 1) * 2; X++) {
		TVector3D_Set(&TempVertex, X * Terrain->GridSize, 0, -(Terrain->GridWidth) * Terrain->GridSize * 2);
		TArray_Push(&Vertices, &TempVertex);
	}

	for (X = 0; X < ((Terrain->GridWidth + 1) * 4) - 3; X += 2) {
		I1 = X;
		I2 = X + 1;
		I3 = X + 2;

		TArray_Push(&Indices, &I1);
		TArray_Push(&Indices, &I2);
		TArray_Push(&Indices, &I3);
	}

	TGeometryID DegenerateTris = TRenderer_Create_Geometry(Renderer, &Indices, &Vertices, 0, 0);
	TGeometryID DegenerateTrisPart2, DegenerateTrisPart3, DegenerateTrisPart4;
	DegenerateTrisPart2 = TRenderer_Copy_Geometry(Renderer, DegenerateTris);
	DegenerateTrisPart3 = TRenderer_Copy_Geometry(Renderer, DegenerateTris);
	DegenerateTrisPart4 = TRenderer_Copy_Geometry(Renderer, DegenerateTris);

	TTransform_Identity(&Transform);
	TTransform_Translate_Z(&Transform, ((Terrain->GridWidth * 4.0) + 2.0) * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, DegenerateTrisPart2, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Rotate_Y(&Transform, M_PI_2);
	TTransform_Translate_Z(&Transform, 2.0 * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, DegenerateTrisPart3, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Rotate_Y(&Transform, M_PI_2);
	TTransform_Translate_Z(&Transform, 2.0 * Terrain->GridSize);
	TTransform_Translate_X(&Transform, ((Terrain->GridWidth * 4.0) + 2.0) * Terrain->GridSize);
	TRenderer_Transform_Geometry(Renderer, DegenerateTrisPart4, &Transform);

	TRenderer_Add_Geometry(Renderer, DegenerateTris, DegenerateTrisPart2);
	TRenderer_Add_Geometry(Renderer, DegenerateTris, DegenerateTrisPart3);
	TRenderer_Add_Geometry(Renderer, DegenerateTris, DegenerateTrisPart4);
	TRenderer_Release_Geometry(Renderer, DegenerateTrisPart2);
	TRenderer_Release_Geometry(Renderer, DegenerateTrisPart3);
	TRenderer_Release_Geometry(Renderer, DegenerateTrisPart4);

	Terrain->DegenerateTris = TRenderer_Create_Mesh(Renderer, DegenerateTris, MaterialID);

	Terrain->SkySphereMaterialID = TRenderer_Create_Material(Renderer, TRenderer_Load_Shader(Renderer, "assets/Shaders/sky.vert", "assets/Shaders/sky.frag"), 0);
	TGeometryID SkySphereGeometryID = TRenderer_Create_Sphere_Geometry(Renderer, 1000.0, 100, 100, 0, 0);
	TRenderer_Geometry_Flip_Normals(Renderer, SkySphereGeometryID);
	Terrain->SkySphere = TRenderer_Create_Mesh(Renderer, SkySphereGeometryID, Terrain->SkySphereMaterialID);
}

void TTerrain_Render(TTerrain* Terrain, TRenderer* Renderer) {
	TTransform Transform;

	float ViewX, ViewY, Height;
	TVector3D TempVector;
	TTransform_Get_Translation(&Renderer->Camera.InverseViewMatrix, &TempVector);
	ViewX = TempVector.X;
	ViewY = TempVector.Z;
	Height = TempVector.Y;

	TTransform_Identity(&Transform);
	TTransform_Set_Translation(&Transform, &TempVector);

	TRenderer_DepthMask(false);
	TRenderer_Render_Mesh(Renderer, Terrain->SkySphere, &Transform);
	TRenderer_DepthMask(true);

	float Dist = (4 * Terrain->GridWidth + 3) * Terrain->GridSize;
	int HeightTimes = 0;

	if (Height / Dist > 1) {
		int HeightMult = Height / Dist;
		HeightMult--;
		HeightMult |= HeightMult >> 1;
		HeightMult |= HeightMult >> 2;
		HeightMult |= HeightMult >> 4;
		HeightMult |= HeightMult >> 8;
		HeightMult |= HeightMult >> 16;
		HeightMult++;
		for (; HeightMult > 1; HeightMult /= 2) {
			HeightTimes++;
		}
	}

	int Start, End;
	if (true) {
		Start = pow(2, Terrain->MinRingLevel);
		End = pow(2, Terrain->MaxRingLevel);
	} else {
		Start = pow(2, Terrain->MinRingLevel + HeightTimes);
		End = pow(2, Terrain->MaxRingLevel + HeightTimes);
	}

	float HeightScaleFactor = 400.0f;
	TRenderer_Set_Material_Uniform(Renderer, Terrain->MaterialID, "HeightScaleFactor", &HeightScaleFactor);

	TVector2D FineBlockOrigXY, FineBlockOrigZW, ViewerPos, AlphaOffset, OneOverWidth;
	TVector2D_Initialize(&FineBlockOrigXY);
	TVector2D_Initialize(&FineBlockOrigZW);
	TVector2D_Initialize(&ViewerPos);
	TVector2D_Initialize(&AlphaOffset);
	TVector2D_Initialize(&OneOverWidth);

	float TextureWidth = 2048.0f;

	TVector2D_Set(&FineBlockOrigXY, 1.0f / (TextureWidth * 2.0f), 1.0f / (TextureWidth * 2.0f));
	TRenderer_Set_Material_Uniform(Renderer, Terrain->MaterialID, "HalfPixel", &FineBlockOrigXY);

	TVector2D_Set(&FineBlockOrigZW, 1.0f / (TextureWidth * 2.0f), 1.0f / (TextureWidth * 2.0f));
	TRenderer_Set_Material_Uniform(Renderer, Terrain->MaterialID, "OneOverTextureWidth", &FineBlockOrigZW);

	TVector2D_Set(&AlphaOffset, ((Terrain->GridWidth + 1.0) * 2.0f), ((Terrain->GridWidth + 1.0) * 2.0f));
	TRenderer_Set_Material_Uniform(Renderer, Terrain->MaterialID, "AlphaOffset", &AlphaOffset);

	TVector2D_Set(&OneOverWidth, 1.0f / (Terrain->GridWidth * 2.0f), 1.0f / (Terrain->GridWidth * 2.0f));
	TRenderer_Set_Material_Uniform(Renderer, Terrain->MaterialID, "OneOverWidth", &OneOverWidth);

	TVector2D_Set(&ViewerPos, ViewX, ViewY);
	TRenderer_Set_Material_Uniform(Renderer, Terrain->MaterialID, "ViewerPos", &ViewerPos);

	int X, Y, Level, Ring, PrevOffX, PrevOffY, OffX, OffY, Dblsquaresize, Squaresize;
	for (Level = Start, Ring = 0; Level < End; Level *= 2, Ring++) {
		Squaresize = Terrain->GridSize * Level;
		PrevOffX = floor(ViewX / Squaresize) * Squaresize;
		PrevOffY = floor(ViewY / Squaresize) * Squaresize;

		Dblsquaresize = Terrain->GridSize * Level * 2;
		OffX = floor(ViewX / Dblsquaresize) * Dblsquaresize;
		OffY = floor(ViewY / Dblsquaresize) * Dblsquaresize;

		TRenderer_Set_Material_Uniform(Renderer, Terrain->MaterialID, "Level", &Ring);

		if (Level == Start) {
			PrevOffX = OffX;
			PrevOffY = OffY;

			for (X = 0; X < 2; X++) {
				for (Y = 0; Y < 2; Y++) {
					TTransform_Identity(&Transform);
					TTransform_Scale_Uniform(&Transform, Level);
					TTransform_Translate_X(&Transform, OffX - 1.0f * Terrain->GridSize * Terrain->GridWidth * Level + Terrain->GridSize * (Terrain->GridWidth) * Level * X);
					TTransform_Translate_Z(&Transform, OffY - 1.0f * Terrain->GridSize * Terrain->GridWidth * Level + Terrain->GridSize * (Terrain->GridWidth) * Level * Y);
					TRenderer_Render_Mesh(Renderer, Terrain->Grid, &Transform);
				}
			}
			TTransform_Identity(&Transform);
			TTransform_Scale_Uniform(&Transform, Level);
			TTransform_Translate_X(&Transform, OffX);
			TTransform_Translate_Z(&Transform, OffY);
			TRenderer_Render_Mesh(Renderer, Terrain->InnerFixup, &Transform);
		}

		TTransform_Identity(&Transform);
		TTransform_Scale_Uniform(&Transform, Level);
		TTransform_Translate_X(&Transform, OffX);
		TTransform_Translate_Z(&Transform, OffY);
		TRenderer_Render_Mesh(Renderer, Terrain->RingFixup, &Transform);
		TRenderer_Render_Mesh(Renderer, Terrain->DegenerateTris, &Transform);

		TTransform_Identity(&Transform);
		TTransform_Scale_Uniform(&Transform, Level);
		TTransform_Translate_X(&Transform, OffX + (-Terrain->GridWidth * Terrain->GridSize * Level));
		TTransform_Translate_Z(&Transform, OffY + ((OffY == PrevOffY ? (Terrain->GridWidth + 1) : (-Terrain->GridWidth)) * Terrain->GridSize * Level));
		TRenderer_Render_Mesh(Renderer, Terrain->RingLXPiece, &Transform);

		TTransform_Identity(&Transform);
		TTransform_Scale_Uniform(&Transform, Level);
		TTransform_Translate_X(&Transform, OffX + ((OffX == PrevOffX ? (Terrain->GridWidth + 1) : (-Terrain->GridWidth)) * Terrain->GridSize * Level));
		TTransform_Translate_Z(&Transform, OffY + ((OffY == PrevOffY ? (-Terrain->GridWidth) : (-Terrain->GridWidth + 1)) * Terrain->GridSize * Level));
		TRenderer_Render_Mesh(Renderer, Terrain->RingLYPiece, &Transform);

		for (X = -2; X < 2; X++) {
			for (Y = -2; Y < 2; Y++) {
				if (X != 1 && X != -2 && Y != 1 && Y != -2) {
					continue;
				}
				TTransform_Identity(&Transform);
				TTransform_Scale_Uniform(&Transform, Level);
				TTransform_Translate_X(&Transform, OffX + Level * Terrain->GridSize * (X * Terrain->GridWidth + (X > -1 ? 2.0f : 0.0f)));
				TTransform_Translate_Z(&Transform, OffY + Level * Terrain->GridSize * (Y * Terrain->GridWidth + (Y > -1 ? 2.0f : 0.0f)));
				TRenderer_Render_Mesh(Renderer, Terrain->Grid, &Transform);
			}
		}
	}

	static float Tod = 1.0f;
	// tod += 0.001f;

	TRenderer_Set_Material_Uniform(Renderer, Terrain->SkySphereMaterialID, "cameraPosition", &TempVector);
	TTransform_Identity(&Transform);
	TTransform_Rotate_Z(&Transform, Tod);
	TVector3D_Set(&TempVector, 1.0, 5.0, 1.0);
	TVector3D_Transform(&TempVector, &Transform);
	TVector3D_Normalize(&TempVector);
	TRenderer_Set_Material_Uniform(Renderer, Terrain->SkySphereMaterialID, "sunDirection", &TempVector);
}

void TTerrain_Destroy(TTerrain* Terrain, TRenderer* Renderer) {
	TRenderer_Release_Mesh(Renderer, Terrain->Grid);
	TRenderer_Release_Mesh(Renderer, Terrain->InnerFixup);
	TRenderer_Release_Mesh(Renderer, Terrain->RingFixup);
	TRenderer_Release_Mesh(Renderer, Terrain->RingLXPiece);
	TRenderer_Release_Mesh(Renderer, Terrain->RingLYPiece);
}
