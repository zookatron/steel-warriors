#include "Hashing.h"

TStringHash HashString(const char* String) {
	unsigned int Hash;
	for (Hash = 5381; *String; String++) {
		Hash = ((Hash << 5) + Hash) + *String;
	}
	return Hash;
}