#include "TRenderer.h"

#include "vendor/glew/glew.h"
#include "vendor/glew/glew_w.h"
#include "vendor/lodepng/lodepng.h"
#include "math.h"

#include <io.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>

#include "Hashing.h"
#include "TArray.h"
#include "TBuffer.h"
#include "TTransform.h"
#include "TVector2D.h"
#include "TVector3D.h"
#include "debug.h"

//----------------------------------------
// Structures
//----------------------------------------

typedef struct {
	TStringHash Name;
	GLint ShaderLocation;
	TRenderDataType DataType;
	GLuint Buffer;
} TInstanceBuffer;

typedef struct {
	GLuint VertexBuffer;
	GLuint TexCoordBuffer;
	GLuint NormalBuffer;
	GLuint IndexBuffer;
	size_t NumIndices;
	uint16_t Refrences;
} TGeometry;

typedef struct {
	GLuint TextureID;
	GLuint FrameBufferID;
	uint32_t Width;
	uint32_t Height;
	uint16_t Refrences;
} TTexture;

typedef struct {
	GLuint ShaderProgram;
	GLint ModelMatrixLocation;
	GLint ViewMatrixLocation;
	GLint ProjectionMatrixLocation;
	GLint VertexPositionLocation;
	GLint VertexTexCoordLocation;
	GLint VertexNormalLocation;
	GLint TextureLocation;
	uint16_t Refrences;
} TShader;

typedef union {
	int Integer;
	float Float;
	TVector2D Vector2D;
	TVector3D Vector3D;
	GLuint TextureID;
} TRenderData;

typedef struct {
	TStringHash Name;
	TRenderDataType DataType;
	TRenderData Data;
	GLint ShaderLocation;
	uint8_t TextureLocation;
} TUniformData;

typedef struct {
	TShaderID ShaderID;
	TTextureID TextureID;
	TArray Uniforms;
	TCulling Culling;
	uint16_t Refrences;
} TMaterial;

typedef struct {
	TMaterialID MaterialID;
	TGeometryID GeometryID;
	TArray InstanceBuffers;
	uint32_t NumInstances;
	uint32_t MaxNumInstances;
	uint16_t Refrences;
} TMesh;

//----------------------------------------
// Helper Functions
//----------------------------------------

void TRenderer_InitializeFont(TRenderer* Renderer, const char* FontFamily, unsigned int FontSize);
TTextureID TRenderer_Create_Texture_Raw(TRenderer* Renderer, GLuint TextureID, uint32_t Width, uint32_t Height);

TMesh* TRenderer_Get_Mesh(TRenderer* Renderer, TMeshID MeshID);
TGeometry* TRenderer_Get_Geometry(TRenderer* Renderer, TGeometryID GeometryID);
TTexture* TRenderer_Get_Texture(TRenderer* Renderer, TTextureID TextureID);
TShader* TRenderer_Get_Shader(TRenderer* Renderer, TShaderID ShaderID);
TMaterial* TRenderer_Get_Material(TRenderer* Renderer, TMaterialID MaterialID);

GLuint LoadShaderProgram(const char* VertexShaderPath, const char* FragmentShaderPath) {
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	TBuffer VertexShaderCode;
	TBuffer_Initialize(&VertexShaderCode);
	TBuffer_ReadFile(&VertexShaderCode, VertexShaderPath);

	TBuffer FragmentShaderCode;
	TBuffer_Initialize(&FragmentShaderCode);
	TBuffer_ReadFile(&FragmentShaderCode, FragmentShaderPath);

	TBuffer ErrorMessage;
	TBuffer_Initialize(&ErrorMessage);

	GLint Result = GL_FALSE;
	GLint InfoLogLength;

	// Compile Vertex Shader
	char const* VertexSourcePointer = VertexShaderCode.Data;
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (!Result && InfoLogLength > 0) {
		TBuffer_Reserve(&ErrorMessage, (size_t)InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, ErrorMessage.Data);
		printf("Error processing vertex shader: %s\n", VertexShaderPath);
		printf("Log: %s\n", (char*)ErrorMessage.Data);
	}
	assert(Result);

	// Compile Fragment Shader
	char const* FragmentSourcePointer = FragmentShaderCode.Data;
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (!Result && InfoLogLength > 0) {
		TBuffer_Reserve(&ErrorMessage, (size_t)InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, ErrorMessage.Data);
		printf("Error processing fragment shader: %s\n", FragmentShaderPath);
		printf("Log: %s\n", (char*)ErrorMessage.Data);
	}
	assert(Result);

	// Link the program
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (!Result && InfoLogLength > 0) {
		TBuffer_Reserve(&ErrorMessage, (size_t)InfoLogLength + 1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, ErrorMessage.Data);
		printf("Error compiling program: %s, %s\n", VertexShaderPath, FragmentShaderPath);
		printf("Log: %s\n", (char*)ErrorMessage.Data);
	}
	assert(Result);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	TBuffer_Destroy(&ErrorMessage);
	TBuffer_Destroy(&VertexShaderCode);
	TBuffer_Destroy(&FragmentShaderCode);

	return ProgramID;
}

GLuint TRenderer_Create_MipmapTest_Texture(TRenderer* Renderer) {
	GLuint TextureID;

	const size_t MAX_SIZE = 64;
	unsigned char ImageData[MAX_SIZE * MAX_SIZE * 4];

	glGenTextures(1, &TextureID);
	glBindTexture(GL_TEXTURE_2D, TextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	int Size, Level, Index, Color;
	for (Size = MAX_SIZE, Color = Level = 0; Size > 0; Size /= 2, Level++) {
		if (++Color > 2) {
			Color = 0;
		}
		for (Index = 0; Index < MAX_SIZE * MAX_SIZE; Index++) {
			ImageData[Index * 4 + 0] = Color == 0 ? 255 : 0;
			ImageData[Index * 4 + 1] = Color == 1 ? 255 : 0;
			ImageData[Index * 4 + 2] = Color == 2 ? 255 : 0;
			ImageData[Index * 4 + 3] = 255;
		}
		glTexImage2D(GL_TEXTURE_2D, Level, GL_RGBA8, Size, Size, 0, GL_RGBA, GL_UNSIGNED_BYTE, ImageData);
	}

	return TRenderer_Create_Texture_Raw(Renderer, TextureID, MAX_SIZE, MAX_SIZE);
}

GLuint TRenderer_Create_HeightmapTest_Texture(TRenderer* Renderer) {
	GLuint TextureID;

	const size_t MAX_SIZE = 64;
	unsigned char ImageData[MAX_SIZE * MAX_SIZE * 4];

	glGenTextures(1, &TextureID);
	glBindTexture(GL_TEXTURE_2D, TextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	int Size, Level, Index;
	for (Index = 0; Index < MAX_SIZE * MAX_SIZE; Index++) {
		ImageData[Index * 4 + 0] = Index % 2 ? 255 : 0;
		ImageData[Index * 4 + 1] = 0;
		ImageData[Index * 4 + 2] = 0;
		ImageData[Index * 4 + 3] = 255;
	}
	for (Size = MAX_SIZE, Level = 0; Size > 0; Size /= 2, Level++) {
		glTexImage2D(GL_TEXTURE_2D, Level, GL_RGBA8, Size, Size, 0, GL_RGBA, GL_UNSIGNED_BYTE, ImageData);
	}

	return TRenderer_Create_Texture_Raw(Renderer, TextureID, MAX_SIZE, MAX_SIZE);
}

GLuint LoadTexture(const char* TexturePath, uint32_t* Width, uint32_t* Height) {
	unsigned char* ImageData;
	if (_access(TexturePath, 0) == -1) {
		printf("Texture not found: %s\n", TexturePath);
		assert(false);
	}
	assert(!lodepng_decode32_file(&ImageData, Width, Height, TexturePath));
	GLuint TextureID;

	glGenTextures(1, &TextureID);
	glBindTexture(GL_TEXTURE_2D, TextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, *Width, *Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, ImageData);
	glGenerateMipmap(GL_TEXTURE_2D);
	free(ImageData);

	return TextureID;
}

void HilburtCurve(int SideLength, int Position, int* X, int* Y) {
	int StepX, StepY, Step;
	*X = *Y = 0;
	for (Step = 1; Step < SideLength; Step *= 2) {
		StepX = 1 & (Position / 2);
		StepY = 1 & (Position ^ StepX);
		if (StepY == 0) {
			if (StepX == 1) {
				*X = Step - 1 - *X;
				*Y = Step - 1 - *Y;
			}

			int Temp = *X;
			*X = *Y;
			*Y = Temp;
		}
		*X += Step * StepX;
		*Y += Step * StepY;
		Position /= 4;
	}
}

//----------------------------------------
// TCamera Functions
//----------------------------------------

void TCamera_Initialize(TCamera* Camera) {
	TCamera_SetPerspectiveProj(Camera, 35.0f, 1.2f, 1.0f, 100000.0f);
	TTransform_Identity(&Camera->ViewMatrix);
	TTransform_Identity(&Camera->InverseViewMatrix);
}

void TCamera_Copy(TCamera* Dest, TCamera* Src) {
	memcpy(Dest, Src, sizeof(TCamera));
}

void TCamera_UpdateView(TCamera* Camera, TTransform* InverseViewMatrix) {
	TTransform_Copy(&Camera->InverseViewMatrix, InverseViewMatrix);
	TTransform_Copy(&Camera->ViewMatrix, InverseViewMatrix);
	TTransform_Inverse(&Camera->ViewMatrix);
}

void TCamera_LookAt(TCamera* Camera, TVector3D* From, TVector3D* To) {
	TTransform Transform;
	TTransform_PointAt(&Transform, From, To);
	TCamera_UpdateView(Camera, &Transform);
}

void TCamera_LookAtSphere(TCamera* Camera, TVector3D* From, TVector3D* To, float Radius) {
	TCamera_LookAt(Camera, From, To);

	TVector3D Difference;
	TVector3D_Copy(&Difference, To);
	TVector3D_Subtract(&Difference, From);

	float Dist = sqrt(Difference.Z * Difference.Z + Difference.X * Difference.X + Difference.Y * Difference.Y);

	float Theta = asin(Radius / Dist) / M_PI * 180.0f;

	TCamera_SetPerspectiveProj(Camera, Theta, 1.0f, 1.0f, 100000.0f);
}

void TCamera_SetPerspectiveProj(TCamera* Camera, float FOV, float Aspect, float ZNear, float ZFar) {
	float YMax = ZNear * tan(FOV * M_PI / 180);
	float YMin = -YMax;
	float XMax = YMax * Aspect;
	float XMin = YMin * Aspect;

	float Width = XMax - XMin;
	float Height = YMax - YMin;

	float Depth = ZFar - ZNear;

	float* Matrix = (float*)Camera->ProjectionMatrix.Matrix;

	Matrix[0] = (2 * ZNear / Width) / Aspect;
	Matrix[1] = 0;
	Matrix[2] = 0;
	Matrix[3] = 0;

	Matrix[4] = 0;
	Matrix[5] = 2 * ZNear / Height;
	Matrix[6] = 0;
	Matrix[7] = 0;

	Matrix[8] = 0;
	Matrix[9] = 0;
	Matrix[10] = -(ZFar + ZNear) / Depth;
	Matrix[11] = -1;

	Matrix[12] = 0;
	Matrix[13] = 0;
	Matrix[14] = -2 * (ZFar * ZNear) / Depth;
	Matrix[15] = 0;
}

void TCamera_SetOrthographicProj(TCamera* Camera, float Left, float Right, float Bottom, float Top, float ZNear, float ZFar) {
	float Width = Right - Left;
	float Height = Top - Bottom;
	float Depth = ZFar - ZNear;

	float* Matrix = (float*)Camera->ProjectionMatrix.Matrix;

	Matrix[0] = 2.0f / Width;
	Matrix[1] = 0;
	Matrix[2] = 0;
	Matrix[3] = 0;

	Matrix[4] = 0;
	Matrix[5] = 2.0f / Height;
	Matrix[6] = 0;
	Matrix[7] = 0;

	Matrix[8] = 0;
	Matrix[9] = 0;
	Matrix[10] = 2.0f / Depth;
	Matrix[11] = 0;

	Matrix[12] = -(Right + Left) / Width;
	Matrix[13] = -(Top + Bottom) / Height;
	Matrix[14] = -(ZFar + ZNear) / Depth;
	Matrix[15] = 1;
}

//----------------------------------------
// TRenderer Functions
//----------------------------------------

void TRenderer_Initialize_TextureMesh(TRenderer* Renderer) {
	TShaderID ShaderID = TRenderer_Load_Shader(Renderer, "assets/Shaders/2D.vert", "assets/Shaders/solid.frag");
	TMaterialID MaterialID = TRenderer_Create_Material(Renderer, ShaderID, Renderer->DefaultTextureID);
	TRenderer_Set_Material_Culling(Renderer, MaterialID, Culling_None);

	TArray Vertices, TexCoords, Indices;
	TVector3D Vector3D;
	TVector2D Vector2D;
	unsigned short Index;
	TArray_Initialize(&Vertices, sizeof(TVector3D));
	TArray_Initialize(&TexCoords, sizeof(TVector2D));
	TArray_Initialize(&Indices, sizeof(unsigned short));

	TVector3D_Set(&Vector3D, 0, 0, 0);
	TArray_Push(&Vertices, &Vector3D);
	TVector3D_Set(&Vector3D, 1, 0, 0);
	TArray_Push(&Vertices, &Vector3D);
	TVector3D_Set(&Vector3D, 0, 1, 0);
	TArray_Push(&Vertices, &Vector3D);
	TVector3D_Set(&Vector3D, 1, 1, 0);
	TArray_Push(&Vertices, &Vector3D);

	TVector2D_Set(&Vector2D, 0, 0);
	TArray_Push(&TexCoords, &Vector2D);
	TVector2D_Set(&Vector2D, 1, 0);
	TArray_Push(&TexCoords, &Vector2D);
	TVector2D_Set(&Vector2D, 0, 1);
	TArray_Push(&TexCoords, &Vector2D);
	TVector2D_Set(&Vector2D, 1, 1);

	Index = 0;
	TArray_Push(&Indices, &Index);
	Index = 1;
	TArray_Push(&Indices, &Index);
	Index = 2;
	TArray_Push(&Indices, &Index);
	Index = 2;
	TArray_Push(&Indices, &Index);
	Index = 1;
	TArray_Push(&Indices, &Index);
	Index = 3;
	TArray_Push(&Indices, &Index);

	TGeometryID GeometryID = TRenderer_Create_Geometry(Renderer, &Indices, &Vertices, &TexCoords, 0);
	Renderer->TextureMeshID = TRenderer_Create_Mesh(Renderer, GeometryID, MaterialID);
}

void TRenderer_Initialize(TRenderer* Renderer, TWindow* Window) {
	bool Success;
	HWND WindowHandle = Window->WindowHandle;
	HDC DeviceContext;
	HGLRC RenderContext;

	PIXELFORMATDESCRIPTOR PixelFormatDescriptor;
	memset(&PixelFormatDescriptor, 0, sizeof(PixelFormatDescriptor));
	PixelFormatDescriptor.nSize = sizeof(PixelFormatDescriptor);
	PixelFormatDescriptor.nVersion = 1;
	PixelFormatDescriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	PixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
	PixelFormatDescriptor.cColorBits = 24;
	PixelFormatDescriptor.cDepthBits = 16;
	PixelFormatDescriptor.iLayerType = PFD_MAIN_PLANE;

	DeviceContext = GetDC(WindowHandle);
	assert(DeviceContext);  // Unable to create a OpenGL device context.

	GLuint PixelFormat;
	PixelFormat = ChoosePixelFormat(DeviceContext, &PixelFormatDescriptor);
	assert(PixelFormat);  // Cannot find suitable pixel format.

	Success = SetPixelFormat(DeviceContext, PixelFormat, &PixelFormatDescriptor);
	assert(Success);  // Unable to set proper pixel format.

	RenderContext = wglCreateContext(DeviceContext);
	assert(RenderContext);  // Unable to create a OpenGL rendering context.

	Success = wglMakeCurrent(DeviceContext, RenderContext);
	assert(Success);  // Unable to activate the OpenGL rendering context.

	RECT ScreenRect;
	GetClientRect(WindowHandle, &ScreenRect);
	Renderer->WindowWidth = ScreenRect.right - ScreenRect.left;
	Renderer->WindowHeight = ScreenRect.bottom - ScreenRect.top;
	glViewport(0, 0, (GLsizei)(ScreenRect.right - ScreenRect.left), (GLsizei)(ScreenRect.bottom - ScreenRect.top));

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_TEXTURE_2D);
	// TODO: Fix degenerates
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLint err = glGetError();
	assert(err == GL_NO_ERROR);

	err = glewInit();
	assert(err == GLEW_OK);

	wglSwapIntervalEXT(0);

	Renderer->Window = WindowHandle;
	Renderer->DeviceContext = DeviceContext;
	Renderer->RenderContext = RenderContext;
	TArray_Initialize(&Renderer->Meshes, sizeof(TMesh));
	TArray_Initialize(&Renderer->Geometries, sizeof(TGeometry));
	TArray_Initialize(&Renderer->Materials, sizeof(TMaterial));
	TArray_Initialize(&Renderer->Textures, sizeof(TTexture));
	TArray_Initialize(&Renderer->Shaders, sizeof(TShader));

	Renderer->CurrentShaderID = Renderer->DefaultShaderID = TRenderer_Load_Shader(Renderer, "assets/Shaders/simple.vert", "assets/Shaders/simple_lighting.frag");
	Renderer->CurrentTextureID = Renderer->DefaultTextureID = TRenderer_Load_Texture(Renderer, "assets/Textures/awesome.png");
	Renderer->CurrentMaterialID = Renderer->DefaultMaterialID = TRenderer_Create_Material(Renderer, Renderer->DefaultShaderID, Renderer->DefaultTextureID);
	Renderer->CurrentCulling = Culling_Back;
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	Renderer->CurrentRenderTarget = 0;

	TRenderer_Initialize_TextureMesh(Renderer);

	TCamera_Initialize(&Renderer->Camera);

	TRenderer_InitializeFont(Renderer, "Calibri", 40);

	err = glGetError();
	assert(err == GL_NO_ERROR);
}

void TRenderer_Destroy(TRenderer* Renderer) {
	TArray_Destroy(&Renderer->Meshes);
	TArray_Destroy(&Renderer->Geometries);
	TArray_Destroy(&Renderer->Materials);

	TRenderer_Release_Shader(Renderer, Renderer->DefaultShaderID);

	wglMakeCurrent(0, 0);
	wglDeleteContext(Renderer->RenderContext);
	ReleaseDC(Renderer->Window, Renderer->DeviceContext);
}

void TRenderer_SwapBuffers(TRenderer* Renderer) {
	SwapBuffers(Renderer->DeviceContext);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLint err = glGetError();
	if (err != GL_NO_ERROR) {
		assert(err == GL_NO_ERROR);
	}
}

void TRenderer_UpdateCamera(TRenderer* Renderer, TCamera* Camera) {
	TCamera_Copy(&Renderer->Camera, Camera);
}

void TRenderer_GetCamera(TRenderer* Renderer, TCamera* Camera) {
	memcpy(Camera, &Renderer->Camera, sizeof(TCamera));
}

// TODO: Move to material
void TRenderer_DepthMask(bool Active) {
	glDepthMask(Active);
}

void TRenderer_Set_Render_Target(TRenderer* Renderer, TTextureID TextureID) {
	if (Renderer->CurrentRenderTarget == TextureID) {
		return;
	}
	Renderer->CurrentRenderTarget = TextureID;

	if (TextureID == 0) {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, Renderer->WindowWidth, Renderer->WindowHeight);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		return;
	}

	TTexture* Texture = TRenderer_Get_Texture(Renderer, TextureID);

	if (!Texture->FrameBufferID) {
		glGenFramebuffers(1, &Texture->FrameBufferID);
		glBindFramebuffer(GL_FRAMEBUFFER, Texture->FrameBufferID);

		// The depth buffer
		GLuint DepthRenderBuffer;
		glGenRenderbuffers(1, &DepthRenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, DepthRenderBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, Texture->Width, Texture->Height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, DepthRenderBuffer);

		// Set "renderedTexture" as our colour attachement #0
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, Texture->TextureID, 0);

		// Set the list of draw buffers.
		GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
		glDrawBuffers(1, DrawBuffers);

		// Always check that our framebuffer is ok
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
			assert(false);
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, Texture->FrameBufferID);
	glViewport(0, 0, Texture->Width, Texture->Height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
}

//----------------------------------------
// TMesh Functions
//----------------------------------------

TMeshID TRenderer_Create_Mesh(TRenderer* Renderer, TGeometryID GeometryID, TMaterialID MaterialID) {
	TMesh Mesh;
	Mesh.GeometryID = GeometryID;
	Mesh.MaterialID = MaterialID;
	TArray_Initialize(&Mesh.InstanceBuffers, sizeof(TInstanceBuffer));
	Mesh.NumInstances = 0;
	Mesh.MaxNumInstances = 0;
	Mesh.Refrences = 1;
	TArray_Push(&Renderer->Meshes, &Mesh);
	return Renderer->Meshes.Length;
}

TMeshID TRenderer_Copy_Mesh(TRenderer* Renderer, TMeshID MeshID) {
	TMesh* Mesh = TRenderer_Get_Mesh(Renderer, MeshID);
	TArray_Push(&Renderer->Meshes, Mesh);
	return Renderer->Meshes.Length;
}

TMesh* TRenderer_Get_Mesh(TRenderer* Renderer, TMeshID MeshID) {
	return TArray_Get(&Renderer->Meshes, MeshID - 1);
}

// TODO: Optimize Model Indices/Remove Duplicate Verticies
TMeshID TRenderer_Load_Mesh(TRenderer* Renderer, const char* MeshPath) {
	TMaterialID MaterialID = Renderer->DefaultMaterialID;

	FILE* File;
	File = fopen(MeshPath, "r");
	if (!File) {
		printf("Model not found: %s\n", MeshPath);
		assert(false);
	}

	TArray Vertices, TexCoords, Normals, Indices;
	TArray_Initialize(&Vertices, sizeof(TVector3D));
	TArray_Initialize(&TexCoords, sizeof(TVector2D));
	TArray_Initialize(&Normals, sizeof(TVector3D));
	TArray_Initialize(&Indices, sizeof(short) * 3);

	const size_t MAX_LINE_LENGTH = 128;
	int Consumed;
	char CurrentLine[MAX_LINE_LENGTH];
	memset(CurrentLine, 0, sizeof(CurrentLine));

	while (fgets(CurrentLine, MAX_LINE_LENGTH, File)) {
		if (strncmp(CurrentLine, "mtllib", 6) == 0) {
			char NameBuffer[MAX_LINE_LENGTH];
			Consumed = sscanf(CurrentLine, "mtllib %128s", (char*)&NameBuffer);
			assert(Consumed == 1);
			MaterialID = TRenderer_Load_Material(Renderer, NameBuffer);
		} else if (strncmp(CurrentLine, "usemtl", 6) == 0) {
			char NameBuffer[MAX_LINE_LENGTH];
			Consumed = sscanf(CurrentLine, "usemtl %128s", (char*)&NameBuffer);
			assert(Consumed == 1);
		} else if (strncmp(CurrentLine, "vt", 2) == 0) {
			TVector2D TexCoord;
			char FloatBuffer1[MAX_LINE_LENGTH];
			char FloatBuffer2[MAX_LINE_LENGTH];
			Consumed = sscanf(CurrentLine, "vt %128s %128s", FloatBuffer1, FloatBuffer2);
			assert(Consumed == 2);
			TexCoord.X = strtof(FloatBuffer1, 0);
			assert(errno == 0);
			TexCoord.Y = strtof(FloatBuffer2, 0);
			assert(errno == 0);
			TArray_Push(&TexCoords, &TexCoord);
		} else if (strncmp(CurrentLine, "vn", 2) == 0) {
			TVector3D Normal;
			char FloatBuffer1[MAX_LINE_LENGTH];
			char FloatBuffer2[MAX_LINE_LENGTH];
			char FloatBuffer3[MAX_LINE_LENGTH];
			Consumed = sscanf(CurrentLine, "vn %128s %128s %128s", FloatBuffer1, FloatBuffer2, FloatBuffer3);
			assert(Consumed == 3);
			Normal.X = strtof(FloatBuffer1, 0);
			assert(errno == 0);
			Normal.Y = strtof(FloatBuffer2, 0);
			assert(errno == 0);
			Normal.Z = strtof(FloatBuffer3, 0);
			assert(errno == 0);
			TArray_Push(&Normals, &Normal);
		} else if (CurrentLine[0] == 'v') {
			TVector3D Vertex;
			char FloatBuffer1[MAX_LINE_LENGTH];
			char FloatBuffer2[MAX_LINE_LENGTH];
			char FloatBuffer3[MAX_LINE_LENGTH];
			Consumed = sscanf(CurrentLine, "v %128s %128s %128s", FloatBuffer1, FloatBuffer2, FloatBuffer3);
			assert(Consumed == 3);
			Vertex.X = strtof(FloatBuffer1, 0);
			assert(errno == 0);
			Vertex.Y = strtof(FloatBuffer2, 0);
			assert(errno == 0);
			Vertex.Z = strtof(FloatBuffer3, 0);
			assert(errno == 0);
			TArray_Push(&Vertices, &Vertex);
		} else if (CurrentLine[0] == 'f') {
			short IndexCache[3][3];
			memset(IndexCache, 0, sizeof(short) * 9);
			char Character = 0;
			uint8_t CurrentIndex = 0;
			uint8_t IndexPos = 0;
			uint8_t Position = 0;
			uint8_t NumberStart = 0;

			// skip the preceding "f" and spaces
			for (; !(CurrentLine[Position] >= 48 && CurrentLine[Position] <= 57); Position++, NumberStart++) {
				;
			}

			// TODO: Clean this crap up
			while (true) {
				Character = CurrentLine[Position];
				if (Character == '/' || Character == ' ' || Character == '\n') {
					assert(NumberStart != Position);
					IndexCache[CurrentIndex][IndexPos] = strtol(CurrentLine + NumberStart, 0, 10);
					assert(errno == 0);
					NumberStart = Position + 1;
					IndexPos++;
					assert(IndexPos < 4);

					if (Character == ' ' || Character == '\n') {
						if (CurrentIndex < 2) {
							CurrentIndex++;
						} else {
							TArray_Push(&Indices, IndexCache[0]);
							TArray_Push(&Indices, IndexCache[1]);
							TArray_Push(&Indices, IndexCache[2]);
							if (Character == '\n') {
								break;
							}
							memcpy(IndexCache[1], IndexCache[2], sizeof(short) * 3);  // triangle fan
						}
						NumberStart = Position + 1;
						IndexPos = 0;
					}
				} else {
					assert(Character >= 48 && Character <= 57);
				}
				Position++;
			}
		}
	}

	fclose(File);

	// MultiIndex_to_UniIndex
	TArray* VertexAttributes[3] = {&Vertices, &TexCoords, &Normals};
	unsigned short NumAttributes = 3;

	TArray NewIndices;
	TArray IndexedAttributes[NumAttributes];
	unsigned short Index, Attr;

	TArray_Initialize(&NewIndices, sizeof(unsigned short));
	for (Attr = 0; Attr < NumAttributes; Attr++) {
		TArray_Initialize(&IndexedAttributes[Attr], VertexAttributes[Attr]->ElementSize);
	}

	for (Index = 0; Index < Indices.Length; Index++) {
		short* CurrentIndices = (short*)TArray_Get(&Indices, Index);

		TArray_Push(&NewIndices, &Index);
		for (Attr = 0; Attr < NumAttributes; Attr++) {
			TArray_Push(&IndexedAttributes[Attr], TArray_Get(VertexAttributes[Attr], CurrentIndices[Attr] - 1));
		}
	}

	TArray_Copy(&Indices, &NewIndices);
	for (Attr = 0; Attr < NumAttributes; Attr++) {
		TArray_Copy(VertexAttributes[Attr], &IndexedAttributes[Attr]);
		TArray_Destroy(&IndexedAttributes[Attr]);
	}

	TGeometryID GeometryID = TRenderer_Create_Geometry(Renderer, &Indices, &Vertices, &TexCoords, &Normals);

	TArray_Destroy(&Vertices);
	TArray_Destroy(&TexCoords);
	TArray_Destroy(&Normals);
	TArray_Destroy(&Indices);

	return TRenderer_Create_Mesh(Renderer, GeometryID, MaterialID);
}

void TRenderer_Render_Mesh(TRenderer* Renderer, TMeshID MeshID, TTransform* Transform) {
	TMesh* Mesh = TRenderer_Get_Mesh(Renderer, MeshID);
	TGeometry* Geometry = TRenderer_Get_Geometry(Renderer, Mesh->GeometryID);
	TMaterial* Material = TRenderer_Get_Material(Renderer, Mesh->MaterialID);
	TShader* Shader = TRenderer_Get_Shader(Renderer, Material->ShaderID);

	TRenderer_Activate_Material(Renderer, Mesh->MaterialID);

	glUniformMatrix4fv(Shader->ModelMatrixLocation, 1, GL_FALSE, (float*)Transform);
	glUniformMatrix4fv(Shader->ViewMatrixLocation, 1, GL_FALSE, (float*)&Renderer->Camera.ProjectionMatrix);
	glUniformMatrix4fv(Shader->ProjectionMatrixLocation, 1, GL_FALSE, (float*)&Renderer->Camera.ViewMatrix);

	glEnableVertexAttribArray(Shader->VertexPositionLocation);
	glBindBuffer(GL_ARRAY_BUFFER, Geometry->VertexBuffer);
	glVertexAttribPointer(Shader->VertexPositionLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

	if (Geometry->TexCoordBuffer && Shader->VertexTexCoordLocation != -1) {
		glEnableVertexAttribArray(Shader->VertexTexCoordLocation);
		glBindBuffer(GL_ARRAY_BUFFER, Geometry->TexCoordBuffer);
		glVertexAttribPointer(Shader->VertexTexCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}

	if (Geometry->NormalBuffer && Shader->VertexNormalLocation != -1) {
		glEnableVertexAttribArray(Shader->VertexNormalLocation);
		glBindBuffer(GL_ARRAY_BUFFER, Geometry->NormalBuffer);
		glVertexAttribPointer(Shader->VertexNormalLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}

	if (Mesh->NumInstances) {
		int Index;
		for (Index = 0; Index < Mesh->InstanceBuffers.Length; Index++) {
			TInstanceBuffer* InstanceBuffer = TArray_Get(&Mesh->InstanceBuffers, Index);

			glEnableVertexAttribArray(InstanceBuffer->ShaderLocation);
			glBindBuffer(GL_ARRAY_BUFFER, InstanceBuffer->Buffer);
			glVertexAttribDivisor(InstanceBuffer->ShaderLocation, 1);
			switch (InstanceBuffer->DataType) {
				case RenderDataType_Float:
					glVertexAttribPointer(InstanceBuffer->ShaderLocation, 1, GL_FLOAT, GL_FALSE, 0, 0);
					break;
				case RenderDataType_Vector2D:
					glVertexAttribPointer(InstanceBuffer->ShaderLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
					break;
				case RenderDataType_Vector3D:
					glVertexAttribPointer(InstanceBuffer->ShaderLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
					break;
				default:
					assert(false);
			}
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Geometry->IndexBuffer);
		glDrawElementsInstanced(GL_TRIANGLES, Geometry->NumIndices, GL_UNSIGNED_SHORT, 0, Mesh->NumInstances);
	} else {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Geometry->IndexBuffer);
		glDrawElements(GL_TRIANGLES, Geometry->NumIndices, GL_UNSIGNED_SHORT, 0);
	}
}

void TRenderer_Update_Mesh_Instanced_Data(TRenderer* Renderer, TMeshID MeshID, char* Name, TRenderDataType DataType, void* Data, uint32_t NumInstances) {
	TMesh* Mesh = TRenderer_Get_Mesh(Renderer, MeshID);
	int Index;
	size_t DataSize;

	if (NumInstances > Mesh->MaxNumInstances) {
		Mesh->MaxNumInstances = NumInstances;
	}
	Mesh->NumInstances = NumInstances;

	switch (DataType) {
		case RenderDataType_Float:
			DataSize = sizeof(float);
			break;
		case RenderDataType_Vector2D:
			DataSize = sizeof(TVector2D);
			break;
		case RenderDataType_Vector3D:
			DataSize = sizeof(TVector3D);
			break;
		default:
			DataSize = 0;
			assert(false);
	}

	TStringHash NameHash = HashString(Name);
	TInstanceBuffer* InstanceBuffer = 0;

	for (Index = 0; Index < Mesh->InstanceBuffers.Length; Index++) {
		TInstanceBuffer* TempInstanceBuffer = TArray_Get(&Mesh->InstanceBuffers, Index);
		if (TempInstanceBuffer->Name == NameHash) {
			InstanceBuffer = TempInstanceBuffer;
			break;
		}
	}

	if (InstanceBuffer == 0) {
		TInstanceBuffer NewInstanceBuffer;

		glGenBuffers(1, &NewInstanceBuffer.Buffer);
		glBindBuffer(GL_ARRAY_BUFFER, NewInstanceBuffer.Buffer);
		glBufferData(GL_ARRAY_BUFFER, Mesh->MaxNumInstances * DataSize, NULL, GL_STREAM_DRAW);

		NewInstanceBuffer.DataType = DataType;
		NewInstanceBuffer.Name = NameHash;

		TMaterial* Material = TRenderer_Get_Material(Renderer, Mesh->MaterialID);
		TShader* Shader = TRenderer_Get_Shader(Renderer, Material->ShaderID);
		NewInstanceBuffer.ShaderLocation = glGetAttribLocation(Shader->ShaderProgram, Name);
		assert(NewInstanceBuffer.ShaderLocation != -1);

		TArray_Push(&Mesh->InstanceBuffers, &NewInstanceBuffer);
		InstanceBuffer = TArray_Get(&Mesh->InstanceBuffers, Mesh->InstanceBuffers.Length - 1);
	}

	glBindBuffer(GL_ARRAY_BUFFER, InstanceBuffer->Buffer);
	glBufferData(GL_ARRAY_BUFFER, Mesh->MaxNumInstances * DataSize, NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, NumInstances * DataSize, Data);
}

void TRenderer_Release_Mesh(TRenderer* Renderer, TMeshID MeshID) {
	TMesh* Mesh = TRenderer_Get_Mesh(Renderer, MeshID);
	if (Mesh->Refrences > 0) {
		return;
	}

	TRenderer_Release_Geometry(Renderer, Mesh->GeometryID);
	TRenderer_Release_Material(Renderer, Mesh->MaterialID);

	int Index;
	for (Index = 0; Index < Mesh->InstanceBuffers.Length; Index++) {
		TInstanceBuffer* InstanceBuffer = TArray_Get(&Mesh->InstanceBuffers, Index);
		glDeleteBuffers(1, &InstanceBuffer->Buffer);
	}
}

//----------------------------------------
// TGeometry Functions
//----------------------------------------

TGeometryID TRenderer_Create_Geometry(TRenderer* Renderer, TArray* Indices, TArray* Vertices, TArray* TexCoords, TArray* Normals) {
	TGeometry Geometry;
	Geometry.IndexBuffer = Geometry.VertexBuffer = Geometry.TexCoordBuffer = Geometry.NormalBuffer = 0;
	Geometry.Refrences = 1;
	TArray_Push(&Renderer->Geometries, &Geometry);
	TGeometryID GeometryID = Renderer->Geometries.Length;
	TRenderer_Update_Geometry_Data(Renderer, GeometryID, Indices, Vertices, TexCoords, Normals);
	return GeometryID;
}

void TRenderer_Release_Geometry(TRenderer* Renderer, TGeometryID GeometryID) {
	TGeometry* Geometry = TRenderer_Get_Geometry(Renderer, GeometryID);
	if (Geometry->Refrences > 0) {
		return;
	}

	if (Geometry->IndexBuffer) {
		glDeleteBuffers(1, &Geometry->IndexBuffer);
	}
	if (Geometry->VertexBuffer) {
		glDeleteBuffers(1, &Geometry->VertexBuffer);
	}
	if (Geometry->TexCoordBuffer) {
		glDeleteBuffers(1, &Geometry->TexCoordBuffer);
	}
	if (Geometry->NormalBuffer) {
		glDeleteBuffers(1, &Geometry->NormalBuffer);
	}
}

TGeometry* TRenderer_Get_Geometry(TRenderer* Renderer, TGeometryID GeometryID) {
	return TArray_Get(&Renderer->Geometries, GeometryID - 1);
}

void TRenderer_Update_Geometry_Data(TRenderer* Renderer, TGeometryID GeometryID, TArray* Indices, TArray* Vertices, TArray* TexCoords, TArray* Normals) {
	TGeometry* Geometry = TRenderer_Get_Geometry(Renderer, GeometryID);

	assert(Indices);
	if (!Geometry->IndexBuffer) {
		glGenBuffers(1, &Geometry->IndexBuffer);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Geometry->IndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, Indices->Length * Indices->ElementSize, Indices->Data, GL_STATIC_DRAW);
	Geometry->NumIndices = Indices->Length;

	assert(Vertices);
	if (!Geometry->VertexBuffer) {
		glGenBuffers(1, &Geometry->VertexBuffer);
	}
	glBindBuffer(GL_ARRAY_BUFFER, Geometry->VertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, Vertices->Length * Vertices->ElementSize, Vertices->Data, GL_STATIC_DRAW);

	if (TexCoords && TexCoords->Length) {
		if (!Geometry->TexCoordBuffer) {
			glGenBuffers(1, &Geometry->TexCoordBuffer);
		}
		glBindBuffer(GL_ARRAY_BUFFER, Geometry->TexCoordBuffer);
		glBufferData(GL_ARRAY_BUFFER, TexCoords->Length * TexCoords->ElementSize, TexCoords->Data, GL_STATIC_DRAW);
	}

	if (Normals && Normals->Length) {
		if (!Geometry->NormalBuffer) {
			glGenBuffers(1, &Geometry->NormalBuffer);
		}
		glBindBuffer(GL_ARRAY_BUFFER, Geometry->NormalBuffer);
		glBufferData(GL_ARRAY_BUFFER, Normals->Length * Normals->ElementSize, Normals->Data, GL_STATIC_DRAW);
	}
}

void TRenderer_Get_Geometry_Data(TRenderer* Renderer, TGeometryID GeometryID, TArray* Indices, TArray* Vertices, TArray* TexCoords, TArray* Normals) {
	TGeometry* Geometry = TRenderer_Get_Geometry(Renderer, GeometryID);

	TArray_Initialize(Indices, sizeof(unsigned short));
	TArray_Initialize(Vertices, sizeof(TVector3D));
	TArray_Initialize(TexCoords, sizeof(TVector2D));
	TArray_Initialize(Normals, sizeof(TVector3D));
	int BufferSize = 0;

	glBindBuffer(GL_ARRAY_BUFFER, Geometry->IndexBuffer);
	glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &BufferSize);
	TVector3D* RawIndices = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
	TArray_CopyData(Indices, RawIndices, (BufferSize / sizeof(unsigned short)));
	glUnmapBuffer(GL_ARRAY_BUFFER);

	glBindBuffer(GL_ARRAY_BUFFER, Geometry->VertexBuffer);
	glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &BufferSize);
	TVector3D* RawVertices = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
	TArray_CopyData(Vertices, RawVertices, (BufferSize / sizeof(TVector3D)));
	glUnmapBuffer(GL_ARRAY_BUFFER);

	if (Geometry->TexCoordBuffer) {
		glBindBuffer(GL_ARRAY_BUFFER, Geometry->TexCoordBuffer);
		glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &BufferSize);
		TVector2D* RawTexCoords = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
		TArray_CopyData(TexCoords, RawTexCoords, (BufferSize / sizeof(TVector2D)));
		glUnmapBuffer(GL_ARRAY_BUFFER);
	}

	if (Geometry->NormalBuffer) {
		glBindBuffer(GL_ARRAY_BUFFER, Geometry->NormalBuffer);
		glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &BufferSize);
		TVector3D* RawNormals = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
		TArray_CopyData(Normals, RawNormals, (BufferSize / sizeof(TVector3D)));
		glUnmapBuffer(GL_ARRAY_BUFFER);
	}
}

TGeometryID TRenderer_Copy_Geometry(TRenderer* Renderer, TGeometryID GeometryID) {
	TArray Indices, Vertices, TexCoords, Normals;
	TRenderer_Get_Geometry_Data(Renderer, GeometryID, &Indices, &Vertices, &TexCoords, &Normals);
	TGeometryID NewGeometry = TRenderer_Create_Geometry(Renderer, &Indices, &Vertices, &TexCoords, &Normals);
	TArray_Destroy(&Indices);
	TArray_Destroy(&Vertices);
	TArray_Destroy(&TexCoords);
	TArray_Destroy(&Normals);
	return NewGeometry;
}

void TRenderer_Add_Geometry(TRenderer* Renderer, TGeometryID SrcGeometryID, TGeometryID DestGeometryID) {
	TArray Indices1, Vertices1, TexCoords1, Normals1, Indices2, Vertices2, TexCoords2, Normals2;
	TRenderer_Get_Geometry_Data(Renderer, DestGeometryID, &Indices1, &Vertices1, &TexCoords1, &Normals1);
	TRenderer_Get_Geometry_Data(Renderer, SrcGeometryID, &Indices2, &Vertices2, &TexCoords2, &Normals2);

	int Index;
	for (Index = 0; Index < Indices2.Length; Index++) {
		*(unsigned short*)TArray_Get(&Indices2, Index) += Vertices1.Length;
	}

	TArray_Append(&Indices1, &Indices2);
	TArray_Append(&Vertices1, &Vertices2);
	TArray_Append(&TexCoords1, &TexCoords2);
	TArray_Append(&Normals1, &Normals2);

	TRenderer_Update_Geometry_Data(Renderer, SrcGeometryID, &Indices1, &Vertices1, &TexCoords1, &Normals1);

	TArray_Destroy(&Indices1);
	TArray_Destroy(&Vertices1);
	TArray_Destroy(&TexCoords1);
	TArray_Destroy(&Normals1);
	TArray_Destroy(&Indices2);
	TArray_Destroy(&Vertices2);
	TArray_Destroy(&TexCoords2);
	TArray_Destroy(&Normals2);
}

void TRenderer_Transform_Geometry(TRenderer* Renderer, TGeometryID GeometryID, TTransform* Transform) {
	TArray Indices, Vertices, TexCoords, Normals;
	TRenderer_Get_Geometry_Data(Renderer, GeometryID, &Indices, &Vertices, &TexCoords, &Normals);

	int Index;
	for (Index = 0; Index < Vertices.Length; Index++) {
		TVector3D_Transform(TArray_Get(&Vertices, Index), Transform);
	}

	TRenderer_Update_Geometry_Data(Renderer, GeometryID, &Indices, &Vertices, &TexCoords, &Normals);

	TArray_Destroy(&Indices);
	TArray_Destroy(&Vertices);
	TArray_Destroy(&TexCoords);
	TArray_Destroy(&Normals);
}

void TRenderer_Geometry_Flip_Normals(TRenderer* Renderer, TGeometryID GeometryID) {
	TArray Indices, Vertices, TexCoords, Normals;
	TRenderer_Get_Geometry_Data(Renderer, GeometryID, &Indices, &Vertices, &TexCoords, &Normals);

	int Index;
	for (Index = 0; Index < Normals.Length; Index++) {
		TVector3D_Scale(TArray_Get(&Normals, Index), -1);
	}

	size_t IndexGroup;
	unsigned short *Index1, *Index3, Temp;
	for (IndexGroup = 0; IndexGroup < Indices.Length / 3; IndexGroup++) {
		Index1 = TArray_Get(&Indices, IndexGroup * 3 + 0);
		Index3 = TArray_Get(&Indices, IndexGroup * 3 + 2);

		Temp = *Index1;
		*Index1 = *Index3;
		*Index3 = Temp;
	}

	TRenderer_Update_Geometry_Data(Renderer, GeometryID, &Indices, &Vertices, &TexCoords, &Normals);

	TArray_Destroy(&Indices);
	TArray_Destroy(&Vertices);
	TArray_Destroy(&TexCoords);
	TArray_Destroy(&Normals);
}

TGeometryID TRenderer_Create_Plane_Geometry(TRenderer* Renderer, int Width, int Height, float SquareSize) {
	TArray Indices, Vertices, TexCoords, Normals;
	TArray_Initialize(&Indices, sizeof(unsigned short));
	TArray_Initialize(&Vertices, sizeof(TVector3D));
	TArray_Initialize(&TexCoords, sizeof(TVector2D));
	TArray_Initialize(&Normals, sizeof(TVector3D));

	int X, Y = 0;
	TVector3D TempVertex;
	TVector2D TempTexCoord;
	TVector3D TempNormal;
	TVector3D_Set(&TempNormal, 0, 1, 0);
	float TexSquareSizeX = 1.0f / Width;
	float TexSquareSizeY = 1.0f / Height;
	for (Y = 0; Y <= Height; Y++) {
		for (X = 0; X <= Width; X++) {
			TVector3D_Set(&TempVertex, X * SquareSize, 0, Y * SquareSize);
			TArray_Push(&Vertices, &TempVertex);
			TVector2D_Set(&TempTexCoord, X * TexSquareSizeX, Y * TexSquareSizeY);
			TArray_Push(&TexCoords, &TempTexCoord);
			TArray_Push(&Normals, &TempNormal);
		}
	}

	int Index, I1, I2, I3, I4;

	unsigned int MaxSide = max(Width, Height);
	MaxSide--;
	MaxSide |= MaxSide >> 1;
	MaxSide |= MaxSide >> 2;
	MaxSide |= MaxSide >> 4;
	MaxSide |= MaxSide >> 8;
	MaxSide |= MaxSide >> 16;
	MaxSide++;

	int Squares = MaxSide * MaxSide;
	int WidthPlus = Width + 1;
	for (Index = 0; Index < Squares; Index++) {
		HilburtCurve(MaxSide, Index, &X, &Y);

		if (X < Width && Y < Height) {
			I1 = X + Y * WidthPlus;
			I2 = X + (Y + 1) * WidthPlus;
			I3 = (X + 1) + Y * WidthPlus;
			I4 = (X + 1) + (Y + 1) * WidthPlus;

			TArray_Push(&Indices, &I1);
			TArray_Push(&Indices, &I2);
			TArray_Push(&Indices, &I3);

			TArray_Push(&Indices, &I3);
			TArray_Push(&Indices, &I2);
			TArray_Push(&Indices, &I4);
		}
	}

	return TRenderer_Create_Geometry(Renderer, &Indices, &Vertices, &TexCoords, &Normals);
}

TGeometryID TRenderer_Create_Sphere_Geometry(TRenderer* Renderer, float Radius, uint32_t XDetail, uint32_t YDetail, float XPercent, float YPercent) {
	TArray Vertices, TexCoords, Normals, Indices;
	TArray_Initialize(&Vertices, sizeof(TVector3D));
	TArray_Initialize(&TexCoords, sizeof(TVector2D));
	TArray_Initialize(&Normals, sizeof(TVector3D));
	TArray_Initialize(&Indices, sizeof(unsigned short));

	Radius = Radius == 0.0 ? 10.0f : Radius;

	XDetail = XDetail == 0 ? 10 : XDetail;
	YDetail = YDetail == 0 ? 10 : YDetail;

	XPercent = XPercent == 0.0f ? 1.0f : XPercent;
	YPercent = YPercent == 0.0f ? 1.0f : YPercent;

	uint32_t WidthSegments = ceil(XDetail * XPercent);
	uint32_t HeightSegments = ceil(YDetail * YPercent);

	float PhiStart = 0;
	float PhiLength = XPercent * M_PI * 2;

	float ThetaStart = 0;
	float ThetaLength = YPercent * M_PI;

	uint32_t X, Y;
	TVector3D TempVertex;
	TVector2D TempTexCoord;
	for (Y = 0; Y <= HeightSegments; Y++) {
		for (X = 0; X <= WidthSegments; X++) {
			TVector2D_Set(&TempTexCoord, X / (float)WidthSegments, Y / (float)HeightSegments);

			TArray_Push(&TexCoords, &TempTexCoord);

			TempVertex.X = -Radius * cos(PhiStart + TempTexCoord.X * PhiLength) * sin(ThetaStart + TempTexCoord.Y * ThetaLength);
			TempVertex.Y = Radius * cos(ThetaStart + TempTexCoord.Y * ThetaLength);
			TempVertex.Z = Radius * sin(PhiStart + TempTexCoord.X * PhiLength) * sin(ThetaStart + TempTexCoord.Y * ThetaLength);

			TArray_Push(&Vertices, &TempVertex);

			TVector3D_Normalize(&TempVertex);
			TArray_Push(&Normals, &TempVertex);
		}
	}

	int I1, I2, I3, I4;
	uint32_t WidthPlus = WidthSegments + 1;
	uint32_t HeightMinus = HeightSegments - 1;
	for (Y = 0; Y < HeightSegments; Y++) {
		for (X = 0; X < WidthSegments; X++) {
			I1 = X + Y * WidthPlus;
			I2 = X + (Y + 1) * WidthPlus;
			I3 = (X + 1) + Y * WidthPlus;
			I4 = (X + 1) + (Y + 1) * WidthPlus;

			if (Y == 0) {
				TArray_Push(&Indices, &I1);
				TArray_Push(&Indices, &I2);
				TArray_Push(&Indices, &I4);
			} else if (Y == HeightMinus && YPercent == 1.0f) {
				TArray_Push(&Indices, &I1);
				TArray_Push(&Indices, &I2);
				TArray_Push(&Indices, &I3);
			} else {
				TArray_Push(&Indices, &I1);
				TArray_Push(&Indices, &I2);
				TArray_Push(&Indices, &I3);

				TArray_Push(&Indices, &I3);
				TArray_Push(&Indices, &I2);
				TArray_Push(&Indices, &I4);
			}
		}
	}

	return TRenderer_Create_Geometry(Renderer, &Indices, &Vertices, &TexCoords, &Normals);
}

//----------------------------------------
// TMaterial Functions
//----------------------------------------

TMaterial* TRenderer_Get_Material(TRenderer* Renderer, TMaterialID MaterialID) {
	return TArray_Get(&Renderer->Materials, MaterialID - 1);
}

TMaterialID TRenderer_Create_Material(TRenderer* Renderer, TShaderID ShaderID, TTextureID TextureID) {
	TMaterial Material;
	Material.TextureID = TextureID;
	Material.ShaderID = ShaderID;
	Material.Culling = Culling_Back;
	Material.Refrences = 1;
	TArray_Initialize(&Material.Uniforms, sizeof(TUniformData));
	TShader* Shader = TRenderer_Get_Shader(Renderer, ShaderID);

	int Index, Uniforms = -1;
	char NameBuffer[32];
	GLsizei NameBufferLen = 0;
	TUniformData UniformData;
	GLenum UniformType = 0;
	GLsizei UniformSize = 0;
	uint8_t TextureCounter = 1;

	glGetProgramiv(Shader->ShaderProgram, GL_ACTIVE_UNIFORMS, &Uniforms);
	for (Index = 0; Index < Uniforms; Index++) {
		glGetActiveUniform(Shader->ShaderProgram, Index, sizeof(NameBuffer) - 1, &NameBufferLen, &UniformSize, &UniformType, NameBuffer);
		if (NameBufferLen) {
			NameBuffer[NameBufferLen] = 0;
			UniformData.Name = HashString(NameBuffer);
			UniformData.ShaderLocation = glGetUniformLocation(Shader->ShaderProgram, NameBuffer);
			switch (UniformType) {
				case GL_INT:
					UniformData.DataType = RenderDataType_Integer;
					UniformData.Data.Integer = 0;
					break;
				case GL_FLOAT:
					UniformData.DataType = RenderDataType_Float;
					UniformData.Data.Float = 0;
					break;
				case GL_FLOAT_VEC2:
					UniformData.DataType = RenderDataType_Vector2D;
					TVector2D_Initialize(&UniformData.Data.Vector2D);
					break;
				case GL_FLOAT_VEC3:
					UniformData.DataType = RenderDataType_Vector3D;
					TVector3D_Initialize(&UniformData.Data.Vector3D);
					break;
				case GL_SAMPLER_2D:
					UniformData.DataType = RenderDataType_Texture;
					UniformData.Data.TextureID = 0;
					UniformData.TextureLocation = Shader->TextureLocation == UniformData.ShaderLocation ? 0 : TextureCounter++;
					break;
				default:
					continue;
			}
			TArray_Push(&Material.Uniforms, &UniformData);
		}
	}

	TArray_Push(&Renderer->Materials, &Material);
	return Renderer->Materials.Length;
}

void TRenderer_Activate_Material(TRenderer* Renderer, TMaterialID MaterialID) {
	if (MaterialID != Renderer->CurrentMaterialID) {
		TMaterial* Material = TRenderer_Get_Material(Renderer, MaterialID);
		TShader* Shader = TRenderer_Get_Shader(Renderer, Material->ShaderID);
		if (Material->ShaderID != Renderer->CurrentShaderID) {
			glUseProgram(Shader->ShaderProgram);
			Renderer->CurrentShaderID = Material->ShaderID;
		}
		if (Material->TextureID && Material->TextureID != Renderer->CurrentTextureID) {
			TTexture* Texture = TRenderer_Get_Texture(Renderer, Material->TextureID);
			if (Texture->TextureID && Shader->TextureLocation) {
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, Texture->TextureID);
				glUniform1i(Shader->TextureLocation, 0);
				glGetError();  // TODO: What the fucking fuck
				Renderer->CurrentTextureID = Material->TextureID;
			}
		}
		if (Material->Culling != Renderer->CurrentCulling) {
			switch (Material->Culling) {
				case Culling_Front:
					if (Renderer->CurrentCulling == Culling_None) {
						glEnable(GL_CULL_FACE);
					}
					glCullFace(GL_FRONT);
					break;
				case Culling_Back:
					if (Renderer->CurrentCulling == Culling_None) {
						glEnable(GL_CULL_FACE);
					}
					glCullFace(GL_BACK);
					break;
				case Culling_None:
					glDisable(GL_CULL_FACE);
					break;
				default:
					assert(false);
			}
			Renderer->CurrentCulling = Material->Culling;
		}

		int Index;
		for (Index = 0; Index < Material->Uniforms.Length; Index++) {
			TUniformData* UniformData = TArray_Get(&Material->Uniforms, Index);
			switch (UniformData->DataType) {
				case RenderDataType_Integer:
					glUniform1i(UniformData->ShaderLocation, UniformData->Data.Integer);
					break;
				case RenderDataType_Float:
					glUniform1f(UniformData->ShaderLocation, UniformData->Data.Float);
					break;
				case RenderDataType_Vector2D:
					glUniform2f(UniformData->ShaderLocation, UniformData->Data.Vector2D.X, UniformData->Data.Vector2D.Y);
					break;
				case RenderDataType_Vector3D:
					glUniform3f(UniformData->ShaderLocation, UniformData->Data.Vector3D.X, UniformData->Data.Vector3D.Y, UniformData->Data.Vector3D.Z);
					break;
				case RenderDataType_Texture:
					if (!UniformData->TextureLocation) {
						break;
					}
					glActiveTexture(GL_TEXTURE0 + UniformData->TextureLocation);
					glBindTexture(GL_TEXTURE_2D, UniformData->Data.TextureID);
					glUniform1i(UniformData->ShaderLocation, UniformData->TextureLocation);
					break;
				default:
					assert(false);
			}
		}
		Renderer->CurrentMaterialID = MaterialID;
	}
}

void TRenderer_Set_Material_Uniform(TRenderer* Renderer, TMaterialID MaterialID, const char* Name, void* Data) {
	TMaterial* Material = TRenderer_Get_Material(Renderer, MaterialID);
	TStringHash NameHash = HashString(Name);

	int Index;
	for (Index = 0; Index < Material->Uniforms.Length; Index++) {
		TUniformData* UniformData = TArray_Get(&Material->Uniforms, Index);
		if (UniformData->Name == NameHash) {
			switch (UniformData->DataType) {
				case RenderDataType_Integer:
					UniformData->Data.Integer = *(int*)Data;
					if (Renderer->CurrentMaterialID == MaterialID) {
						glUniform1i(UniformData->ShaderLocation, UniformData->Data.Integer);
					}
					break;
				case RenderDataType_Float:
					UniformData->Data.Float = *(float*)Data;
					if (Renderer->CurrentMaterialID == MaterialID) {
						glUniform1f(UniformData->ShaderLocation, UniformData->Data.Float);
					}
					break;
				case RenderDataType_Vector2D:
					UniformData->Data.Vector2D = *(TVector2D*)Data;
					if (Renderer->CurrentMaterialID == MaterialID) {
						glUniform2f(UniformData->ShaderLocation, UniformData->Data.Vector2D.X, UniformData->Data.Vector2D.Y);
					}
					break;
				case RenderDataType_Vector3D:
					UniformData->Data.Vector3D = *(TVector3D*)Data;
					if (Renderer->CurrentMaterialID == MaterialID) {
						glUniform3f(UniformData->ShaderLocation, UniformData->Data.Vector3D.X, UniformData->Data.Vector3D.Y, UniformData->Data.Vector3D.Z);
					}
					break;
				case RenderDataType_Texture:
					UniformData->Data.TextureID = TRenderer_Get_Texture(Renderer, *(TTextureID*)Data)->TextureID;
					if (Renderer->CurrentMaterialID == MaterialID) {
						if (!UniformData->TextureLocation) {
							break;
						}
						glActiveTexture(GL_TEXTURE0 + UniformData->TextureLocation);
						glBindTexture(GL_TEXTURE_2D, UniformData->Data.TextureID);
						glUniform1i(UniformData->ShaderLocation, UniformData->TextureLocation);
					}
					break;
				default:
					assert(false);
			}
		}
	}
}

void TRenderer_Set_Material_Texture(TRenderer* Renderer, TMaterialID MaterialID, TTextureID TextureID) {
	TMaterial* Material = TRenderer_Get_Material(Renderer, MaterialID);
	Material->TextureID = TextureID;
}

TMaterialID TRenderer_Load_Material(TRenderer* Renderer, const char* MaterialPath) {
	TTextureID TextureID = Renderer->DefaultTextureID;
	TShaderID ShaderID = Renderer->DefaultShaderID;

	FILE* File;
	File = fopen(MaterialPath, "r");
	if (!File) {
		printf("Material not found: %s\n", MaterialPath);
		assert(false);
	}

	char CurrentLine[128];
	memset(CurrentLine, 0, sizeof(CurrentLine));

	while (fgets(CurrentLine, 128, File)) {
		if (strncmp(CurrentLine, "map_kd", 6) == 0) {
			char NameBuffer[128];
			int Consumed = sscanf(CurrentLine, "map_kd %128s", (char*)&NameBuffer);
			assert(Consumed == 1);
			TextureID = TRenderer_Load_Texture(Renderer, NameBuffer);
			break;
		}
	}

	fclose(File);

	return TRenderer_Create_Material(Renderer, ShaderID, TextureID);
}

void TRenderer_Release_Material(TRenderer* Renderer, TMaterialID MaterialID) {
	TMaterial* Material = TRenderer_Get_Material(Renderer, MaterialID);
	if (Material->Refrences > 0) {
		return;
	}

	if (Material->TextureID) {
		TRenderer_Release_Texture(Renderer, Material->TextureID);
	}
	if (Material->ShaderID) {
		TRenderer_Release_Shader(Renderer, Material->ShaderID);
	}
}

void TRenderer_Set_Material_Culling(TRenderer* Renderer, TMaterialID MaterialID, TCulling Culling) {
	TMaterial* Material = TRenderer_Get_Material(Renderer, MaterialID);
	Material->Culling = Culling;
}

//----------------------------------------
// TShader Functions
//----------------------------------------

TShader* TRenderer_Get_Shader(TRenderer* Renderer, TShaderID ShaderID) {
	return TArray_Get(&Renderer->Shaders, ShaderID - 1);
}

TShaderID TRenderer_Load_Shader(TRenderer* Renderer, const char* VertexShaderPath, const char* FragmentShaderPath) {
	TShader Shader;

	Shader.ShaderProgram = LoadShaderProgram(VertexShaderPath, FragmentShaderPath);

	Shader.ModelMatrixLocation = glGetUniformLocation(Shader.ShaderProgram, "ModelMatrix");
	Shader.ViewMatrixLocation = glGetUniformLocation(Shader.ShaderProgram, "ViewMatrix");
	Shader.ProjectionMatrixLocation = glGetUniformLocation(Shader.ShaderProgram, "ProjectionMatrix");
	Shader.VertexPositionLocation = glGetAttribLocation(Shader.ShaderProgram, "VertexPosition");
	Shader.VertexTexCoordLocation = glGetAttribLocation(Shader.ShaderProgram, "VertexTexCoord");
	Shader.VertexNormalLocation = glGetAttribLocation(Shader.ShaderProgram, "VertexNormal");
	Shader.TextureLocation = glGetUniformLocation(Shader.ShaderProgram, "Texture");

	TArray_Push(&Renderer->Shaders, &Shader);
	return Renderer->Shaders.Length;
}

void TRenderer_Release_Shader(TRenderer* Renderer, TShaderID ShaderID) {
	TShader* Shader = TRenderer_Get_Shader(Renderer, ShaderID);
	if (Shader->Refrences > 0) {
		return;
	}

	if (Shader->ShaderProgram) {
		glDeleteProgram(Shader->ShaderProgram);
	}
}

//----------------------------------------
// TTexture Functions
//----------------------------------------

TTextureID TRenderer_Create_Texture_Raw(TRenderer* Renderer, GLuint TextureID, uint32_t Width, uint32_t Height) {
	TTexture Texture;
	Texture.TextureID = TextureID;
	Texture.FrameBufferID = 0;
	Texture.Width = Width;
	Texture.Height = Height;
	Texture.Refrences = 1;
	TArray_Push(&Renderer->Textures, &Texture);
	return Renderer->Textures.Length;
}

TTextureID TRenderer_Create_Texture(TRenderer* Renderer, uint32_t Width, uint32_t Height) {
	GLuint TextureID = 0;

	glGenTextures(1, &TextureID);
	glBindTexture(GL_TEXTURE_2D, TextureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	return TRenderer_Create_Texture_Raw(Renderer, TextureID, Width, Height);
}

void TRenderer_Release_Texture(TRenderer* Renderer, TTextureID TextureID) {
	TTexture* Texture = TRenderer_Get_Texture(Renderer, TextureID);
	if (Texture->Refrences > 0) {
		return;
	}

	if (Texture->TextureID) {
		glDeleteTextures(1, &Texture->TextureID);
	}
	// glDeleteRenderbuffersEXT(1, &depth_rb);
	// glDeleteFramebuffersEXT(1, &fb);
}

TTexture* TRenderer_Get_Texture(TRenderer* Renderer, TTextureID TextureID) {
	return TArray_Get(&Renderer->Textures, TextureID - 1);
}

TTextureID TRenderer_Load_Texture(TRenderer* Renderer, const char* TexturePath) {
	uint32_t Width, Height;
	GLuint TextureID = LoadTexture(TexturePath, &Width, &Height);
	return TRenderer_Create_Texture_Raw(Renderer, TextureID, Width, Height);
}

TTextureID TRenderer_Load_Heightmap_Texture(TRenderer* Renderer, const char* TexturePath) {
	unsigned char* ImageData;
	float* FloatData;
	float* FloatData2;
	uint32_t Width;
	uint32_t Height;
	if (_access(TexturePath, 0) == -1) {
		printf("Texture not found: %s\n", TexturePath);
		assert(false);
	}
	int Result = lodepng_decode32_file(&ImageData, &Width, &Height, TexturePath);
	assert(!Result);
	if (Width <= 0 || Height <= 0) {
		assert(false);
		return 0;
	}

	FloatData = malloc(sizeof(float) * Width * Height);
	FloatData2 = malloc(sizeof(float) * Width * Height);

	int X, Y;
	float OneOver255 = 1.0f / 255.0f;
	for (Y = 0; Y < Height; Y++) {
		for (X = 0; X < Width; X++) {
			FloatData[Y * Width + X] = (float)ImageData[Y * Width * 4 + X * 4] * OneOver255;
		}
	}

	float Kernel[7] = {0.0366328, 0.111281, 0.216745, 0.270682, 0.216745, 0.111281, 0.0366328};
	int i;
	float Sum;

	// horizontal guassian smoothing
	for (Y = 0; Y < Height; Y++) {
		for (X = 0; X < Width; X++) {
			Sum = 0;
			for (i = 0; i < 7; i++) {
				Sum += Kernel[i] * FloatData[Y * Width + clamp(X + i - 3, 0, Width - 1)];
			}
			FloatData2[Y * Width + X] = Sum;
		}
	}

	// vertical guassian smoothing
	for (Y = 0; Y < Height; Y++) {
		for (X = 0; X < Width; X++) {
			Sum = 0;
			for (i = 0; i < 7; i++) {
				Sum += Kernel[i] * FloatData2[clamp(Y + i - 3, 0, Height - 1) * Width + X];
			}
			FloatData[Y * Width + X] = Sum;
		}
	}

	GLuint TextureID;
	glGenTextures(1, &TextureID);
	glBindTexture(GL_TEXTURE_2D, TextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE32F_ARB, Width, Height, 0, GL_LUMINANCE, GL_FLOAT, FloatData);
	glGenerateMipmap(GL_TEXTURE_2D);

	free(ImageData);
	free(FloatData);
	free(FloatData2);

	return TRenderer_Create_Texture_Raw(Renderer, TextureID, Width, Height);
}

TTextureID TRenderer_Create_Heightmap_NormalMap_Texture(TRenderer* Renderer, TTextureID HeightMapTexID) {
	TTexture* HeightMapTex = TRenderer_Get_Texture(Renderer, HeightMapTexID);
	glBindTexture(GL_TEXTURE_2D, HeightMapTex->TextureID);

	uint32_t Width, Height;
	float* HeightData;
	unsigned char* NormalData;
	int IntWidth, IntHeight;
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &IntWidth);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &IntHeight);
	Width = IntWidth;
	Height = IntHeight;
	if (Width <= 0 || Height <= 0) {
		assert(false);
		return 0;
	}
	HeightData = malloc(sizeof(float) * Width * Height);
	NormalData = malloc(sizeof(unsigned char) * Width * Height * 4);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_LUMINANCE, GL_FLOAT, HeightData);

	int X, Y;
	for (Y = 0; Y < Height; Y++) {
		for (X = 0; X < Width; X++) {

			float Height1 = HeightData[clamp(Y - 1, 0, Height - 1) * Width + X];
			float Height2 = HeightData[Y * Width + clamp(X - 1, 0, Width - 1)];
			float Height3 = HeightData[clamp(Y + 1, 0, Height - 1) * Width + X];
			float Height4 = HeightData[Y * Width + clamp(X + 1, 0, Width - 1)];

			TVector3D Normal;
			TVector3D_Set(&Normal, Height2 - Height4, 2.0f / 255.0f, Height1 - Height3);
			TVector3D_Normalize(&Normal);

			NormalData[(Y * Width + X) * 4 + 0] = Normal.X * 127.0f + 128.0f;
			NormalData[(Y * Width + X) * 4 + 1] = Normal.Y * 127.0f + 128.0f;
			NormalData[(Y * Width + X) * 4 + 2] = Normal.Z * 127.0f + 128.0f;
			NormalData[(Y * Width + X) * 4 + 3] = 255;
		}
	}

	GLuint TextureID;
	glGenTextures(1, &TextureID);
	glBindTexture(GL_TEXTURE_2D, TextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NormalData);
	glGenerateMipmap(GL_TEXTURE_2D);

	free(HeightData);
	free(NormalData);

	return TRenderer_Create_Texture_Raw(Renderer, TextureID, Width, Height);
}

void TRenderer_Render_Texture(TRenderer* Renderer, TTextureID TextureID, float DestX, float DestY, float DestWidth, float DestHeight, float SrcX, float SrcY, float SrcWidth, float SrcHeight) {
	TTransform Transform, TextureTransform;
	TTransform_Identity(&Transform);
	TTransform_Identity(&TextureTransform);

	TTransform_Scale_XYZ(&Transform, DestWidth / (Renderer->WindowWidth / 2.0f), DestHeight / (Renderer->WindowHeight / 2.0f), 0);
	TTransform_Translate_XYZ(&Transform, DestX / (Renderer->WindowWidth / 2.0f) - 1, DestY / (Renderer->WindowHeight / 2.0f) - 1, 0);
	TTransform_Scale_XYZ(&TextureTransform, SrcWidth, SrcHeight, 0);
	TTransform_Translate_XYZ(&TextureTransform, SrcX, SrcY, 0);

	TMesh* Mesh = TRenderer_Get_Mesh(Renderer, Renderer->TextureMeshID);
	TRenderer_Set_Material_Texture(Renderer, Mesh->MaterialID, TextureID);
	TRenderer_Set_Material_Uniform(Renderer, Mesh->MaterialID, "TextureMatrix", &TextureTransform);
	TRenderer_Render_Mesh(Renderer, Renderer->TextureMeshID, &Transform);
}

//----------------------------------------
// Font Functions
//----------------------------------------

typedef struct {
	int TextureX;
	int TextureY;
	int GlyphX;
	int GlyphY;
	int GlyphWidth;
	int GlyphHeight;
	int CellIncX;
	int CellIncY;
} TGlyphInfo;

typedef struct {
	unsigned int CharWidth, CharHeight;
	unsigned int TexWidth, TexHeight;
	unsigned int CharsPerRow;
	unsigned int Ascent;
	unsigned int BeginningChar;
	unsigned int NumberOfChars;
	TGlyphInfo Glyphs[150];
	GLuint FontTexture;
	TMaterialID MaterialID;
} TFontData;

// TODO: http://msdn.microsoft.com/en-us/library/ms533820%28v=vs.85%29.aspx
void TRenderer_InitializeFont(TRenderer* Renderer, const char* FontFamily, unsigned int FontSize) {
	TFontData* FontData = Renderer->FontData = malloc(sizeof(TFontData));
	TGlyphInfo* Glyphs = FontData->Glyphs;

	HFONT FontHandle = CreateFont(FontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_OUTLINE_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, FontFamily);
	assert(FontHandle);

	HGDIOBJ PreviousObject = SelectObject(Renderer->DeviceContext, FontHandle);

	TEXTMETRIC TextMetrics;
	GetTextMetrics(Renderer->DeviceContext, &TextMetrics);
	assert(TextMetrics.tmPitchAndFamily & TMPF_TRUETYPE);

	FontData->CharHeight = TextMetrics.tmHeight;
	FontData->CharWidth = TextMetrics.tmMaxCharWidth;
	FontData->Ascent = TextMetrics.tmDescent;

	static const MAT2 IDENTITY_MATRIX = {{0, 1}, {0, 0}, {0, 0}, {0, 1}};

	FontData->BeginningChar = 10;
	FontData->NumberOfChars = 150;
	FontData->CharsPerRow = 10;
	int Rows = FontData->NumberOfChars / FontData->CharsPerRow + 1;

	FontData->TexWidth = FontData->CharsPerRow * FontData->CharWidth;
	FontData->TexHeight = Rows * FontData->CharHeight;

	glPixelTransferf(GL_RED_BIAS, 1.0f);
	glPixelTransferf(GL_GREEN_BIAS, 1.0f);
	glPixelTransferf(GL_BLUE_BIAS, 1.0f);

	glGenTextures(1, &FontData->FontTexture);
	glBindTexture(GL_TEXTURE_2D, FontData->FontTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FontData->TexWidth, FontData->TexHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

	TShaderID ShaderID = TRenderer_Load_Shader(Renderer, "assets/Shaders/flat.vert", "assets/Shaders/flat.frag");
	TShader* Shader = TRenderer_Get_Shader(Renderer, ShaderID);
	assert(Shader->VertexPositionLocation != -1 && Shader->VertexTexCoordLocation != -1);
	FontData->MaterialID = TRenderer_Create_Material(Renderer, ShaderID, TRenderer_Create_Texture_Raw(Renderer, FontData->FontTexture, FontData->TexWidth, FontData->TexHeight));

	uint8_t* BitmapBuffer = 0;
	size_t BitmapBufferSize = 0;
	unsigned int CharacterIndex, BufferIndex;
	for (CharacterIndex = 0; CharacterIndex < FontData->NumberOfChars; CharacterIndex++) {
		unsigned char Character = FontData->BeginningChar + CharacterIndex;
		GLYPHMETRICS GlyphMetrics;
		unsigned int BitmapSize = GetGlyphOutline(Renderer->DeviceContext, Character, GGO_GRAY8_BITMAP, &GlyphMetrics, 0, 0, &IDENTITY_MATRIX);
		assert(BitmapSize != GDI_ERROR);

		if (BitmapSize > BitmapBufferSize) {
			BitmapBuffer = realloc(BitmapBuffer, BitmapSize);
			BitmapBufferSize = BitmapSize;
		}
		GetGlyphOutline(Renderer->DeviceContext, Character, GGO_GRAY8_BITMAP, &GlyphMetrics, BitmapSize, BitmapBuffer, &IDENTITY_MATRIX);
		Glyphs[CharacterIndex].GlyphX = GlyphMetrics.gmptGlyphOrigin.x;
		Glyphs[CharacterIndex].GlyphY = GlyphMetrics.gmptGlyphOrigin.y;
		Glyphs[CharacterIndex].GlyphWidth = GlyphMetrics.gmBlackBoxX;
		Glyphs[CharacterIndex].GlyphHeight = GlyphMetrics.gmBlackBoxY;
		Glyphs[CharacterIndex].CellIncX = GlyphMetrics.gmCellIncX;
		Glyphs[CharacterIndex].CellIncY = GlyphMetrics.gmCellIncY;
		Glyphs[CharacterIndex].TextureX = (CharacterIndex - (CharacterIndex / FontData->CharsPerRow) * FontData->CharsPerRow) * FontData->CharWidth;
		Glyphs[CharacterIndex].TextureY = (1 + (CharacterIndex / FontData->CharsPerRow)) * FontData->CharHeight - FontData->Ascent;

		if (Character != ' ' && BitmapSize != 0) {
			int BitmapLineWidth = GlyphMetrics.gmBlackBoxX + 3 - ((GlyphMetrics.gmBlackBoxX + 3) % 4);
			for (BufferIndex = 0; BufferIndex < BitmapLineWidth * GlyphMetrics.gmBlackBoxY && BufferIndex < BitmapBufferSize; BufferIndex++) {
				BitmapBuffer[BufferIndex] = BitmapBuffer[BufferIndex] == 0 ? 0 : BitmapBuffer[BufferIndex] * 4 - 1;
			}
			glTexSubImage2D(GL_TEXTURE_2D, 0, Glyphs[CharacterIndex].TextureX, Glyphs[CharacterIndex].TextureY, BitmapLineWidth, GlyphMetrics.gmBlackBoxY, GL_ALPHA, GL_UNSIGNED_BYTE, BitmapBuffer);
		}
	}
	if (BitmapBuffer) {
		free(BitmapBuffer);
	}

	glPixelTransferf(GL_RED_BIAS, 0.0f);
	glPixelTransferf(GL_GREEN_BIAS, 0.0f);
	glPixelTransferf(GL_BLUE_BIAS, 0.0f);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, 0);

	SelectObject(Renderer->DeviceContext, PreviousObject);
	DeleteObject(FontHandle);
}

void TRenderer_DrawText(TRenderer* Renderer, const char* Text, const TVector2D Translation) {
	TFontData* FontData = Renderer->FontData;
	TGlyphInfo* Glyphs = FontData->Glyphs;

	TArray Indices, Vertices, TexCoords;
	TVector3D Vector3D;
	TVector2D Vector2D;
	TArray_Initialize(&Indices, sizeof(unsigned short));
	TArray_Initialize(&Vertices, sizeof(TVector3D));
	TArray_Initialize(&TexCoords, sizeof(TVector2D));

	float X = 0, Y = 0;
	unsigned int Index, Counter;
	unsigned short NewIndices[6];
	for (Counter = Index = 0; Text[Index]; Index++) {
		uint8_t Character = Text[Index] - FontData->BeginningChar;
		if (Character < 0 || Character > FontData->NumberOfChars) {
			continue;
		}

		X += FontData->Glyphs[Character].GlyphX;

		float QuadX = X;
		float QuadY = Y - Glyphs[Character].GlyphHeight;
		float QuadWidth = (float)Glyphs[Character].GlyphWidth;
		float QuadHeight = (float)Glyphs[Character].GlyphHeight;
		float TexX = Glyphs[Character].TextureX / (float)FontData->TexWidth;
		float TexY = Glyphs[Character].TextureY / (float)FontData->TexHeight;
		float TexWidth = Glyphs[Character].GlyphWidth / (float)FontData->TexWidth;
		float TexHeight = Glyphs[Character].GlyphHeight / (float)FontData->TexHeight;

		X += Glyphs[Character].CellIncX;

		TVector3D_Set(&Vector3D, QuadX, QuadY, 0);
		TArray_Push(&Vertices, &Vector3D);
		TVector3D_Set(&Vector3D, QuadX + QuadWidth, QuadY, 0);
		TArray_Push(&Vertices, &Vector3D);
		TVector3D_Set(&Vector3D, QuadX + QuadWidth, QuadY + QuadHeight, 0);
		TArray_Push(&Vertices, &Vector3D);
		TVector3D_Set(&Vector3D, QuadX, QuadY + QuadHeight, 0);
		TArray_Push(&Vertices, &Vector3D);

		TVector2D_Set(&Vector2D, TexX, TexY);
		TArray_Push(&TexCoords, &Vector2D);
		TVector2D_Set(&Vector2D, TexX + TexWidth, TexY);
		TArray_Push(&TexCoords, &Vector2D);
		TVector2D_Set(&Vector2D, TexX + TexWidth, TexY + TexHeight);
		TArray_Push(&TexCoords, &Vector2D);
		TVector2D_Set(&Vector2D, TexX, TexY + TexHeight);
		TArray_Push(&TexCoords, &Vector2D);

		NewIndices[0] = Counter++;
		NewIndices[2] = Counter++;
		NewIndices[1] = Counter++;
		NewIndices[4] = Counter++;
		NewIndices[3] = NewIndices[0];
		NewIndices[5] = NewIndices[1];

		TArray_CopyData(&Indices, NewIndices, 6);
	}

	TTransform Transform, ProjectionMatrix, ViewMatrix;
	TTransform_Identity(&ProjectionMatrix);

	TCamera Camera;
	TCamera_Initialize(&Camera);
	TCamera_SetOrthographicProj(&Camera, 0, 1200, 900, 0, -1, 1);
	TTransform_Copy(&ProjectionMatrix, &Camera.ProjectionMatrix);

	TTransform_Identity(&ViewMatrix);
	TTransform_Identity(&Transform);
	TTransform_Translate_X(&Transform, Translation.X);
	TTransform_Translate_Y(&Transform, Translation.Y);

	TMaterial* Material = TRenderer_Get_Material(Renderer, FontData->MaterialID);
	TShader* Shader = TRenderer_Get_Shader(Renderer, Material->ShaderID);
	TRenderer_Activate_Material(Renderer, FontData->MaterialID);

	glUniformMatrix4fv(Shader->ModelMatrixLocation, 1, GL_FALSE, (float*)&Transform);
	glUniformMatrix4fv(Shader->ViewMatrixLocation, 1, GL_FALSE, (float*)&ProjectionMatrix);
	glUniformMatrix4fv(Shader->ProjectionMatrixLocation, 1, GL_FALSE, (float*)&ViewMatrix);

	// bind vertex attributes
	glEnableVertexAttribArray(Shader->VertexPositionLocation);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttribPointer(Shader->VertexPositionLocation, 3, GL_FLOAT, GL_FALSE, 0, Vertices.Data);

	glEnableVertexAttribArray(Shader->VertexTexCoordLocation);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttribPointer(Shader->VertexTexCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, TexCoords.Data);

	// draw indexed primitive
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDrawElements(GL_TRIANGLES, Indices.Length, GL_UNSIGNED_SHORT, Indices.Data);

	glDisableVertexAttribArray(Shader->VertexPositionLocation);
	glDisableVertexAttribArray(Shader->VertexTexCoordLocation);

	// clean up
	TArray_Destroy(&Indices);
	TArray_Destroy(&Vertices);
	TArray_Destroy(&TexCoords);
}
