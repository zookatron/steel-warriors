#ifndef TVector2D_H
#define TVector2D_H

#include "defines.h"

typedef struct {
	float X;
	float Y;
} TVector2D;

void TVector2D_Copy(TVector2D* Dest, const TVector2D* Src);
void TVector2D_Initialize(TVector2D* Vector);
void TVector2D_Set(TVector2D* Vector, float X, float Y);
bool TVector2D_Equals(const TVector2D* First, const TVector2D* Second);
void TVector2D_Add(TVector2D* Vector, const TVector2D* Other);
void TVector2D_Subtract(TVector2D* Vector, const TVector2D* Other);
void TVector2D_Multiply(TVector2D* Vector, const TVector2D* Other);
void TVector2D_Scale(TVector2D* Vector, const float Factor);
void TVector2D_Normalize(TVector2D* Vector);
void TVector2D_Rotate(TVector2D* Vector, const float Angle);
float TVector2D_Dot(const TVector2D* First, const TVector2D* Second);
float TVector2D_Length(const TVector2D* Vector);
float TVector2D_LengthSquared(const TVector2D* Vector);
float TVector2D_Angle(const TVector2D* Vector);
void TVector2D_Perpendicular(TVector2D* Vector);

#endif
