#ifndef TRenderScene_H
#define TRenderScene_H

#include "TObjectManager.h"
#include "TRenderer.h"
#include "defines.h"

typedef TObjectID TRenderSceneNodeID;

typedef struct {
	TObjectManager Nodes;
} TRenderScene;

void TRenderScene_Initialize(TRenderScene* Scene);
void TRenderScene_Destroy(TRenderScene* Scene);
TRenderSceneNodeID TRenderScene_Add(TRenderScene* Scene, TRenderSceneNodeID ParentID, TMeshID MeshID);
void TRenderScene_Remove(TRenderScene* Scene, TRenderSceneNodeID NodeID);
TTransform* TRenderScene_GetLocalTransform(TRenderScene* Scene, TRenderSceneNodeID NodeID);
TTransform* TRenderScene_GetWorldTransform(TRenderScene* Scene, TRenderSceneNodeID NodeID);
void TRenderScene_UpdateWorldTransforms(TRenderScene* Scene);
void TRenderScene_Render(TRenderScene* Scene, TRenderer* Renderer);

#endif