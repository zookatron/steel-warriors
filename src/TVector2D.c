#include "TVector2D.h"
#include <math.h>

void TVector2D_Copy(TVector2D* Dest, const TVector2D* Src) {
	Dest->X = Src->X;
	Dest->Y = Src->Y;
}

void TVector2D_Initialize(TVector2D* Vector) {
	Vector->X = Vector->Y = 0;
}

void TVector2D_Set(TVector2D* Vector, float X, float Y) {
	Vector->X = X;
	Vector->Y = Y;
}

bool TVector2D_Equals(const TVector2D* First, const TVector2D* Second) {
	return First->X == Second->X && First->Y == Second->Y;
}

void TVector2D_Add(TVector2D* Vector, const TVector2D* Other) {
	Vector->X += Other->X;
	Vector->Y += Other->Y;
}

void TVector2D_Subtract(TVector2D* Vector, const TVector2D* Other) {
	Vector->X -= Other->X;
	Vector->Y -= Other->Y;
}

void TVector2D_Multiply(TVector2D* Vector, const TVector2D* Other) {
	Vector->X *= Other->X;
	Vector->Y *= Other->Y;
}

void TVector2D_Scale(TVector2D* Vector, const float Factor) {
	Vector->X *= Factor;
	Vector->Y *= Factor;
}

void TVector2D_Normalize(TVector2D* Vector) {
	float Len = TVector2D_Length(Vector);
	if (Len != 0.0f) {
		Vector->X /= Len;
		Vector->Y /= Len;
	}
}

void TVector2D_Rotate(TVector2D* Vector, const float Angle) {
	float CosAngle = cos(Angle / 180.0f * 3.1415f);
	float SinAngle = sin(Angle / 180.0f * 3.1415f);
	float NewX = Vector->X * CosAngle - Vector->Y * SinAngle;
	float NewY = Vector->X * SinAngle + Vector->Y * CosAngle;
	Vector->X = NewX;
	Vector->Y = NewY;
}

float TVector2D_Dot(const TVector2D* First, const TVector2D* Second) {
	return First->X * Second->X + First->Y * Second->Y;
}

float TVector2D_Length(const TVector2D* Vector) {
	return sqrt(Vector->X * Vector->X + Vector->Y * Vector->Y);
}

float TVector2D_LengthSquared(const TVector2D* Vector) {
	return Vector->X * Vector->X + Vector->Y * Vector->Y;
}

float TVector2D_Angle(const TVector2D* Vector) {
	return atan2(Vector->Y, Vector->X) * 180.0f / 3.1415f;
}

void TVector2D_Perpendicular(TVector2D* Vector) {
	float X = Vector->X;
	Vector->X = -Vector->Y;
	Vector->Y = X;
}
