#include "TGUI.h"

typedef enum { TControlType_Label, TControlType_Graphic, TControlType_Button, TControlType_Dropdown } TControlType;

typedef union {
	TLabelInfo LabelInfo;
	TGraphicInfo GraphicInfo;
	TButtonInfo ButtonInfo;
} TControlInfo;

typedef struct {
	TControlInfo ControlInfo;
	TControlType ControlType;
} TControl;

void (*MessageRoutingMatrix[4][7])(TGUI*, TControl*, const void*, TMessageStream*);

bool TRect_Intersect(TRect* Rect, TVector2D* Vector2D) {
	return Vector2D->X > Rect->X && Vector2D->X < Rect->X + Rect->Width && Vector2D->Y > Rect->Y && Vector2D->Y < Rect->Y + Rect->Height;
}

void THandleMessage_ButtonMouseUp(TGUI* GUI, TControl* Control, const void* MessageData, TMessageStream* Output) {
	uint8_t MouseButton = *(uint8_t*)MessageData;
	if (MouseButton == MouseButton_Left && TRect_Intersect(&Control->ControlInfo.ButtonInfo.Rect, &GUI->MousePosition)) {
		TControlID ControlID = TObjectManager_GetID(&GUI->Controls, Control);
		TMessageStream_PutMessage(Output, GUIMessage_ButtonClick, &ControlID, sizeof(TControlID));
	}
}

void TGUI_Initialize(TGUI* GUI) {
	TObjectManager_Initialize(&GUI->Controls, sizeof(TControl));
	memset(MessageRoutingMatrix, 0, sizeof(MessageRoutingMatrix));
	MessageRoutingMatrix[TControlType_Button][WindowMessage_MouseUp] = &THandleMessage_ButtonMouseUp;
}

void TGUI_Destroy(TGUI* GUI) {
	TObjectManager_Destroy(&GUI->Controls);
}

void TGUI_Update(TGUI* GUI, TMessageStream* Input, TMessageStream* Output) {
	const uint8_t* InputMessage = 0;
	while ((InputMessage = TMessageStream_NextMessage(Input, InputMessage))) {
		if (*InputMessage == WindowMessage_MousePosChange) {
			TVector2D_Copy(&GUI->MousePosition, TMessage_GetData(InputMessage));
		}

		TControl* Control = 0;
		while ((Control = TObjectManager_Next(&GUI->Controls, Control))) {
			void (*MessageHandler)(TGUI*, TControl*, const void*, TMessageStream*) = MessageRoutingMatrix[Control->ControlType][*InputMessage];
			if (MessageHandler) {
				(*MessageHandler)(GUI, Control, TMessage_GetData(InputMessage), Output);
			}
		}
	}
}

void TGUI_Render(TGUI* GUI, TRenderer* Renderer) {
	TRect* Rect;
	TVector2D Vector2D;
	TControl* Control = 0;
	while ((Control = TObjectManager_Next(&GUI->Controls, Control))) {
		switch (Control->ControlType) {
			case TControlType_Button:
				Rect = &Control->ControlInfo.ButtonInfo.Rect;
				TRenderer_Render_Texture(Renderer, 0, Rect->X, Rect->Y, Rect->Width, Rect->Height, 0, 0, 1, 1);
				TVector2D_Set(&Vector2D, Rect->X + 10, Rect->Y + 35);
				TRenderer_DrawText(Renderer, Control->ControlInfo.ButtonInfo.Text, Vector2D);
				break;
			default:
				assert(false);
		}
	}
}

TControlID TGUI_CreateControl(TGUI* GUI, void* ControlInfo, size_t ControlInfoSize, TControlType Type) {
	TControl Control;
	Control.ControlType = Type;
	memcpy(&Control.ControlInfo, ControlInfo, ControlInfoSize);
	return TObjectManager_Add(&GUI->Controls, &Control);
}

void* TGUI_GetControl(TGUI* GUI, TControlID ControlID, TControlType Type) {
	TControl* Control = TObjectManager_Get(&GUI->Controls, ControlID);
	if (!Control || Control->ControlType != Type) {
		return 0;
	}
	return &Control->ControlInfo;
}

void TGUI_DestroyControl(TGUI* GUI, TControlID ControlID, TControlType Type) {
	TControl* Control = TObjectManager_Get(&GUI->Controls, ControlID);
	if (!Control || Control->ControlType != Type) {
		return;
	}
	TObjectManager_Remove(&GUI->Controls, ControlID);
}

TControlID TGUI_CreateButton(TGUI* GUI, TButtonInfo* ButtonInfo) {
	return TGUI_CreateControl(GUI, ButtonInfo, sizeof(TButtonInfo), TControlType_Button);
}

TButtonInfo* TGUI_GetButton(TGUI* GUI, TControlID ButtonID) {
	return TGUI_GetControl(GUI, ButtonID, TControlType_Button);
}

void TGUI_DestroyButton(TGUI* GUI, TControlID ButtonID) {
	return TGUI_DestroyControl(GUI, ButtonID, TControlType_Button);
}
