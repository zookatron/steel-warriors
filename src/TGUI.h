#ifndef TGUI_H
#define TGUI_H

#include "TObjectManager.h"
#include "defines.h"
#include "defines.h"

typedef enum {
	GUIMessage_ButtonClick
} TGUIMessage;

struct TGUI {
	TObjectManager Controls;
	TVector2D MousePosition;
};
typedef struct TGUI TGUI;

typedef TObjectID TControlID;

typedef struct {
	float X;
	float Y;
	float Width;
	float Height;
} TRect;

typedef struct {
	TRect Rect;
	char* Text;
	float FontSize;
} TLabelInfo;

typedef struct {
	TRect Rect;
	TRect SrcRect;
	TTextureID Texture;
} TGraphicInfo;

typedef struct {
	TRect Rect;
	char* Text;
} TButtonInfo;

void TGUI_Initialize(TGUI* GUI);
void TGUI_Destroy(TGUI* GUI);
void TGUI_Update(TGUI* GUI, TMessageStream* Input, TMessageStream* Output);
void TGUI_Render(TGUI* GUI, TRenderer* Renderer);

TControlID TGUI_CreateButton(TGUI* GUI, TButtonInfo* ButtonInfo);
TButtonInfo* TGUI_GetButton(TGUI* GUI, TControlID ButtonID);
void TGUI_DestroyButton(TGUI* GUI, TControlID ButtonID);

#endif