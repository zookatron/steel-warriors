#include "TResourceManager.h"
#include "TBuffer.h"
#include "vendor/jsmn/jsmn.h"

void TResourceManager_Initialize(TResourceManager* ResourceManager, TRenderer* Renderer) {
	ResourceManager->Renderer = Renderer;
	THashMap_Initialize(&ResourceManager->Meshes, sizeof(TMeshID));
}

void TResourceManager_Destroy(TResourceManager* ResourceManager) {
	TMeshID* MeshID = 0;
	while ((MeshID = THashMap_Next(&ResourceManager->Meshes, MeshID))) {
		TRenderer_Release_Mesh(ResourceManager->Renderer, *MeshID);
	}
	THashMap_Destroy(&ResourceManager->Meshes);
}

void TResourceManager_LoadResourcePack(TResourceManager* ResourceManager, const char* ResourcePackPath) {
	jsmn_parser Parser;
	jsmntok_t Tokens[256];
	TBuffer Input;

	TBuffer_Initialize(&Input);
	TBuffer_ReadFile(&Input, ResourcePackPath);
	char* js = Input.Data;

	jsmn_init(&Parser);
	assert(jsmn_parse(&Parser, js, Tokens, 256) == JSMN_SUCCESS);

	typedef enum { OBJECT, KEY, VALUE } parse_state;
	char* AssetTypes[] = {"Meshes", "Textures", "Sounds"};
	parse_state State = OBJECT;
	int current_asset = -1;
	char* AssetName;
	size_t RemainingAssets;

	size_t Index, AssetType, RemainingTokens;
	for (Index = 0, RemainingTokens = 1; RemainingTokens > 0; Index++, RemainingTokens--) {
		jsmntok_t* Token = &Tokens[Index];

		// Should never reach uninitialized tokens
		assert(Token->start != -1 && Token->end != -1);

		if (Token->type == JSMN_ARRAY || Token->type == JSMN_OBJECT) {
			RemainingTokens += Token->size;
		}

		switch (State) {
			case OBJECT:
				// must be an object
				assert(Token->type == JSMN_OBJECT);

				// must have even number of children, otherwise you have unmatched pairs
				assert(Token->size % 2 == 0);

				if (current_asset != -1) {
					RemainingAssets = Token->size;
				}

				State = KEY;

				break;

			case KEY:
				// object keys must be strings
				assert(Token->type == JSMN_STRING);

				if (current_asset == -1) {
					for (AssetType = 0; AssetType < sizeof(AssetTypes) / sizeof(char*); AssetType++) {
						if (json_token_streq(js, Token, AssetTypes[AssetType])) {
							current_asset = AssetType;
							break;
						}
					}

					// we must find a matching asset type
					assert(current_asset != -1);

					State = OBJECT;
				} else {
					AssetName = json_token_tostr(js, Token);

					RemainingAssets--;
					State = VALUE;
				}

				break;

			case VALUE:
				// object values must be strings or primitives
				assert(Token->type == JSMN_STRING || Token->type == JSMN_PRIMITIVE);

				char* AssetPath = json_token_tostr(js, Token);

				if (current_asset == 0) {
					TMeshID MeshID = TRenderer_Load_Mesh(ResourceManager->Renderer, AssetPath);
					THashMap_Add(&ResourceManager->Meshes, AssetName, &MeshID);
				} else {
					assert(false);
				}

				RemainingAssets--;
				State = KEY;

				if (RemainingAssets == 0) {
					current_asset = 0;
				}

				break;

			default:
				assert(false);
		}
	}
}

TMeshID TResourceManager_GetMesh(TResourceManager* ResourceManager, const char* MeshPath) {
	TMeshID* MeshID = THashMap_Get(&ResourceManager->Meshes, MeshPath);
	assert(MeshID != 0);
	return *MeshID;
}
