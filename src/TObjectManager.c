#include "TObjectManager.h"

typedef bool ActiveType;
typedef uint16_t CounterType;
typedef uint16_t PositionType;
#define POSITION_BIT_WIDTH 16
#define COUNTER_MAX 65535  //((1 << 16)-1)

/*---------------------------------------------------------*/
/* Helper Functions                                        */
/*---------------------------------------------------------*/

// In the ID structure, Position is in the Low bits, Counter is High bits.
// Position holds the index of the element in the Manager's array.
// Counter holds an access counter that is incremented in the element each time the position
// is used, so that old ObjectID's will not match anymore when an element is reused.
TObjectID To_ID(PositionType Position, CounterType Counter) {
	TObjectID WidePosition = Position;
	TObjectID WideCounter = Counter;
	return WidePosition + (WideCounter << POSITION_BIT_WIDTH);
}

PositionType ID_to_Position(uint32_t ID) {
	return (PositionType)(ID & ((1 << (POSITION_BIT_WIDTH + 1)) - 1));
}

CounterType ID_to_Counter(TObjectID ID) {
	return (CounterType)(ID >> POSITION_BIT_WIDTH);
}

bool Element_IsActive(const void* Element) {
	return *(ActiveType*)Element;
}

ActiveType* Element_Active(const void* Element) {
	return (ActiveType*)Element;
}

CounterType* Element_Counter(const void* Element) {
	return (CounterType*)(Element + sizeof(ActiveType));
}

PositionType* Element_Data(const void* Element) {
	return (PositionType*)(Element + sizeof(ActiveType) + sizeof(CounterType));
}

void* TObjectManager_GetElement(TObjectManager* ObjectManager, const TObjectID ObjectID) {
	PositionType Position = ID_to_Position(ObjectID);
	if (Position >= ObjectManager->Objects.Length) {
		return 0;
	}
	void* Element = TArray_Get(&ObjectManager->Objects, Position);
	if (!Element_IsActive(Element) || *Element_Counter(Element) != ID_to_Counter(ObjectID)) {
		return 0;
	}
	return Element;
}

/*---------------------------------------------------------*/
/* Function Implementations                                */
/*---------------------------------------------------------*/

void TObjectManager_Initialize(TObjectManager* ObjectManager, const size_t ElementSize) {
	// We need to have enought space in the data portion of the element to store our free list position when the element is freed
	size_t ArrayElementSize = sizeof(ActiveType) + sizeof(CounterType) + max(ElementSize, sizeof(PositionType));
	TArray_Initialize(&ObjectManager->Objects, ArrayElementSize);

	// MaxNumObjects indicates no free elements
	ObjectManager->NextFree = MaxNumObjects;
	ObjectManager->ElementSize = ElementSize;
	ObjectManager->NumElements = 0;
}

void TObjectManager_Destroy(TObjectManager* ObjectManager) {
	TArray_Destroy(&ObjectManager->Objects);
}

TObjectID TObjectManager_Add(TObjectManager* ObjectManager, void* Object) {
	// We can't add objects if the Manager is full
	assert(ObjectManager->Objects.Length < MaxNumObjects);

	int32_t FreePosition;
	void* FreeElement;

	// If we have free elements, use them, otherwise create a new element slot
	if (ObjectManager->NextFree != MaxNumObjects) {
		FreePosition = ObjectManager->NextFree;
		FreeElement = TArray_Get(&ObjectManager->Objects, FreePosition);
		ObjectManager->NextFree = *Element_Data(FreeElement);
		*Element_Active(FreeElement) = true;
	} else {
		TArray_Push(&ObjectManager->Objects, 0);
		FreePosition = ObjectManager->Objects.Length - 1;
		FreeElement = TArray_Get(&ObjectManager->Objects, FreePosition);
		*Element_Active(FreeElement) = true;
		*Element_Counter(FreeElement) = 0;
	}

	CounterType* FreeCounter = Element_Counter(FreeElement);
	(*FreeCounter)++;
	if (*FreeCounter >= COUNTER_MAX) {
		*FreeCounter = 1;
	}

	ObjectManager->NumElements++;

	if (Object) {
		memcpy(Element_Data(FreeElement), Object, ObjectManager->ElementSize);
	}

	return To_ID(FreePosition, *FreeCounter);
}

void TObjectManager_Remove(TObjectManager* ObjectManager, const TObjectID ObjectID) {
	void* Element = TObjectManager_GetElement(ObjectManager, ObjectID);

	if (Element) {
		*Element_Active(Element) = false;
		*Element_Data(Element) = ObjectManager->NextFree;
		ObjectManager->NextFree = ID_to_Position(ObjectID);
		ObjectManager->NumElements--;
	}
}

void* TObjectManager_Get(TObjectManager* ObjectManager, const TObjectID ObjectID) {
	void* Element = TObjectManager_GetElement(ObjectManager, ObjectID);
	if (!Element) {
		return 0;
	}
	return Element_Data(Element);
}

TObjectID TObjectManager_GetID(TObjectManager* ObjectManager, const void* Object) {
	assert(Object > ObjectManager->Objects.Data && Object < ObjectManager->Objects.Data + ObjectManager->Objects.ElementSize * ObjectManager->Objects.Length);
	const void* Element = Object - sizeof(ActiveType) - sizeof(CounterType);

	CounterType Counter = *Element_Counter(Element);
	int Distance = Element - ObjectManager->Objects.Data;
	uint16_t Position = Distance / ObjectManager->Objects.ElementSize;
	return To_ID(Position, Counter);
}

// Iteration will be invalidated by a call to TObjectManager_Add on the TObjectManager, but not a call to TObjectManager_Remove
void* TObjectManager_Next(TObjectManager* ObjectManager, void* Object) {
	size_t Index = 0;

	if (Object) {
		TObjectID ObjectID = TObjectManager_GetID(ObjectManager, Object);
		if (!ObjectID) {
			return 0;
		}
		Index = ID_to_Position(ObjectID) + 1;
	}

	void* Next = 0;
	while (Next == 0) {
		if (Index >= ObjectManager->Objects.Length) {
			return 0;
		}
		Next = TArray_Get(&ObjectManager->Objects, Index);
		if (!Element_IsActive(Next)) {
			Next = 0;
			Index++;
		}
	}
	return Element_Data(Next);
}
