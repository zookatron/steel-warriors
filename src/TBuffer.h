#ifndef TBuffer_H
#define TBuffer_H

#include "defines.h"

typedef struct {
	size_t UsedData;
	size_t DataSize;
	void* Data;
} TBuffer;

void TBuffer_Initialize(TBuffer* Buffer);
void TBuffer_Destroy(TBuffer* Buffer);
void TBuffer_Reserve(TBuffer* Buffer, const size_t Amount);
void TBuffer_Add(TBuffer* Buffer, const size_t Amount, void* Data);
void TBuffer_Remove(TBuffer* Buffer, const size_t Amount);
void TBuffer_Empty(TBuffer* Buffer);
void TBuffer_ReadFile(TBuffer* FileBuffer, const char* FilePath);

#endif