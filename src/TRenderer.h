#ifndef TRenderer_H
#define TRenderer_H

#include "TArray.h"
#include "TWindow.h"
#include "defines.h"
#include "defines.h"

typedef uint32_t TMeshID;
typedef uint32_t TGeometryID;
typedef uint32_t TMaterialID;
typedef uint32_t TShaderID;
typedef uint32_t TTextureID;

typedef enum {
	RenderDataType_Integer,
	RenderDataType_Float,
	RenderDataType_Vector2D,
	RenderDataType_Vector3D,
	RenderDataType_Texture,
} TRenderDataType;

typedef enum {
	Culling_Back,
	Culling_Front,
	Culling_None,
} TCulling;

typedef struct {
	TTransform ViewMatrix;
	TTransform InverseViewMatrix;
	TTransform ProjectionMatrix;
} TCamera;

typedef struct {
	void* Window;
	void* DeviceContext;
	void* RenderContext;
	uint32_t WindowWidth;
	uint32_t WindowHeight;
	TShaderID DefaultShaderID;
	TTextureID DefaultTextureID;
	TMaterialID DefaultMaterialID;
	TShaderID CurrentShaderID;
	TTextureID CurrentTextureID;
	TMaterialID CurrentMaterialID;
	TCulling CurrentCulling;
	TTextureID CurrentRenderTarget;
	TCamera Camera;
	TArray Meshes;
	TArray Geometries;
	TArray Materials;
	TArray Shaders;
	TArray Textures;
	void* FontData;
	TMeshID TextureMeshID;
} TRenderer;

void TCamera_Initialize(TCamera* Camera);
void TCamera_Copy(TCamera* Dest, TCamera* Src);
void TCamera_UpdateView(TCamera* Camera, TTransform* InverseViewMatrix);
void TCamera_SetOrientation(TCamera* Camera, TVector3D* Position, TVector3D* Rotation);
void TCamera_LookAt(TCamera* Camera, TVector3D* From, TVector3D* To);
void TCamera_LookAtSphere(TCamera* Camera, TVector3D* From, TVector3D* To, float Radius);
void TCamera_SetPerspectiveProj(TCamera* Camera, float FOV, float Aspect, float ZNear, float ZFar);
void TCamera_SetOrthographicProj(TCamera* Camera, float Left, float Right, float Bottom, float Top, float ZNear, float ZFar);

void TRenderer_Initialize(TRenderer* Renderer, TWindow* Window);
void TRenderer_Destroy(TRenderer* Renderer);
void TRenderer_SwapBuffers(TRenderer* Renderer);
void TRenderer_UpdateCamera(TRenderer* Renderer, TCamera* Camera);
void TRenderer_GetCamera(TRenderer* Renderer, TCamera* Camera);
void TRenderer_DepthMask(bool Active);
void TRenderer_Set_Render_Target(TRenderer* Renderer, TTextureID TextureID);
void TRenderer_Render_Test(TRenderer* Renderer);

TMeshID TRenderer_Create_Mesh(TRenderer* Renderer, TGeometryID GeometryID, TMaterialID MaterialID);
TMeshID TRenderer_Copy_Mesh(TRenderer* Renderer, TMeshID MeshID);
TMeshID TRenderer_Load_Mesh(TRenderer* Renderer, const char* MeshPath);
void TRenderer_Release_Mesh(TRenderer* Renderer, TMeshID MeshID);
void TRenderer_Render_Mesh(TRenderer* Renderer, TMeshID MeshID, TTransform* Transform);
void TRenderer_Update_Mesh_Instanced_Data(TRenderer* Renderer, TMeshID MeshID, char* Name, TRenderDataType DataType, void* Data, uint32_t NumInstances);

TMaterialID TRenderer_Create_Material(TRenderer* Renderer, TShaderID ShaderID, TTextureID TextureID);
TMaterialID TRenderer_Load_Material(TRenderer* Renderer, const char* MaterialPath);
void TRenderer_Release_Material(TRenderer* Renderer, TMaterialID MaterialID);
void TRenderer_Activate_Material(TRenderer* Renderer, TMaterialID MaterialID);
void TRenderer_Set_Material_Uniform(TRenderer* Renderer, TMaterialID MaterialID, const char* Name, void* Data);
void TRenderer_Set_Material_Culling(TRenderer* Renderer, TMaterialID MaterialID, TCulling Culling);

TTextureID TRenderer_Create_Texture(TRenderer* Renderer, uint32_t Width, uint32_t Height);
TTextureID TRenderer_Load_Texture(TRenderer* Renderer, const char* TexturePath);
void TRenderer_Release_Texture(TRenderer* Renderer, TTextureID TextureID);
TTextureID TRenderer_Load_Heightmap_Texture(TRenderer* Renderer, const char* TexturePath);
TTextureID TRenderer_Create_Heightmap_NormalMap_Texture(TRenderer* Renderer, TTextureID HeightMapTexID);
void TRenderer_Render_Texture(TRenderer* Renderer, TTextureID TextureID, float DestX, float DestY, float DestWidth, float DestHeight, float SrcX, float SrcY, float SrcWidth, float SrcHeight);

TShaderID TRenderer_Load_Shader(TRenderer* Renderer, const char* VertexShaderPath, const char* FragmentShaderPath);
void TRenderer_Release_Shader(TRenderer* Renderer, TShaderID ShaderID);

TGeometryID TRenderer_Create_Geometry(TRenderer* Renderer, TArray* Indices, TArray* Vertices, TArray* TexCoords, TArray* Normals);
void TRenderer_Release_Geometry(TRenderer* Renderer, TGeometryID GeometryID);
TGeometryID TRenderer_Copy_Geometry(TRenderer* Renderer, TGeometryID GeometryID);
void TRenderer_Update_Geometry_Data(TRenderer* Renderer, TGeometryID GeometryID, TArray* Indices, TArray* Vertices, TArray* TexCoords, TArray* Normals);
void TRenderer_Get_Geometry_Data(TRenderer* Renderer, TGeometryID GeometryID, TArray* Indices, TArray* Vertices, TArray* TexCoords, TArray* Normals);
void TRenderer_Add_Geometry(TRenderer* Renderer, TGeometryID SrcGeometryID, TGeometryID DestGeometryID);
void TRenderer_Transform_Geometry(TRenderer* Renderer, TGeometryID GeometryID, TTransform* Transform);
void TRenderer_Geometry_Flip_Normals(TRenderer* Renderer, TGeometryID GeometryID);

TGeometryID TRenderer_Create_Sphere_Geometry(TRenderer* Renderer, float Radius, uint32_t XDetail, uint32_t YDetail, float XPercent, float YPercent);
TGeometryID TRenderer_Create_Plane_Geometry(TRenderer* Renderer, int Width, int Height, float SquareSize);
void TRenderer_DrawText(TRenderer* Renderer, const char* Text, const TVector2D Translation);

#endif