#include "TMessageStream.h"
#include "string.h"
#include <stdlib.h>

typedef struct {
	TMessageType MessageType;
	size_t DataSize;
} TMessageHeader;

void TMessageStream_Initialize(TMessageStream* MessageStream) {
	MessageStream->UsedData = MessageStream->DataSize = 0;
	MessageStream->Data = 0;
}

void TMessageStream_Destroy(TMessageStream* MessageStream) {
	free(MessageStream->Data);
	MessageStream->UsedData = MessageStream->DataSize = 0;
	MessageStream->Data = 0;
}

const void* TMessageStream_NextMessage(const TMessageStream* MessageStream, TMessage LastMessage) {
	if (LastMessage) {
		const void* Target = LastMessage + sizeof(TMessageHeader) + ((TMessageHeader*)LastMessage)->DataSize;
		return (Target - MessageStream->Data) < MessageStream->UsedData ? Target : 0;
	}
	return MessageStream->UsedData ? MessageStream->Data : 0;
}

void TMessageStream_PutMessage(TMessageStream* MessageStream, const uint32_t MessageType, const void* Data, const size_t DataSize) {
	size_t TotalSize = sizeof(TMessageHeader) + DataSize;
	if (MessageStream->UsedData + TotalSize > MessageStream->DataSize) {
		MessageStream->DataSize = MessageStream->DataSize ? (TotalSize + MessageStream->DataSize) << 1 : 2 * TotalSize;
		MessageStream->Data = realloc(MessageStream->Data, MessageStream->DataSize);
	}

	TMessageHeader MessageHeader = {MessageType, DataSize};
	memcpy(MessageStream->Data + MessageStream->UsedData, &MessageHeader, sizeof(TMessageHeader));
	memcpy(MessageStream->Data + MessageStream->UsedData + sizeof(TMessageHeader), Data, DataSize);
	MessageStream->UsedData += TotalSize;
}

void TMessageStream_Empty(TMessageStream* MessageStream) {
	MessageStream->UsedData = 0;
}

TMessageType TMessage_GetType(TMessage Message) {
	return ((TMessageHeader*)Message)->MessageType;
}

const void* TMessage_GetData(TMessage Message) {
	return Message + sizeof(TMessageHeader);
}

size_t TMessage_GetDataSize(TMessage Message) {
	return ((TMessageHeader*)Message)->DataSize;
}