#ifndef TWINDOW_H
#define TWINDOW_H

#include "TMessageStream.h"
#include "defines.h"
#include "defines.h"

typedef struct {
	void* WindowHandle;
	bool MouseCaptured;
	TVector2D ScreenSize;
} TWindow;

typedef enum {
	WindowMessage_None,
	WindowMessage_MousePosChange,
	WindowMessage_MouseMove,
	WindowMessage_MouseDown,
	WindowMessage_MouseUp,
	WindowMessage_KeyDown,
	WindowMessage_KeyUp,
	WindowMessage_CaptureLost,
} TWindowMessage;

typedef enum {
	MouseButton_None,
	MouseButton_Left,
	MouseButton_Right,
	MouseButton_Middle
} TMouseButton;

typedef enum {
	Key_None,
	Key_Space,
	Key_Escape,
	Key_Enter,
	Key_Tab,
	Key_A = 0x41,
	Key_B,
	Key_C,
	Key_D,
	Key_E,
	Key_F,
	Key_G,
	Key_H,
	Key_I,
	Key_J,
	Key_K,
	Key_L,
	Key_M,
	Key_N,
	Key_O,
	Key_P,
	Key_Q,
	Key_R,
	Key_S,
	Key_T,
	Key_U,
	Key_V,
	Key_W,
	Key_X,
	Key_Y,
	Key_Z,
} TKey;

typedef struct {
	bool MouseDown[4];
	bool MousePressed[4];
	bool MouseReleased[4];
	bool KeyDown[255];
	bool KeyPressed[255];
	bool KeyReleased[255];
	TVector2D MousePos;
	TVector2D MouseMove;
} TInputState;

void TWindow_Initialize(TWindow* Window, const uint32_t Width, const uint32_t Height);
void TWindow_Update(TWindow* Window, TMessageStream* WindowMessages);
void TWindow_Capture_Mouse(TWindow* Window, bool Capture);
void TWindow_Destroy(const TWindow* Window);
bool Thread_PumpMessages();
void Thread_Sleep(const uint32_t Milliseconds);
void TInputState_Initialize(TInputState* InputState);
void TInputState_Update(TInputState* InputState, const TMessageStream* WindowMessages);

#endif