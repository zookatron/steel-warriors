#ifndef TResourceManager_H
#define TResourceManager_H

#include "THashMap.h"
#include "TRenderer.h"
#include "defines.h"

typedef struct {
	TRenderer* Renderer;
	THashMap Meshes;
} TResourceManager;

void TResourceManager_Initialize(TResourceManager* ResourceManager, TRenderer* Renderer);
void TResourceManager_Destroy(TResourceManager* ResourceManager);
void TResourceManager_LoadResourcePack(TResourceManager* ResourceManager, const char* ResourcePackPath);
TMeshID TResourceManager_GetMesh(TResourceManager* ResourceManager, const char* MeshPath);

#endif