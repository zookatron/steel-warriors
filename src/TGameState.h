#ifndef TGameState_H
#define TGameState_H

#include "TClouds.h"
#include "TTerrain.h"
#include "TObjectManager.h"
#include "TRenderScene.h"
#include "TRenderer.h"
#include "TResourceManager.h"
#include "TShip.h"
#include "TTransform.h"

typedef TObjectID TShipID;
typedef TObjectID TPlayerID;
typedef TObjectID TProjectileID;

struct TPlayer {
	TShipID ShipID;
	int Score;
	float RespawnTimer;
};
typedef struct TPlayer TPlayer;

struct TGameState {
	TObjectManager Players;
	TPlayerID LocalPlayer;
	TObjectManager Ships;
	TObjectManager Projectiles;
	TResourceManager ResourceManager;
	TCamera Camera;
	TRenderScene RenderScene;
	TTerrain Terrain;
	TClouds Clouds;
	TRenderer* Renderer;
	bool Authority;
};
typedef struct TGameState TGameState;

void TGameState_Initialize(TGameState* GameState, TRenderer* Renderer, bool Authority);
void TGameState_Destroy(TGameState* GameState);
void TGameState_Step(TGameState* GameState, float Delta);
void TGameState_Render(TGameState* GameState, TRenderer* Renderer);
TPlayerID TGameState_AddPlayer(TGameState* GameState);
void TGameState_RemovePlayer(TGameState* GameState, TPlayerID PlayerID);
TPlayer* TGameState_GetPlayer(TGameState* GameState, TPlayerID PlayerID);
TShipID TGameState_CreateShip(TGameState* GameState);
void TGameState_DestroyShip(TGameState* GameState, TShipID ShipID);
TShip* TGameState_GetShip(TGameState* GameState, TShipID ShipID);
TPlayerID TGameState_FindShipPlayer(TGameState* GameState, TShipID ShipID);
TProjectileID TGameState_CreateProjectile(TGameState* GameState, TTransform* Transform);
void TGameState_DestroyProjectile(TGameState* GameState, TProjectileID ProjectileID);
TShip* TGameState_GetProjectile(TGameState* GameState, TProjectileID ProjectileID);
TShip* TGameState_GetPlayerShip(TGameState* GameState, TPlayerID PlayerID);
void TGameState_CameraFollow(TGameState* GameState, TShip* Ship);

#endif