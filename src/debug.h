#ifndef debug_H
#define debug_H

#include "defines.h"
#include "TMessageStream.h"
#include "TTransform.h"
#include "TVector2D.h"
#include "TVector3D.h"

void DebugRawData(const void* Data, size_t DataLength);
void DebugString(const char* Data);
void DebugInteger(const long long int Data);
void DebugFloat(const double Data);
void DebugPointer(const void* Data);
void DebugMessageStream(TMessageStream* MessageStream);
void DebugTransform(TTransform* Transform);
void DebugVector2D(TVector2D* Vector2D);
void DebugVector3D(TVector3D* Vector3D);

#endif