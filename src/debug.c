#include "debug.h"
#include <stdio.h>

void DebugRawData(const void* Data, size_t DataLength) {
	int Index;
	printf("Data at 0x%p:\n", Data);
	for (Index = 0; Index < DataLength; Index++) {
		printf("%.2X ", *(((unsigned char*)Data) + Index));
		if ((Index + 1) % 4 == 0 || Index == DataLength - 1) {
			printf("\n");
		}
	}
}

void DebugString(const char* Data) {
	printf("%s\n", Data);
}

void DebugInteger(const long long int Data) {
	printf("%lld\n", Data);
}

void DebugFloat(const double Data) {
	printf("%f\n", Data);
}

void DebugPointer(const void* Data) {
	printf("%p\n", Data);
}

void DebugMessageStream(TMessageStream* MessageStream) {
	const uint32_t* Message = 0;
	printf("-------------------------------\n");
	while ((Message = TMessageStream_NextMessage(MessageStream, Message))) {
		printf("%p, %d\n", Message, *Message);
	}
	printf("%zu\n", MessageStream->DataSize);
}

void DebugTransform(TTransform* Transform) {
	printf("---------------\n");
	printf("%4.1f %4.1f %4.1f %4.1f\n", Transform->Matrix[0][0], Transform->Matrix[0][1], Transform->Matrix[0][2], Transform->Matrix[0][3]);
	printf("%4.1f %4.1f %4.1f %4.1f\n", Transform->Matrix[1][0], Transform->Matrix[1][1], Transform->Matrix[1][2], Transform->Matrix[1][3]);
	printf("%4.1f %4.1f %4.1f %4.1f\n", Transform->Matrix[2][0], Transform->Matrix[2][1], Transform->Matrix[2][2], Transform->Matrix[2][3]);
	printf("%4.1f %4.1f %4.1f %4.1f\n", Transform->Matrix[3][0], Transform->Matrix[3][1], Transform->Matrix[3][2], Transform->Matrix[3][3]);
	printf("---------------\n");
}

void DebugVector2D(TVector2D* Vector2D) {
	printf("<%6.3f %6.3f>\n", Vector2D->X, Vector2D->Y);
}

void DebugVector3D(TVector3D* Vector3D) {
	printf("<%6.3f %6.3f %6.3f>\n", Vector3D->X, Vector3D->Y, Vector3D->Z);
}