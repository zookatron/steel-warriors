#include "THashMap.h"
#include "Hashing.h"

int THashMap_Find(THashMap* HashMap, const char* Name) {
	TStringHash NameHash = HashString(Name);
	int Index;
	for (Index = 0; Index < HashMap->Elements.Length; Index++) {
		if (*(TStringHash*)TArray_Get(&HashMap->Elements, Index) == NameHash) {
			return Index;
		}
	}
	return -1;
}

void THashMap_Initialize(THashMap* HashMap, const size_t ElementSize) {
	TArray_Initialize(&HashMap->Elements, sizeof(TStringHash) + ElementSize);
}

void THashMap_Destroy(THashMap* HashMap) {
	TArray_Destroy(&HashMap->Elements);
}

void THashMap_Add(THashMap* HashMap, char* Name, void* Element) {
	TArray_Push(&HashMap->Elements, 0);
	void* Data = TArray_Get(&HashMap->Elements, HashMap->Elements.Length - 1);
	*(TStringHash*)Data = HashString(Name);
	memcpy(Data + sizeof(TStringHash), Element, HashMap->Elements.ElementSize - sizeof(TStringHash));
}

void THashMap_Remove(THashMap* HashMap, const char* Name) {
	int Index = THashMap_Find(HashMap, Name);
	if (Index != -1) {
		TArray_Remove(&HashMap->Elements, Index);
	}
}

void* THashMap_Get(THashMap* HashMap, const char* Name) {
	int Index = THashMap_Find(HashMap, Name);
	if (Index == -1) {
		return 0;
	}
	return TArray_Get(&HashMap->Elements, Index) + sizeof(TStringHash);
}

void* THashMap_Next(THashMap* HashMap, void* Element) {
	size_t Index = Element ? (Element - HashMap->Elements.Data) / HashMap->Elements.ElementSize + 1 : 0;
	if (Index >= HashMap->Elements.Length) {
		return 0;
	}
	return TArray_Get(&HashMap->Elements, Index) + sizeof(TStringHash);
}
