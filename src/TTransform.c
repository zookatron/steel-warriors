#include "TTransform.h"
#include "math.h"
#include <string.h>

void TTransform_Identity(TTransform* Transform) {
	Transform->Matrix[0][0] = 1;
	Transform->Matrix[0][1] = 0;
	Transform->Matrix[0][2] = 0;
	Transform->Matrix[0][3] = 0;

	Transform->Matrix[1][0] = 0;
	Transform->Matrix[1][1] = 1;
	Transform->Matrix[1][2] = 0;
	Transform->Matrix[1][3] = 0;

	Transform->Matrix[2][0] = 0;
	Transform->Matrix[2][1] = 0;
	Transform->Matrix[2][2] = 1;
	Transform->Matrix[2][3] = 0;

	Transform->Matrix[3][0] = 0;
	Transform->Matrix[3][1] = 0;
	Transform->Matrix[3][2] = 0;
	Transform->Matrix[3][3] = 1;
}

void TTransform_Copy(TTransform* Dest, const TTransform* Src) {
	memcpy(Dest->Matrix, Src->Matrix, sizeof(Dest->Matrix));
}

void TTransform_Multiply(TTransform* Transform, const TTransform* Other) {
	int Index1, Index2, Index3;

	TTransform Temp;
	memset(Temp.Matrix, 0, sizeof(Temp.Matrix));
	for (Index1 = 0; Index1 < 4; Index1++) {
		for (Index2 = 0; Index2 < 4; Index2++) {
			for (Index3 = 0; Index3 < 4; Index3++) {
				Temp.Matrix[Index2][Index1] += Other->Matrix[Index3][Index1] * Transform->Matrix[Index2][Index3];
			}
		}
	}
	TTransform_Copy(Transform, &Temp);
}

void TTransform_Inverse(TTransform* Transform) {
	int IndexX, IndexY;

	TVector3D TempTranslate;
	TempTranslate.X = -Transform->Matrix[3][0];
	TempTranslate.Y = -Transform->Matrix[3][1];
	TempTranslate.Z = -Transform->Matrix[3][2];

	TTransform Temp;
	for (IndexX = 0; IndexX < 4; IndexX++) {
		for (IndexY = 0; IndexY < 4; IndexY++) {
			Temp.Matrix[IndexX][IndexY] = Transform->Matrix[IndexY][IndexX];
		}
	}

	TVector3D_Transform(&TempTranslate, &Temp);

	for (IndexX = 0; IndexX < 3; IndexX++) {
		for (IndexY = 0; IndexY < 3; IndexY++) {
			Transform->Matrix[IndexX][IndexY] = Temp.Matrix[IndexX][IndexY];
		}
	}

	Transform->Matrix[3][0] = TempTranslate.X;
	Transform->Matrix[3][1] = TempTranslate.Y;
	Transform->Matrix[3][2] = TempTranslate.Z;
}

void TTransform_Translate_X(TTransform* Transform, const float Amount) {
	Transform->Matrix[3][0] += Amount;
}

void TTransform_Translate_Y(TTransform* Transform, const float Amount) {
	Transform->Matrix[3][1] += Amount;
}

void TTransform_Translate_Z(TTransform* Transform, const float Amount) {
	Transform->Matrix[3][2] += Amount;
}

void TTransform_Translate_XYZ(TTransform* Transform, const float X, const float Y, const float Z) {
	Transform->Matrix[3][0] += X;
	Transform->Matrix[3][1] += Y;
	Transform->Matrix[3][2] += Z;
}

void TTransform_Translate(TTransform* Transform, const TVector3D* Amount) {
	Transform->Matrix[3][0] += Amount->X;
	Transform->Matrix[3][1] += Amount->Y;
	Transform->Matrix[3][2] += Amount->Z;
}

void TTransform_Set_Translation(TTransform* Transform, const TVector3D* Translation) {
	Transform->Matrix[3][0] = Translation->X;
	Transform->Matrix[3][1] = Translation->Y;
	Transform->Matrix[3][2] = Translation->Z;
}

void TTransform_Get_Translation(const TTransform* Transform, TVector3D* Translation) {
	Translation->X = Transform->Matrix[3][0];
	Translation->Y = Transform->Matrix[3][1];
	Translation->Z = Transform->Matrix[3][2];
}

void TTransform_Get_Forward(const TTransform* Transform, TVector3D* Translation) {
	Translation->X = Transform->Matrix[2][0];
	Translation->Y = Transform->Matrix[2][1];
	Translation->Z = Transform->Matrix[2][2];
}

void TTransform_Get_Up(const TTransform* Transform, TVector3D* Translation) {
	Translation->X = Transform->Matrix[1][0];
	Translation->Y = Transform->Matrix[1][1];
	Translation->Z = Transform->Matrix[1][2];
}

void TTransform_Get_Right(const TTransform* Transform, TVector3D* Translation) {
	Translation->X = Transform->Matrix[0][0];
	Translation->Y = Transform->Matrix[0][1];
	Translation->Z = Transform->Matrix[0][2];
}

void TTransform_Rotate_X(TTransform* Transform, const float Angle) {
	float AngleCos = cos(Angle);
	float AngleSin = sin(Angle);

	TTransform Temp;
	Temp.Matrix[0][0] = 1;
	Temp.Matrix[0][1] = 0;
	Temp.Matrix[0][2] = 0;
	Temp.Matrix[0][3] = 0;
	Temp.Matrix[1][0] = 0;
	Temp.Matrix[1][1] = AngleCos;
	Temp.Matrix[1][2] = AngleSin;
	Temp.Matrix[1][3] = 0;
	Temp.Matrix[2][0] = 0;
	Temp.Matrix[2][1] = -AngleSin;
	Temp.Matrix[2][2] = AngleCos;
	Temp.Matrix[2][3] = 0;
	Temp.Matrix[3][0] = 0;
	Temp.Matrix[3][1] = 0;
	Temp.Matrix[3][2] = 0;
	Temp.Matrix[3][3] = 1;

	TTransform_Multiply(Transform, &Temp);
}

void TTransform_Rotate_Y(TTransform* Transform, const float Angle) {
	float AngleCos = cos(Angle);
	float AngleSin = sin(Angle);

	TTransform Temp;
	Temp.Matrix[0][0] = AngleCos;
	Temp.Matrix[0][1] = 0;
	Temp.Matrix[0][2] = -AngleSin;
	Temp.Matrix[0][3] = 0;
	Temp.Matrix[1][0] = 0;
	Temp.Matrix[1][1] = 1;
	Temp.Matrix[1][2] = 0;
	Temp.Matrix[1][3] = 0;
	Temp.Matrix[2][0] = AngleSin;
	Temp.Matrix[2][1] = 0;
	Temp.Matrix[2][2] = AngleCos;
	Temp.Matrix[2][3] = 0;
	Temp.Matrix[3][0] = 0;
	Temp.Matrix[3][1] = 0;
	Temp.Matrix[3][2] = 0;
	Temp.Matrix[3][3] = 1;

	TTransform_Multiply(Transform, &Temp);
}

void TTransform_Rotate_Z(TTransform* Transform, const float Angle) {
	float AngleCos = cos(Angle);
	float AngleSin = sin(Angle);

	TTransform Temp;
	Temp.Matrix[0][0] = AngleCos;
	Temp.Matrix[0][1] = AngleSin;
	Temp.Matrix[0][2] = 0;
	Temp.Matrix[0][3] = 0;
	Temp.Matrix[1][0] = -AngleSin;
	Temp.Matrix[1][1] = AngleCos;
	Temp.Matrix[1][2] = 0;
	Temp.Matrix[1][3] = 0;
	Temp.Matrix[2][0] = 0;
	Temp.Matrix[2][1] = 0;
	Temp.Matrix[2][2] = 1;
	Temp.Matrix[2][3] = 0;
	Temp.Matrix[3][0] = 0;
	Temp.Matrix[3][1] = 0;
	Temp.Matrix[3][2] = 0;
	Temp.Matrix[3][3] = 1;

	TTransform_Multiply(Transform, &Temp);
}

void TTransform_Rotate_XYZ(TTransform* Transform, const float X, const float Y, const float Z) {
	TTransform_Rotate_X(Transform, X);
	TTransform_Rotate_Y(Transform, Y);
	TTransform_Rotate_Z(Transform, Z);
}

void TTransform_Rotate(TTransform* Transform, const TVector3D* Angles) {
	TTransform_Rotate_X(Transform, Angles->X);
	TTransform_Rotate_Y(Transform, Angles->Y);
	TTransform_Rotate_Z(Transform, Angles->Z);
}

void TTransform_Scale_X(TTransform* Transform, const float Amount) {
	Transform->Matrix[0][0] *= Amount;
}

void TTransform_Scale_Y(TTransform* Transform, const float Amount) {
	Transform->Matrix[1][1] *= Amount;
}

void TTransform_Scale_Z(TTransform* Transform, const float Amount) {
	Transform->Matrix[2][2] *= Amount;
}

void TTransform_Scale_XYZ(TTransform* Transform, const float X, const float Y, const float Z) {
	Transform->Matrix[0][0] *= X;
	Transform->Matrix[1][1] *= Y;
	Transform->Matrix[2][2] *= Z;
}

void TTransform_Scale(TTransform* Transform, const TVector3D* Amount) {
	Transform->Matrix[0][0] *= Amount->X;
	Transform->Matrix[1][1] *= Amount->Y;
	Transform->Matrix[2][2] *= Amount->Z;
}

void TTransform_Scale_Uniform(TTransform* Transform, const float Amount) {
	Transform->Matrix[0][0] *= Amount;
	Transform->Matrix[1][1] *= Amount;
	Transform->Matrix[2][2] *= Amount;
}

void TVector3D_Transform(TVector3D* Vector, const TTransform* Transform) {
	TVector3D Temp;
	TVector3D_Initialize(&Temp);
	Temp.X = Vector->X * Transform->Matrix[0][0] + Vector->Y * Transform->Matrix[1][0] + Vector->Z * Transform->Matrix[2][0] + Transform->Matrix[3][0];
	Temp.Y = Vector->X * Transform->Matrix[0][1] + Vector->Y * Transform->Matrix[1][1] + Vector->Z * Transform->Matrix[2][1] + Transform->Matrix[3][1];
	Temp.Z = Vector->X * Transform->Matrix[0][2] + Vector->Y * Transform->Matrix[1][2] + Vector->Z * Transform->Matrix[2][2] + Transform->Matrix[3][2];
	TVector3D_Copy(Vector, &Temp);
}

void TTransform_PointAt(TTransform* Transform, TVector3D* From, TVector3D* To) {
	TVector3D Difference;
	TVector3D_Copy(&Difference, To);
	TVector3D_Subtract(&Difference, From);

	float Dist = sqrt(Difference.Z * Difference.Z + Difference.X * Difference.X);
	float RotX = atan2(Difference.Y, Dist);
	float RotY = atan2(-Difference.X, -Difference.Z);

	TTransform_Identity(Transform);
	TTransform_Rotate_XYZ(Transform, RotX, RotY, 0);
	TTransform_Set_Translation(Transform, From);
}