#ifndef TMessageStream_H
#define TMessageStream_H

#include "TVector2D.h"

typedef uint32_t TMessageType;
typedef const void* TMessage;

typedef struct {
	size_t UsedData;
	size_t DataSize;
	void* Data;
} TMessageStream;

void TMessageStream_Initialize(TMessageStream* MessageStream);
void TMessageStream_Destroy(TMessageStream* MessageStream);
TMessage TMessageStream_NextMessage(const TMessageStream* MessageStream, TMessage LastMessage);
void TMessageStream_PutMessage(TMessageStream* MessageStream, const TMessageType MessageType, const void* Data, const size_t DataSize);
void TMessageStream_Empty(TMessageStream* MessageStream);

TMessageType TMessage_GetType(TMessage Message);
const void* TMessage_GetData(TMessage Message);
size_t TMessage_GetDataSize(TMessage Message);

#endif