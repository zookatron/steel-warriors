#include "TProjectile.h"

void TProjectile_Initialize(TProjectile* Projectile) {
	memset(Projectile, 0, sizeof(TProjectile));
	TTransform_Identity(&Projectile->Transform);
	Projectile->Life = 10;
}

void TProjectile_Destroy(TProjectile* Projectile) {
	TTransform_Identity(&Projectile->Transform);
}

void TProjectile_Update(TProjectile* Projectile, TGameState* GameState, float Delta) {
	TVector3D TempVelocity;
	TVector3D_Copy(&TempVelocity, &Projectile->Velocity);
	TVector3D_Scale(&TempVelocity, Delta);
	TVector3D_Add(&Projectile->Position, &TempVelocity);

	TTransform_Set_Translation(&Projectile->Transform, &Projectile->Position);

	TTransform_Copy(TRenderScene_GetLocalTransform(&GameState->RenderScene, Projectile->SceneNodeID), &Projectile->Transform);

	Projectile->Life -= Delta;
}