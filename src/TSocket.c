#include "TSocket.h"
#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>

typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr sockaddr;

typedef struct {
	sockaddr_in Address;
	bool Connected;
	bool Disconnected;
} TConnection;

void TSocket_Send_Raw(TSocket* Socket, TConnectionID ConnectionID, TSocketMessage Message, const void* Data, size_t DataSize) {
	if (DataSize >= 1024) {
		assert(false);
		return;
	}
	char Buffer[1024];
	size_t BufferSize = DataSize + sizeof(TSocketMessage);
	memcpy(Buffer, &Message, sizeof(TSocketMessage));
	if (DataSize > 0) {
		assert(Data);
		memcpy(Buffer + sizeof(TSocketMessage), Data, DataSize);
	}

	TConnection* Connection = TObjectManager_Get(&Socket->Connections, ConnectionID);
	assert(Connection);
	int BytesSent = sendto(Socket->Socket, Buffer, BufferSize, 0, (sockaddr*)&Connection->Address, sizeof(sockaddr_in));
	// display_string("send:");
	// display_int(BytesSent);
	if (BytesSent != BufferSize) {
		int Error = WSAGetLastError();
		if (Error == 10054) {
			Connection->Connected = false;
			Connection->Disconnected = true;
		}
		printf("Error: %d", Error);
		assert(BytesSent == BufferSize);
	}
}

TConnectionID TSocket_AddConnection(TSocket* Socket, unsigned long IP, u_short Port) {
	TConnection NewConnection;
	NewConnection.Address.sin_addr.s_addr = IP;
	NewConnection.Address.sin_port = Port;
	NewConnection.Address.sin_family = AF_INET;
	NewConnection.Connected = false;
	NewConnection.Disconnected = false;

	return TObjectManager_Add(&Socket->Connections, &NewConnection);
}

TConnectionID TSocket_FindConnection(TSocket* Socket, sockaddr_in* Address) {
	TConnection* Connection = 0;
	while ((Connection = TObjectManager_Next(&Socket->Connections, Connection))) {
		if (memcmp(&Connection->Address, Address, 8) == 0) {
			return TObjectManager_GetID(&Socket->Connections, Connection);
		}
	}
	return 0;
}

void TSocket_Initialize(TSocket* Socket, int Port) {
	TObjectManager_Initialize(&Socket->Connections, sizeof(TConnection));

	WSADATA WSAData;
	assert(WSAStartup(MAKEWORD(2, 2), &WSAData) == 0);

	sockaddr_in Address;
	Address.sin_addr.s_addr = INADDR_ANY;
	Address.sin_port = htons(Port);
	Address.sin_family = AF_INET;

	Socket->Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	assert(Socket->Socket != SOCKET_ERROR);
	int OneInt = 1;
	unsigned long OneLong = 1;
	assert(setsockopt((SOCKET)Socket->Socket, SOL_SOCKET, SO_REUSEADDR, (char*)&OneInt, sizeof(OneInt)) != SOCKET_ERROR);
	assert(ioctlsocket((SOCKET)Socket->Socket, FIONBIO, &OneLong) == NO_ERROR);
	bind(Socket->Socket, (sockaddr*)&Address, sizeof(sockaddr_in));
}

TConnectionID TSocket_Connect(TSocket* Socket, const char* IP, int Port) {
	TConnectionID ConnectionID = TSocket_AddConnection(Socket, inet_addr(IP), htons(Port));
	TSocket_Send_Raw(Socket, ConnectionID, SocketMessage_Connect, 0, 0);
	return ConnectionID;
}

void TSocket_Disconnect(TSocket* Socket, TConnectionID ConnectionID) {
	TSocket_Send_Raw(Socket, ConnectionID, SocketMessage_Disconnect, 0, 0);
	TConnection* Connection = TObjectManager_Get(&Socket->Connections, ConnectionID);
	Connection->Connected = false;
	Connection->Disconnected = true;
}

void TSocket_Destroy(TSocket* Socket) {

	TConnection* Connection = 0;
	while ((Connection = TObjectManager_Next(&Socket->Connections, Connection))) {
		TSocket_Send_Raw(Socket, TObjectManager_GetID(&Socket->Connections, Connection), SocketMessage_Disconnect, 0, 0);
	}

	shutdown(Socket->Socket, SD_BOTH);
	closesocket(Socket->Socket);
	WSACleanup();
	Socket->Socket = INVALID_SOCKET;
	TObjectManager_Destroy(&Socket->Connections);
}

void TSocket_Update(TSocket* Socket, TMessageStream* SocketMessages) {
	sockaddr_in Address;
	int AddressSize = sizeof(Address);
	char Buffer[1024];
	int BytesReceived, Error;

	TConnection* Connection = 0;
	TConnectionID Disconnects[Socket->Connections.NumElements];
	int NumDisconnects = 0;
	while ((Connection = TObjectManager_Next(&Socket->Connections, Connection))) {
		if (Connection->Disconnected) {
			Disconnects[NumDisconnects] = TObjectManager_GetID(&Socket->Connections, Connection);
			TMessageStream_PutMessage(SocketMessages, SocketMessage_Disconnect, &Disconnects[NumDisconnects], sizeof(Disconnects[NumDisconnects]));
			NumDisconnects++;
		}
	}
	int Index;
	for (Index = 0; Index < NumDisconnects; Index++) {
		TObjectManager_Remove(&Socket->Connections, Disconnects[Index]);
	}

	while (true) {
		BytesReceived = recvfrom(Socket->Socket, Buffer, 1024, 0, (sockaddr*)&Address, &AddressSize);
		if (BytesReceived < sizeof(TSocketMessage)) {
			assert(false);
			return;
		}
		if (BytesReceived == SOCKET_ERROR) {
			Error = WSAGetLastError();
			if (Error == WSAEWOULDBLOCK) {
				break;
			}
			if (Error == 10054) {
				continue;
			}
			if (Error != 0) {
				printf("Error: %d", Error);
				assert(false);
			}
		}

		// display_string("recv:");
		// display_int(BytesReceived);

		TConnectionID ConnectionID = 0;
		TSocketMessage Message = *(TSocketMessage*)Buffer;
		char* MessageData = Buffer + sizeof(TSocketMessage);
		size_t MessageDataSize = BytesReceived - sizeof(TSocketMessage);

		ConnectionID = TSocket_FindConnection(Socket, &Address);
		if (ConnectionID == 0 && Message == SocketMessage_Connect) {
			// display_string("connect");
			// display_string(inet_ntoa(Address.sin_addr));
			// display_int(ntohs(Address.sin_port));
			ConnectionID = TSocket_AddConnection(Socket, Address.sin_addr.s_addr, Address.sin_port);
		}
		if (ConnectionID == 0) {
			continue;
		}

		char OutBuffer[1024];
		if (Message == SocketMessage_Data) {
			if (MessageDataSize <= 0) {
				assert(false);
				return;
			}
			memcpy(OutBuffer, &ConnectionID, sizeof(ConnectionID));
			memcpy(OutBuffer + sizeof(ConnectionID), MessageData, MessageDataSize);
			TMessageStream_PutMessage(SocketMessages, SocketMessage_Data, OutBuffer, MessageDataSize + sizeof(ConnectionID));
		} else if (Message == SocketMessage_Connect) {
			TConnection* Connection = TObjectManager_Get(&Socket->Connections, ConnectionID);
			if (Connection->Connected) {
				continue;
			}
			TSocket_Send_Raw(Socket, ConnectionID, SocketMessage_ConnectConfirm, 0, 0);
			TMessageStream_PutMessage(SocketMessages, SocketMessage_Connect, &ConnectionID, sizeof(ConnectionID));
			Connection->Connected = true;
		} else if (Message == SocketMessage_ConnectConfirm) {
			TConnection* Connection = TObjectManager_Get(&Socket->Connections, ConnectionID);
			if (Connection->Connected) {
				continue;
			}
			TMessageStream_PutMessage(SocketMessages, SocketMessage_Connect, &ConnectionID, sizeof(ConnectionID));
			Connection->Connected = true;
		} else if (Message == SocketMessage_Disconnect) {
			TSocket_Disconnect(Socket, ConnectionID);
		} else {
			assert(false);
		}
	}
}

void TSocket_Send(TSocket* Socket, TConnectionID ConnectionID, const void* Data, size_t DataSize) {
	TSocket_Send_Raw(Socket, ConnectionID, SocketMessage_Data, Data, DataSize);
}
