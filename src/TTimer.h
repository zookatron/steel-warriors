#ifndef TTimer_H
#define TTimer_H

#include "defines.h"

typedef struct {
	uint64_t IntervalTime;
	uint64_t StartTime;
	uint64_t Frequency;
	bool Running;
} TTimer;

void TTimer_Initialize(TTimer* Timer);
void TTimer_Restart(TTimer* Timer);
float TTimer_GetTime(TTimer* Timer);
float TTimer_Interval(TTimer* Timer);

#endif
