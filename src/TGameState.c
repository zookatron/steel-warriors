#include "TGameState.h"
#include "TProjectile.h"
#include "TShip.h"

void TGameState_Initialize(TGameState* GameState, TRenderer* Renderer, bool Authority) {
	GameState->Renderer = Renderer;
	TCamera_Initialize(&GameState->Camera);
	TResourceManager_Initialize(&GameState->ResourceManager, Renderer);
	TRenderScene_Initialize(&GameState->RenderScene);
	TObjectManager_Initialize(&GameState->Projectiles, sizeof(TProjectile));
	TObjectManager_Initialize(&GameState->Ships, sizeof(TShip));
	TTerrain_Initialize(&GameState->Terrain, Renderer);
	TClouds_Initialize(&GameState->Clouds, Renderer);
	TObjectManager_Initialize(&GameState->Players, sizeof(TPlayer));
	GameState->LocalPlayer = TGameState_AddPlayer(GameState);
	GameState->Authority = Authority;
}

void TGameState_Destroy(TGameState* GameState) {
	TObjectManager_Destroy(&GameState->Projectiles);
	TObjectManager_Destroy(&GameState->Ships);
	TResourceManager_Destroy(&GameState->ResourceManager);
	TRenderScene_Destroy(&GameState->RenderScene);
	TTerrain_Destroy(&GameState->Terrain, GameState->Renderer);
	TClouds_Destroy(&GameState->Clouds, GameState->Renderer);
}

void TGameState_Step(TGameState* GameState, float Delta) {
	TPlayer* Player = 0;
	while ((Player = TObjectManager_Next(&GameState->Players, Player))) {
		if (GameState->Authority && !Player->ShipID && Player->RespawnTimer <= 0.0f) {
			Player->ShipID = TGameState_CreateShip(GameState);
		}
	}

	TShip* Ship = 0;
	while ((Ship = TObjectManager_Next(&GameState->Ships, Ship))) {
		UpdateShipPosition(&Ship->Position, &Ship->Input, Delta);
		UpdateShipSceneNode(Ship->SceneNodeID, &GameState->RenderScene, &Ship->Position);
		PerformShipActions(Ship, Delta, GameState);
	}

	TProjectile* Projectile = 0;
	while ((Projectile = TObjectManager_Next(&GameState->Projectiles, Projectile))) {
		TProjectile_Update(Projectile, GameState, Delta);
		if (Projectile->Life <= 0) {
			TObjectManager_Remove(&GameState->Projectiles, TObjectManager_GetID(&GameState->Projectiles, Projectile));
		}
	}
}

void TGameState_Render(TGameState* GameState, TRenderer* Renderer) {
	TRenderScene_UpdateWorldTransforms(&GameState->RenderScene);
	TRenderer_UpdateCamera(Renderer, &GameState->Camera);
	TRenderScene_Render(&GameState->RenderScene, Renderer);
	TTerrain_Render(&GameState->Terrain, Renderer);
	// TClouds_Render(&GameState->Clouds, Renderer);
}

TPlayerID TGameState_AddPlayer(TGameState* GameState) {
	TPlayer Player;
	Player.ShipID = 0;
	Player.Score = 0;
	Player.RespawnTimer = 0;
	return TObjectManager_Add(&GameState->Players, &Player);
}

void TGameState_RemovePlayer(TGameState* GameState, TPlayerID PlayerID) {
	TPlayer* Player = TObjectManager_Get(&GameState->Players, PlayerID);
	if (Player->ShipID) {
		TGameState_DestroyShip(GameState, Player->ShipID);
	}
	TObjectManager_Remove(&GameState->Players, PlayerID);
}

TPlayer* TGameState_GetPlayer(TGameState* GameState, TPlayerID PlayerID) {
	return TObjectManager_Get(&GameState->Players, PlayerID);
}

TShipID TGameState_CreateShip(TGameState* GameState) {
	TShipID NewShipID = TObjectManager_Add(&GameState->Ships, 0);
	TShip* NewShip = TObjectManager_Get(&GameState->Ships, NewShipID);
	TShip_Initialize(NewShip, GameState);
	return NewShipID;
}

void TGameState_DestroyShip(TGameState* GameState, TShipID ShipID) {
	TPlayerID PlayerID = TGameState_FindShipPlayer(GameState, ShipID);
	TPlayer* Player = TObjectManager_Get(&GameState->Players, PlayerID);
	Player->ShipID = 0;
	Player->RespawnTimer = 5;

	TShip* Ship = TObjectManager_Get(&GameState->Ships, ShipID);
	TShip_Destroy(Ship, GameState);
	TObjectManager_Remove(&GameState->Ships, ShipID);
}

TShip* TGameState_GetShip(TGameState* GameState, TShipID ShipID) {
	return TObjectManager_Get(&GameState->Ships, ShipID);
}

TPlayerID TGameState_FindShipPlayer(TGameState* GameState, TShipID ShipID) {
	TPlayer* Player = 0;
	while ((Player = TObjectManager_Next(&GameState->Players, Player))) {
		if (Player->ShipID == ShipID) {
			return TObjectManager_GetID(&GameState->Players, Player);
		}
	}
	return 0;
}

TProjectileID TGameState_CreateProjectile(TGameState* GameState, TTransform* Transform) {
	TProjectileID NewProjectileID = TObjectManager_Add(&GameState->Projectiles, 0);
	TProjectile* NewProjectile = TObjectManager_Get(&GameState->Projectiles, NewProjectileID);

	float Speed = -500.0f;
	TProjectile_Initialize(NewProjectile);
	TTransform_Copy(&NewProjectile->Transform, Transform);
	TTransform_Get_Translation(&NewProjectile->Transform, &NewProjectile->Position);
	TTransform_Get_Forward(&NewProjectile->Transform, &NewProjectile->Velocity);
	TVector3D_Scale(&NewProjectile->Velocity, Speed);
	NewProjectile->SceneNodeID = TRenderScene_Add(&GameState->RenderScene, 0, TResourceManager_GetMesh(&GameState->ResourceManager, "Objects/FalconPrimaryBullet"));

	return NewProjectileID;
}

void TGameState_DestroyProjectile(TGameState* GameState, TProjectileID ProjectileID) {
	TProjectile* Projectile = TObjectManager_Get(&GameState->Projectiles, ProjectileID);
	TProjectile_Destroy(Projectile);
	TObjectManager_Remove(&GameState->Projectiles, ProjectileID);
}

TShip* TGameState_GetProjectile(TGameState* GameState, TProjectileID ProjectileID) {
	return TObjectManager_Get(&GameState->Projectiles, ProjectileID);
}

TShip* TGameState_GetPlayerShip(TGameState* GameState, TPlayerID PlayerID) {
	TPlayer* Player = TGameState_GetPlayer(GameState, PlayerID);
	if (!Player || Player->ShipID == 0) {
		return 0;
	}
	return TObjectManager_Get(&GameState->Ships, Player->ShipID);
}

void TGameState_CameraFollow(TGameState* GameState, TShip* Ship) {
	TTransform ViewMatrix, Transform;
	TVector3D Translate;
	TVector3D_Set(&Translate, 0, 5, 20);
	TTransform_Identity(&Transform);
	TTransform_Translate(&Transform, &Translate);
	TTransform_Identity(&ViewMatrix);
	TTransform_Rotate(&ViewMatrix, &Ship->Input.LookInput);
	TTransform_Get_Translation(&Ship->Position.Transform, &Translate);
	TTransform_Set_Translation(&ViewMatrix, &Translate);
	TTransform_Multiply(&Transform, &ViewMatrix);
	TCamera_UpdateView(&GameState->Camera, &Transform);

	// TVector3D_Set(&Translate, 0, 10, 0);
	// TCamera_LookAt(&GameState->Camera, &Ship->Position.Position, &Translate);
}