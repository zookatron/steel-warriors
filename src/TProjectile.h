#ifndef TProjectile_H
#define TProjectile_H

#include "TGameState.h"
#include "TRenderer.h"
#include "TShip.h"
#include "TTimer.h"

typedef struct {
	TVector3D Position;
	TVector3D Velocity;
	TTransform Transform;
	TRenderSceneNodeID SceneNodeID;
	float Life;
} TProjectile;

void TProjectile_Initialize(TProjectile* Projectile);
void TProjectile_Destroy(TProjectile* Projectile);
void TProjectile_Update(TProjectile* Projectile, TGameState* GameState, float Delta);

#endif