#ifndef TShip_H
#define TShip_H

#include "TMessageStream.h"
#include "TRenderScene.h"
#include "TTransform.h"
#include "TVector2D.h"
#include "TVector3D.h"
#include "defines.h"

typedef struct TGameState TGameState;

typedef struct {
	TVector3D Position;
	TVector3D Velocity;
	TVector3D Angle;
	TVector3D AngularVelocity;
	TTransform Transform;
} TShipPosition;

typedef struct {
	float Radius;
} TShipCollisionData;

typedef struct {
	TVector3D MoveInput;
	TVector3D LookInput;
	bool PrimaryFiring;
	bool AltFiring;
} TShipInput;

typedef struct {
	TShipPosition Position;
	TShipCollisionData CollisionData;
	TRenderSceneNodeID SceneNodeID;
	TShipInput Input;
} TShip;

void TShip_Initialize(TShip* Ship, TGameState* GameState);
void TShip_Destroy(TShip* Ship, TGameState* GameState);
void TShipInput_Initialize(TShipInput* ShipInput);
void GetShipInput(const TInputState* InputState, const TShip* Ship, TShipInput* ShipInput);
void UpdateShipPosition(TShipPosition* Ship, const TShipInput* Input, const float Delta);
void UpdateShipSceneNode(TRenderSceneNodeID SceneNodeID, TRenderScene* Scene, TShipPosition* ShipPosition);
void PerformShipActions(TShip* Ship, float Delta, TGameState* GameState);

#endif