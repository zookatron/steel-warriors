#ifndef TNetworkManager_H
#define TNetworkManager_H

#include "TGameState.h"
#include "TMessageStream.h"
#include "TSocket.h"
#include "defines.h"
#include "defines.h"

typedef struct {
	TSocket Socket;
	TMessageStream SocketMessages;
	bool IsServer;
	TConnectionID Server;
	TPlayerID ClientNetworkID;
	TArray NetworkPlayers;
	float LastUpdate;
	float Now;
	bool Connected;
} TNetworkManager;

void TNetworkManager_Initialize_Client(TNetworkManager* NetworkManager);
void TNetworkManager_Initialize_Server(TNetworkManager* NetworkManager, TGameState* GameState);
void TNetworkManager_Destroy(TNetworkManager* NetworkManager);
void TNetworkManager_Update(TNetworkManager* NetworkManager, TGameState* GameState, float Delta);

#endif