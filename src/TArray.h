#ifndef TArray_H
#define TArray_H

#include "TTransform.h"

struct TArray {
	size_t Length;
	size_t ElementSize;
	size_t DataSize;
	void* Data;
};
typedef struct TArray TArray;

void TArray_Initialize(TArray* Array, const size_t ElementSize);
void TArray_Destroy(TArray* Array);
void TArray_Copy(TArray* Dest, TArray* Src);
void TArray_CopyData(TArray* Array, void* Elements, size_t Length);
void TArray_Append(TArray* Dest, TArray* Src);
void* TArray_Get(const TArray* Array, const size_t Element);
void* TArray_Pop(TArray* Array);
void TArray_Reserve(TArray* Array, size_t Length);
void TArray_Push(TArray* Array, void* Element);
void TArray_Remove(TArray* Array, size_t Element);
void TArray_Empty(TArray* Array);
void TArray_From_CArray(TArray* Array, void* Data, size_t Elements);
void TArray_Sort(TArray* Array, int (*Comparator)(void*, void*));

#endif