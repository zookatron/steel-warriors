#pragma once

#include "TArray.h"
#include "TRenderer.h"

typedef uint32_t TObjectID;
#define MaxNumObjects 65535 //((1 << 16)-1)

typedef struct {
	TArray Objects;
	uint16_t NextFree;
	uint16_t NumElements;
	size_t ElementSize;
} TObjectManager;

void TObjectManager_Initialize(TObjectManager* ObjectManager, const size_t ElementSize);
void TObjectManager_Destroy(TObjectManager* ObjectManager);
TObjectID TObjectManager_Add(TObjectManager* ObjectManager, void* Object);
void TObjectManager_Remove(TObjectManager* ObjectManager, const TObjectID ObjectID);
void* TObjectManager_Get(TObjectManager* ObjectManager, const TObjectID ObjectID);
TObjectID TObjectManager_GetID(TObjectManager* ObjectManager, const void* Object);
void* TObjectManager_Next(TObjectManager* ObjectManager, void* Object);
