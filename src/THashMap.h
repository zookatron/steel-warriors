#ifndef THashMap_H
#define THashMap_H

#include "TArray.h"
#include "defines.h"

typedef struct {
	TArray Elements;
} THashMap;

void THashMap_Initialize(THashMap* HashMap, const size_t ElementSize);
void THashMap_Destroy(THashMap* HashMap);
void THashMap_Add(THashMap* HashMap, char* Name, void* Element);
void THashMap_Remove(THashMap* HashMap, const char* Name);
void* THashMap_Get(THashMap* HashMap, const char* Name);
void* THashMap_Next(THashMap* HashMap, void* Element);

#endif