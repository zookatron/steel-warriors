#ifndef TTransform_H
#define TTransform_H

#include "TVector3D.h"

typedef struct {
	float Matrix[4][4];
} TTransform;

void TTransform_Identity(TTransform* Transform);
void TTransform_Copy(TTransform* Dest, const TTransform* Src);
void TTransform_Multiply(TTransform* Transform, const TTransform* Other);
void TTransform_Inverse(TTransform* Transform);
void TTransform_Translate_X(TTransform* Transform, const float Amount);
void TTransform_Translate_Y(TTransform* Transform, const float Amount);
void TTransform_Translate_Z(TTransform* Transform, const float Amount);
void TTransform_Translate_XYZ(TTransform* Transform, const float X, const float Y, const float Z);
void TTransform_Translate(TTransform* Transform, const TVector3D* Amount);
void TTransform_Set_Translation(TTransform* Transform, const TVector3D* Translation);
void TTransform_Get_Translation(const TTransform* Transform, TVector3D* Translation);
void TTransform_Get_Forward(const TTransform* Transform, TVector3D* Translation);
void TTransform_Get_Up(const TTransform* Transform, TVector3D* Translation);
void TTransform_Get_Right(const TTransform* Transform, TVector3D* Translation);
void TTransform_Rotate_X(TTransform* Transform, const float Angle);
void TTransform_Rotate_Y(TTransform* Transform, const float Angle);
void TTransform_Rotate_Z(TTransform* Transform, const float Angle);
void TTransform_Rotate_XYZ(TTransform* Transform, const float X, const float Y, const float Z);
void TTransform_Rotate(TTransform* Transform, const TVector3D* Angles);
void TTransform_Scale_X(TTransform* Transform, const float Amount);
void TTransform_Scale_Y(TTransform* Transform, const float Amount);
void TTransform_Scale_Z(TTransform* Transform, const float Amount);
void TTransform_Scale_XYZ(TTransform* Transform, const float X, const float Y, const float Z);
void TTransform_Scale(TTransform* Transform, const TVector3D* Amount);
void TTransform_Scale_Uniform(TTransform* Transform, const float Amount);
void TVector3D_Transform(TVector3D* Vector, const TTransform* Transform);
void TTransform_PointAt(TTransform* Transform, TVector3D* From, TVector3D* To);

#endif
