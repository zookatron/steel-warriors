#include "TNetworkManager.h"
#include <stdio.h>

typedef TPlayerID TNetworkID;

typedef struct {
	TPlayerID PlayerID;
	float LastShipUpdate;
} TPlayerInfo;

typedef struct {
	TConnectionID ConnectionID;
	TNetworkID NetworkID;
	TPlayerID PlayerID;
	TShipID PlayerShipID;
	TArray PlayersInfo;
} TNetworkPlayer;

typedef enum { NetworkMessage_GameInitial, NetworkMessage_PlayerInitial, NetworkMessage_PlayerQuit, NetworkMessage_ShipInitial, NetworkMessage_PlayerUpdate, NetworkMessage_ShipUpdate } TNetworkMessage;

typedef struct {
	TNetworkID NetworkID;
	TShipPosition Position;
	TShipInput Input;
} TNetworkMessage_ShipUpdate;

typedef struct { TNetworkID NewNetworkID; } TNetworkMessage_GameInitial;

typedef struct { TNetworkID NetworkID; } TNetworkMessage_PlayerInitial;

typedef struct {
	TNetworkID NetworkID;
	TShipPosition Position;
} TNetworkMessage_ShipInitial;

typedef struct { TNetworkID NetworkID; } TNetworkMessage_Player;

void TNetworkManager_SendMessage(TNetworkManager* NetworkManager, TConnectionID ConnectionID, TNetworkMessage Message, const void* Data, size_t DataSize) {
	if (DataSize >= 1024) {
		assert(false);
	}
	char Buffer[1024];
	size_t BufferSize = DataSize + 1;
	Buffer[0] = Message;
	if (DataSize > 0) {
		assert(Data);
		memcpy(Buffer + 1, Data, DataSize);
	}
	TSocket_Send(&NetworkManager->Socket, ConnectionID, Buffer, BufferSize);
}

void TNetworkManager_DistributeMessage(TNetworkManager* NetworkManager, TNetworkMessage Message, void* Data, size_t Length, TConnectionID Exclude) {
	int Index;
	for (Index = 0; Index < NetworkManager->NetworkPlayers.Length; Index++) {
		TNetworkPlayer* Player = TArray_Get(&NetworkManager->NetworkPlayers, Index);
		if (Player->ConnectionID == 0) {
			continue;
		}
		if (Player->ConnectionID == Exclude) {
			continue;
		}
		TNetworkManager_SendMessage(NetworkManager, Player->ConnectionID, Message, Data, Length);
	}
}

TNetworkPlayer* TNetworkManager_PlayerFromConnectionID(TNetworkManager* NetworkManager, TConnectionID ConnectionID) {
	int Index;
	for (Index = 0; Index < NetworkManager->NetworkPlayers.Length; Index++) {
		TNetworkPlayer* NetworkPlayer = TArray_Get(&NetworkManager->NetworkPlayers, Index);
		if (NetworkPlayer->ConnectionID == ConnectionID) {
			return NetworkPlayer;
		}
	}
	return 0;
}

TNetworkPlayer* TNetworkManager_PlayerFromNetworkID(TNetworkManager* NetworkManager, TNetworkID NetworkID) {
	int Index;
	for (Index = 0; Index < NetworkManager->NetworkPlayers.Length; Index++) {
		TNetworkPlayer* NetworkPlayer = TArray_Get(&NetworkManager->NetworkPlayers, Index);
		if (NetworkPlayer->NetworkID == NetworkID) {
			return NetworkPlayer;
		}
	}
	return 0;
}

void TNetworkManager_SetupNewClient(TNetworkManager* NetworkManager, TNetworkPlayer* NewNetworkPlayer, TGameState* GameState) {
	TNetworkMessage_GameInitial GameInitial;
	GameInitial.NewNetworkID = NewNetworkPlayer->NetworkID;
	TNetworkManager_SendMessage(NetworkManager, NewNetworkPlayer->ConnectionID, NetworkMessage_GameInitial, &GameInitial, sizeof(TNetworkMessage_GameInitial));

	TNetworkMessage_PlayerInitial NewNetworkPlayerInitial;
	NewNetworkPlayerInitial.NetworkID = NewNetworkPlayer->NetworkID;

	int Index;
	for (Index = 0; Index < NetworkManager->NetworkPlayers.Length; Index++) {
		TNetworkPlayer* NetworkPlayer = TArray_Get(&NetworkManager->NetworkPlayers, Index);
		if (NetworkPlayer->ConnectionID == NewNetworkPlayer->ConnectionID) {
			continue;
		}

		TNetworkMessage_PlayerInitial PlayerInitial;
		PlayerInitial.NetworkID = NetworkPlayer->NetworkID;
		TNetworkManager_SendMessage(NetworkManager, NewNetworkPlayer->ConnectionID, NetworkMessage_PlayerInitial, &PlayerInitial, sizeof(PlayerInitial));

		if (NetworkPlayer->ConnectionID != 0) {
			TNetworkManager_SendMessage(NetworkManager, NetworkPlayer->ConnectionID, NetworkMessage_PlayerInitial, &NewNetworkPlayerInitial, sizeof(NewNetworkPlayerInitial));
		}

		TShip* Ship = TGameState_GetShip(GameState, NetworkPlayer->PlayerShipID);
		if (Ship) {
			TNetworkMessage_ShipInitial ShipInitial;
			ShipInitial.NetworkID = NetworkPlayer->NetworkID;
			ShipInitial.Position = Ship->Position;
			TNetworkManager_SendMessage(NetworkManager, NewNetworkPlayer->ConnectionID, NetworkMessage_ShipInitial, &ShipInitial, sizeof(ShipInitial));
		}
	}
}

void TNetworkManager_AddNetworkPlayer(TNetworkManager* NetworkManager, TConnectionID ConnectionID, TNetworkID NetworkID, TPlayerID PlayerID, TGameState* GameState) {
	TNetworkPlayer NewPlayer;
	NewPlayer.ConnectionID = ConnectionID;
	NewPlayer.NetworkID = NetworkID;
	NewPlayer.PlayerID = PlayerID;
	NewPlayer.PlayerShipID = 0;
	TArray_Initialize(&NewPlayer.PlayersInfo, sizeof(TPlayerInfo));

	int Index;
	for (Index = 0; Index < NetworkManager->NetworkPlayers.Length; Index++) {
		TNetworkPlayer* OldPlayer = TArray_Get(&NetworkManager->NetworkPlayers, Index);
		TPlayerInfo NewPlayerInfo, OldPlayerInfo;

		NewPlayerInfo.PlayerID = PlayerID;
		NewPlayerInfo.LastShipUpdate = 0;

		OldPlayerInfo.PlayerID = OldPlayer->PlayerID;
		OldPlayerInfo.LastShipUpdate = 0;

		TArray_Push(&NewPlayer.PlayersInfo, &OldPlayerInfo);
		TArray_Push(&OldPlayer->PlayersInfo, &NewPlayerInfo);
	}

	TArray_Push(&NetworkManager->NetworkPlayers, &NewPlayer);
	if (NetworkManager->IsServer && ConnectionID != 0) {
		TNetworkManager_SetupNewClient(NetworkManager, &NewPlayer, GameState);
	}
}

void TNetworkManager_ServerRemoveNetworkPlayer(TNetworkManager* NetworkManager, TConnectionID ConnectionID, TGameState* GameState) {
	TNetworkPlayer* RemovedPlayer = TNetworkManager_PlayerFromConnectionID(NetworkManager, ConnectionID);
	if (!RemovedPlayer) {
		return;
	}
	TGameState_RemovePlayer(GameState, RemovedPlayer->PlayerID);
	TArray_Destroy(&RemovedPlayer->PlayersInfo);

	TNetworkMessage_Player PlayerQuit;
	PlayerQuit.NetworkID = RemovedPlayer->PlayerID;

	TNetworkManager_DistributeMessage(NetworkManager, NetworkMessage_PlayerQuit, &PlayerQuit, sizeof(PlayerQuit), ConnectionID);

	int RemovedPlayerIndex = -1, Index, InfoIndex;
	for (Index = 0; Index < NetworkManager->NetworkPlayers.Length; Index++) {
		TNetworkPlayer* Player = TArray_Get(&NetworkManager->NetworkPlayers, Index);
		if (Player->ConnectionID == ConnectionID) {
			RemovedPlayerIndex = Index;
		} else {
			for (InfoIndex = 0; InfoIndex < Player->PlayersInfo.Length; InfoIndex++) {
				TPlayerInfo* PlayerInfo = TArray_Get(&Player->PlayersInfo, InfoIndex);
				if (PlayerInfo->PlayerID == RemovedPlayer->PlayerID) {
					TArray_Remove(&Player->PlayersInfo, InfoIndex);
					break;
				}
			}
		}
	}
	if (RemovedPlayerIndex != -1) {
		TArray_Remove(&NetworkManager->NetworkPlayers, RemovedPlayerIndex);
	}
}

void TNetworkManager_Initialize_Client(TNetworkManager* NetworkManager) {
	TSocket_Initialize(&NetworkManager->Socket, 6633);
	TMessageStream_Initialize(&NetworkManager->SocketMessages);
	NetworkManager->IsServer = false;
	NetworkManager->Server = TSocket_Connect(&NetworkManager->Socket, "127.0.0.1", 6634);
	TArray_Initialize(&NetworkManager->NetworkPlayers, sizeof(TNetworkPlayer));
	NetworkManager->ClientNetworkID = 0;
	NetworkManager->LastUpdate = 0;
	NetworkManager->Now = 0;
	NetworkManager->Connected = false;
}

void TNetworkManager_Initialize_Server(TNetworkManager* NetworkManager, TGameState* GameState) {
	TSocket_Initialize(&NetworkManager->Socket, 6634);
	TMessageStream_Initialize(&NetworkManager->SocketMessages);
	NetworkManager->IsServer = true;
	TArray_Initialize(&NetworkManager->NetworkPlayers, sizeof(TNetworkPlayer));
	NetworkManager->ClientNetworkID = 0;
	NetworkManager->LastUpdate = 0;
	NetworkManager->Now = 0;
	TNetworkManager_AddNetworkPlayer(NetworkManager, 0, GameState->LocalPlayer, GameState->LocalPlayer, GameState);
}

void TNetworkManager_Destroy(TNetworkManager* NetworkManager) {
	TSocket_Destroy(&NetworkManager->Socket);
	TMessageStream_Destroy(&NetworkManager->SocketMessages);
	TArray_Destroy(&NetworkManager->NetworkPlayers);
	// todo: Make sure PlayersInfo arrays get cleaned up
}

void TNetworkManager_Update(TNetworkManager* NetworkManager, TGameState* GameState, float Delta) {
	TSocket_Update(&NetworkManager->Socket, &NetworkManager->SocketMessages);

	NetworkManager->Now += Delta;
	if (NetworkManager->Now - NetworkManager->LastUpdate > 0.1) {
		// display_string("UPDATE");
		NetworkManager->LastUpdate = NetworkManager->Now;
		if (NetworkManager->IsServer) {
			TNetworkMessage_ShipUpdate ShipUpdates[GameState->Players.NumElements];

			TPlayer* Player = 0;
			int Index = 0;
			while ((Player = TObjectManager_Next(&GameState->Players, Player))) {
				TShip* Ship = TObjectManager_Get(&GameState->Ships, Player->ShipID);
				if (Ship) {
					ShipUpdates[Index].NetworkID = TObjectManager_GetID(&GameState->Players, Player);
					ShipUpdates[Index].Position = Ship->Position;
					ShipUpdates[Index].Input = Ship->Input;
				} else {
					ShipUpdates[Index].NetworkID = 0;
				}
				Index++;
			}

			int InnerIndex;
			for (Index = 0; Index < NetworkManager->NetworkPlayers.Length; Index++) {
				TNetworkPlayer* NetworkPlayer = TArray_Get(&NetworkManager->NetworkPlayers, Index);

				TPlayer* Player = TGameState_GetPlayer(GameState, NetworkPlayer->PlayerID);
				if (NetworkPlayer->PlayerShipID != Player->ShipID) {
					NetworkPlayer->PlayerShipID = Player->ShipID;
					if (Player->ShipID != 0) {
						TShip* Ship = TGameState_GetShip(GameState, Player->ShipID);
						TNetworkMessage_ShipInitial ShipInitial;
						ShipInitial.NetworkID = NetworkPlayer->NetworkID;
						ShipInitial.Position = Ship->Position;

						for (InnerIndex = 0; InnerIndex < NetworkManager->NetworkPlayers.Length; InnerIndex++) {
							TNetworkPlayer* OtherNetworkPlayer = TArray_Get(&NetworkManager->NetworkPlayers, InnerIndex);
							if (OtherNetworkPlayer->ConnectionID == 0) {
								continue;
							}
							memset(&ShipInitial, 0, 0);
							TNetworkManager_SendMessage(NetworkManager, OtherNetworkPlayer->ConnectionID, NetworkMessage_ShipInitial, &ShipInitial, sizeof(ShipInitial));
						}
					}
				}

				if (NetworkPlayer->ConnectionID == 0) {
					continue;
				}
				for (InnerIndex = 0; InnerIndex < GameState->Players.NumElements; InnerIndex++) {
					if (ShipUpdates[InnerIndex].NetworkID == 0) {
						continue;
					}
					if (ShipUpdates[InnerIndex].NetworkID == NetworkPlayer->NetworkID) {
						continue;
					}
					TNetworkManager_SendMessage(NetworkManager, NetworkPlayer->ConnectionID, NetworkMessage_ShipUpdate, &ShipUpdates[InnerIndex], sizeof(TNetworkMessage_ShipUpdate));
				}
			}
		} else if (NetworkManager->ClientNetworkID && NetworkManager->Connected) {
			TShip* Ship = TGameState_GetPlayerShip(GameState, GameState->LocalPlayer);
			if (Ship) {
				TNetworkMessage_ShipUpdate ShipUpdate;
				ShipUpdate.NetworkID = NetworkManager->ClientNetworkID;
				ShipUpdate.Position = Ship->Position;
				ShipUpdate.Input = Ship->Input;
				TNetworkManager_SendMessage(NetworkManager, NetworkManager->Server, NetworkMessage_ShipUpdate, &ShipUpdate, sizeof(ShipUpdate));
			}
		}
	}

	TMessage SocketMessage = 0;
	while ((SocketMessage = TMessageStream_NextMessage(&NetworkManager->SocketMessages, SocketMessage))) {
		if (TMessage_GetType(SocketMessage) == SocketMessage_Data) {
			const void* MessageData = TMessage_GetData(SocketMessage);
			TConnectionID ConnectionID = *(TConnectionID*)MessageData;
			MessageData += sizeof(TConnectionID);
			TNetworkMessage MessageType = *(TNetworkMessage*)MessageData;
			MessageData += sizeof(TNetworkMessage);

			if (MessageType == NetworkMessage_ShipUpdate) {
				// display_string("NetworkMessage_ShipUpdate");
				const TNetworkMessage_ShipUpdate* ShipUpdate = (TNetworkMessage_ShipUpdate*)MessageData;
				if (NetworkManager->IsServer) {
					TNetworkPlayer* Player = TNetworkManager_PlayerFromConnectionID(NetworkManager, ConnectionID);
					assert(Player);
					TShip* Ship = TGameState_GetPlayerShip(GameState, Player->PlayerID);
					if (Ship) {
						Ship->Position = ShipUpdate->Position;
						Ship->Input = ShipUpdate->Input;

						TNetworkMessage_ShipUpdate ShipUpdateDist = *ShipUpdate;
						ShipUpdateDist.NetworkID = Player->PlayerID;
						TNetworkManager_DistributeMessage(NetworkManager, NetworkMessage_ShipUpdate, &ShipUpdateDist, sizeof(TNetworkMessage_ShipUpdate), ConnectionID);
					}
				} else {
					TNetworkPlayer* Player = TNetworkManager_PlayerFromNetworkID(NetworkManager, ShipUpdate->NetworkID);
					if (!Player) {
						continue;
					}
					TShip* Ship = TGameState_GetPlayerShip(GameState, Player->PlayerID);
					if (Ship) {
						Ship->Position = ShipUpdate->Position;
						Ship->Input = ShipUpdate->Input;
					}
				}
			} else if (MessageType == NetworkMessage_GameInitial && !NetworkManager->IsServer) {
				// display_string("NetworkMessage_GameInitial");
				const TNetworkMessage_GameInitial* GameInitial = (TNetworkMessage_GameInitial*)MessageData;
				NetworkManager->ClientNetworkID = GameInitial->NewNetworkID;
				TNetworkManager_AddNetworkPlayer(NetworkManager, 0, GameInitial->NewNetworkID, GameState->LocalPlayer, GameState);
				NetworkManager->Connected = true;
			} else if (MessageType == NetworkMessage_PlayerInitial && !NetworkManager->IsServer) {
				// display_string("NetworkMessage_PlayerInitial");
				const TNetworkMessage_PlayerInitial* PlayerInitial = (TNetworkMessage_PlayerInitial*)MessageData;
				TNetworkManager_AddNetworkPlayer(NetworkManager, 0, PlayerInitial->NetworkID, TGameState_AddPlayer(GameState), GameState);
			} else if (MessageType == NetworkMessage_ShipInitial && !NetworkManager->IsServer) {
				// display_string("NetworkMessage_ShipInitial");
				const TNetworkMessage_ShipInitial* ShipInitial = (TNetworkMessage_ShipInitial*)MessageData;
				TNetworkPlayer* NetworkPlayer = TNetworkManager_PlayerFromNetworkID(NetworkManager, ShipInitial->NetworkID);
				if (!NetworkPlayer) {
					continue;
				}
				TPlayer* Player = TGameState_GetPlayer(GameState, NetworkPlayer->PlayerID);
				Player->ShipID = TGameState_CreateShip(GameState);
				TShip* Ship = TGameState_GetShip(GameState, Player->ShipID);
				Ship->Position = ShipInitial->Position;
			} else {
				printf("ConnectionID: %d, MessageType: %d\n", ConnectionID, MessageType);
				assert(false);
			}
		} else if (TMessage_GetType(SocketMessage) == SocketMessage_Connect && NetworkManager->IsServer) {
			TPlayerID PlayerID = TGameState_AddPlayer(GameState);
			TConnectionID ConnectionID = *(TConnectionID*)TMessage_GetData(SocketMessage);
			TNetworkManager_AddNetworkPlayer(NetworkManager, ConnectionID, PlayerID, PlayerID, GameState);
		} else if (TMessage_GetType(SocketMessage) == SocketMessage_Disconnect) {
			if (NetworkManager->IsServer) {
				TConnectionID ConnectionID = *(TConnectionID*)TMessage_GetData(SocketMessage);
				TNetworkManager_ServerRemoveNetworkPlayer(NetworkManager, ConnectionID, GameState);
			} else {
				int Index;
				for (Index = 0; Index < NetworkManager->NetworkPlayers.Length; Index++) {
					TNetworkPlayer* Player = TArray_Get(&NetworkManager->NetworkPlayers, Index);
					if (Player->PlayerID != GameState->LocalPlayer) {
						TGameState_RemovePlayer(GameState, Player->PlayerID);
					}
				}
				TArray_Empty(&NetworkManager->NetworkPlayers);
				NetworkManager->Connected = false;
			}
		}
	}

	TMessageStream_Empty(&NetworkManager->SocketMessages);
}
