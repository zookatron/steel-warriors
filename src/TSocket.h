#ifndef TSocket_H
#define TSocket_H

#include "TArray.h"
#include "TMessageStream.h"
#include "TObjectManager.h"

typedef TObjectID TConnectionID;

typedef enum {
	SocketMessage_None,
	SocketMessage_Data,
	SocketMessage_Connect,
	SocketMessage_ConnectConfirm,
	SocketMessage_Disconnect
} TSocketMessage;

struct TSocket {
	unsigned long long int Socket;
	TObjectManager Connections;
};
typedef struct TSocket TSocket;

void TSocket_Initialize(TSocket* Socket, int Port);
TConnectionID TSocket_Connect(TSocket* Socket, const char* IP, int Port);
void TSocket_Disconnect(TSocket* Socket, TConnectionID ConnectionID);
void TSocket_Destroy(TSocket* Socket);
void TSocket_Update(TSocket* Socket, TMessageStream* SocketMessages);
void TSocket_Send(TSocket* Socket, TConnectionID ConnectionID, const void* Data, size_t DataSize);

#endif