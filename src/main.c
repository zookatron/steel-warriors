#include <stdio.h>
#include "TArray.h"
#include "TGUI.h"
#include "TGameState.h"
#include "TMessageStream.h"
#include "TNetworkManager.h"
#include "TProjectile.h"
#include "TTransform.h"
#include "TVector2D.h"
#include "TVector3D.h"
#include "TVector3D.h"
#include "TWindow.h"
#include "defines.h"

/*void CreateProp() {
    TMeshID RockMeshID = TRenderer_Load_Mesh(&Renderer, "assets/Meshes/Objects/rock.obj");
    TRenderSceneNodeID RockSceneNodeID = TRenderScene_Add(&Scene, 0, RockMeshID);
    TTransform_Translate_X(TRenderScene_GetLocalTransform(&Scene, RockSceneNodeID), 100.0f);
}*/

typedef enum { TNetworkMode_None, TNetworkMode_Server, TNetworkMode_Client } TNetworkMode;

typedef struct { TNetworkMode NetworkMode; } TGameInfo;

void StartGame(TRenderer* Renderer, TWindow* Window, TGameInfo* GameInfo) {
	TGameState GameState;
	TGameState_Initialize(&GameState, Renderer, GameInfo->NetworkMode == TNetworkMode_Client ? false : true);
	TResourceManager_LoadResourcePack(&GameState.ResourceManager, "assets/Falcon.res");

	TNetworkManager NetworkManager;
	if (GameInfo->NetworkMode == TNetworkMode_Server) {
		TNetworkManager_Initialize_Server(&NetworkManager, &GameState);
	}
	if (GameInfo->NetworkMode == TNetworkMode_Client) {
		TNetworkManager_Initialize_Client(&NetworkManager);
	}

	TInputState InputState;
	TInputState_Initialize(&InputState);

	TMessageStream WindowMessages;
	TMessageStream_Initialize(&WindowMessages);

	TTimer Timer;
	TTimer_Initialize(&Timer);
	uint16_t Frames = 0;
	char AverageFPS[32] = {0};
	float Accumulator = 0;
	float TimeStep = 0.01f;
	TVector2D TextPos;
	TVector2D_Set(&TextPos, 10, 40);

	while (!InputState.KeyDown[Key_Escape]) {
		Thread_Sleep(10);
		TMessageStream_Empty(&WindowMessages);
		TWindow_Update(Window, &WindowMessages);
		TInputState_Update(&InputState, &WindowMessages);

		if (InputState.KeyPressed[Key_Tab]) {
			TWindow_Capture_Mouse(Window, !Window->MouseCaptured);
		}

		if (GameInfo->NetworkMode != TNetworkMode_None) {
			TNetworkManager_Update(&NetworkManager, &GameState, TimeStep);
		}

		TShip* PlayerShip = TGameState_GetPlayerShip(&GameState, GameState.LocalPlayer);
		if (PlayerShip && Window->MouseCaptured) {
			GetShipInput(&InputState, PlayerShip, &PlayerShip->Input);
		}

		Accumulator += TTimer_Interval(&Timer);
		while (Accumulator >= TimeStep) {
			TGameState_Step(&GameState, TimeStep);
			Accumulator -= TimeStep;
		}

		if (PlayerShip) {
			TGameState_CameraFollow(&GameState, PlayerShip);
		}

		TGameState_Render(&GameState, Renderer);

		TRenderer_DrawText(Renderer, AverageFPS, TextPos);
		TRenderer_SwapBuffers(Renderer);

		Frames++;
		if (TTimer_GetTime(&Timer) > 0.999f) {
			sprintf(AverageFPS, "%d", (int)Frames);
			Frames = 0;
			TTimer_Restart(&Timer);
		}
	}

	if (GameInfo->NetworkMode != TNetworkMode_None) {
		TNetworkManager_Destroy(&NetworkManager);
	}

	TWindow_Capture_Mouse(Window, false);
	TMessageStream_Destroy(&WindowMessages);
	TGameState_Destroy(&GameState);
}

int main() {
	TWindow Window;
	TWindow_Initialize(&Window, 1200, 900);

	TMessageStream WindowMessages, GUIMessages;
	TMessageStream_Initialize(&WindowMessages);
	TMessageStream_Initialize(&GUIMessages);

	TInputState InputState;
	TInputState_Initialize(&InputState);

	TRenderer Renderer;
	TRenderer_Initialize(&Renderer, &Window);

	TGUI MainMenuGUI;
	TGUI_Initialize(&MainMenuGUI);

	TButtonInfo ButtonInfo;
	ButtonInfo.Rect.X = 10;
	ButtonInfo.Rect.Y = 10;
	ButtonInfo.Rect.Width = 200;
	ButtonInfo.Rect.Height = 50;
	ButtonInfo.Text = "Start";
	TControlID StartButtonID = TGUI_CreateButton(&MainMenuGUI, &ButtonInfo);
	ButtonInfo.Rect.X = 10;
	ButtonInfo.Rect.Y = 70;
	ButtonInfo.Rect.Width = 200;
	ButtonInfo.Rect.Height = 50;
	ButtonInfo.Text = "Server";
	TControlID ServerButtonID = TGUI_CreateButton(&MainMenuGUI, &ButtonInfo);
	ButtonInfo.Rect.X = 10;
	ButtonInfo.Rect.Y = 130;
	ButtonInfo.Rect.Width = 200;
	ButtonInfo.Rect.Height = 50;
	ButtonInfo.Text = "Client";
	TControlID ClientButtonID = TGUI_CreateButton(&MainMenuGUI, &ButtonInfo);

	while (!InputState.KeyPressed[Key_Escape]) {
		Thread_Sleep(10);
		TMessageStream_Empty(&WindowMessages);
		TWindow_Update(&Window, &WindowMessages);
		TInputState_Update(&InputState, &WindowMessages);
		TMessageStream_Empty(&GUIMessages);
		TGUI_Update(&MainMenuGUI, &WindowMessages, &GUIMessages);

		const uint8_t* GUIMessage = 0;
		while ((GUIMessage = TMessageStream_NextMessage(&GUIMessages, GUIMessage))) {
			if (*GUIMessage == GUIMessage_ButtonClick) {
				const TControlID* ControlID = TMessage_GetData(GUIMessage);
				if (*ControlID == StartButtonID) {
					TGameInfo GameInfo;
					GameInfo.NetworkMode = TNetworkMode_None;
					StartGame(&Renderer, &Window, &GameInfo);
					break;
				} else if (*ControlID == ServerButtonID) {
					TGameInfo GameInfo;
					GameInfo.NetworkMode = TNetworkMode_Server;
					StartGame(&Renderer, &Window, &GameInfo);
					break;
				} else if (*ControlID == ClientButtonID) {
					TGameInfo GameInfo;
					GameInfo.NetworkMode = TNetworkMode_Client;
					StartGame(&Renderer, &Window, &GameInfo);
					break;
				}
			}
		}

		TGUI_Render(&MainMenuGUI, &Renderer);
		TRenderer_SwapBuffers(&Renderer);
	}

	TGUI_Destroy(&MainMenuGUI);
	TRenderer_Destroy(&Renderer);
	TMessageStream_Destroy(&WindowMessages);
	TWindow_Destroy(&Window);

	return 0;
}
