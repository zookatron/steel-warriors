#ifndef TClouds_H
#define TClouds_H

#include "TRenderer.h"

typedef struct {
	TMeshID TestMesh;
	TMeshID CloudMesh;
	TMaterialID CloudMaterial;
	TArray Impostors;
} TClouds;

void TClouds_Initialize(TClouds* Clouds, TRenderer* Renderer);
void TClouds_Render(TClouds* Clouds, TRenderer* Renderer);
void TClouds_Destroy(TClouds* Clouds, TRenderer* Renderer);

#endif