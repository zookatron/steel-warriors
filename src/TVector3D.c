#include "TVector3D.h"
#include <math.h>

void TVector3D_Copy(TVector3D* Dest, const TVector3D* Src) {
	Dest->X = Src->X;
	Dest->Y = Src->Y;
	Dest->Z = Src->Z;
}

void TVector3D_Initialize(TVector3D* Vector) {
	Vector->X = Vector->Y = Vector->Z = 0;
}

void TVector3D_Set(TVector3D* Vector, float X, float Y, float Z) {
	Vector->X = X;
	Vector->Y = Y;
	Vector->Z = Z;
}

bool TVector3D_Equals(const TVector3D* First, const TVector3D* Second) {
	return First->X == Second->X && First->Y == Second->Y && First->Z == Second->Z;
}

void TVector3D_Add(TVector3D* Vector, const TVector3D* Other) {
	Vector->X += Other->X;
	Vector->Y += Other->Y;
	Vector->Z += Other->Z;
}

void TVector3D_Subtract(TVector3D* Vector, const TVector3D* Other) {
	Vector->X -= Other->X;
	Vector->Y -= Other->Y;
	Vector->Z -= Other->Z;
}

void TVector3D_Multiply(TVector3D* Vector, const TVector3D* Other) {
	Vector->X *= Other->X;
	Vector->Y *= Other->Y;
	Vector->Z *= Other->Z;
}

void TVector3D_Scale(TVector3D* Vector, const float Factor) {
	Vector->X *= Factor;
	Vector->Y *= Factor;
	Vector->Z *= Factor;
}

void TVector3D_Normalize(TVector3D* Vector) {
	float Len = TVector3D_Length(Vector);
	if (Len != 0.0f) {
		Vector->X /= Len;
		Vector->Y /= Len;
		Vector->Z /= Len;
	}
}

void TVector3D_Rotate(TVector3D* Vector, const TVector3D* Angles) {
	TVector3D_Rotate_Z(Vector, Angles->Z);
	TVector3D_Rotate_X(Vector, Angles->X);
	TVector3D_Rotate_Y(Vector, Angles->Y);
}

void TVector3D_Rotate_X(TVector3D* Vector, const float Angle) {
	float CosAngle = cos(Angle / 180.0f * 3.1415f);
	float SinAngle = sin(Angle / 180.0f * 3.1415f);
	float NewY = Vector->Y * CosAngle - Vector->Z * SinAngle;
	float NewZ = Vector->Y * SinAngle + Vector->Z * CosAngle;
	Vector->Y = NewY;
	Vector->Z = NewZ;
}

void TVector3D_Rotate_Y(TVector3D* Vector, const float Angle) {
	float CosAngle = cos(Angle / 180.0f * 3.1415f);
	float SinAngle = sin(Angle / 180.0f * 3.1415f);
	float NewZ = Vector->Z * CosAngle - Vector->X * SinAngle;
	float NewX = Vector->Z * SinAngle + Vector->X * CosAngle;
	Vector->Z = NewZ;
	Vector->X = NewX;
}

void TVector3D_Rotate_Z(TVector3D* Vector, const float Angle) {
	float CosAngle = cos(Angle / 180.0f * 3.1415f);
	float SinAngle = sin(Angle / 180.0f * 3.1415f);
	float NewX = Vector->X * CosAngle - Vector->Y * SinAngle;
	float NewY = Vector->X * SinAngle + Vector->Y * CosAngle;
	Vector->X = NewX;
	Vector->Y = NewY;
}

float TVector3D_Dot(const TVector3D* First, const TVector3D* Second) {
	return First->X * Second->X + First->Y * Second->Y + First->Z * Second->Z;
}

float TVector3D_Length(const TVector3D* Vector) {
	return sqrt(Vector->X * Vector->X + Vector->Y * Vector->Y + Vector->Z * Vector->Z);
}

float TVector3D_LengthSquared(const TVector3D* Vector) {
	return Vector->X * Vector->X + Vector->Y * Vector->Y + Vector->Z * Vector->Z;
}
