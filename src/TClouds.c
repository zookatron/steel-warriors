#include "TClouds.h"
#include "math.h"

typedef struct {
	float DistanceToCamera;
	TVector3D Position;
} TParticle;

typedef struct {
	TArray Particles;
	TArray ParticlePositions;
	TVector3D Position;
	TTextureID ImpostorTexture;
	TMeshID ImpostorMesh;
	float Radius;
	float PrevHAngle;
	float PrevVAngle;
	float PrevDist;
} TImpostor;

void TClouds_Initialize(TClouds* Clouds, TRenderer* Renderer) {
	TTransform Transform;
	TShaderID TShaderID;
	TTextureID TextureID;
	TMaterialID MaterialID;
	TGeometryID GeometryID;
	int ImpostorIndex, ParticleIndex;

	int NumImpostors = 10;
	int NumParticles = 100;
	float PuffSize = 10;
	float Radius = 20;
	float MaxSize = (Radius + PuffSize) * 2.0f;

	TShaderID = TRenderer_Load_Shader(Renderer, "assets/Shaders/simple.vert", "assets/Shaders/simple.frag");
	TextureID = TRenderer_Load_Texture(Renderer, "assets/Textures/grass.png");
	MaterialID = TRenderer_Create_Material(Renderer, TShaderID, TextureID);
	GeometryID = TRenderer_Create_Sphere_Geometry(Renderer, MaxSize * 0.5, 100, 100, 0, 0);
	Clouds->TestMesh = TRenderer_Create_Mesh(Renderer, GeometryID, MaterialID);

	TShaderID = TRenderer_Load_Shader(Renderer, "assets/Shaders/particles.vert", "assets/Shaders/particles.frag");
	TextureID = TRenderer_Load_Texture(Renderer, "assets/Textures/blur.png");
	Clouds->CloudMaterial = TRenderer_Create_Material(Renderer, TShaderID, TextureID);
	TRenderer_Set_Material_Culling(Renderer, MaterialID, Culling_None);

	GeometryID = TRenderer_Create_Plane_Geometry(Renderer, 1, 1, 1.0f);
	TTransform_Identity(&Transform);
	TTransform_Translate_XYZ(&Transform, -0.5f, 0, -0.5f);
	TRenderer_Transform_Geometry(Renderer, GeometryID, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Scale_XYZ(&Transform, PuffSize, PuffSize, PuffSize);
	TRenderer_Transform_Geometry(Renderer, GeometryID, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Rotate_X(&Transform, M_PI * 0.5);
	TRenderer_Transform_Geometry(Renderer, GeometryID, &Transform);

	Clouds->CloudMesh = TRenderer_Create_Mesh(Renderer, GeometryID, Clouds->CloudMaterial);

	TShaderID = TRenderer_Load_Shader(Renderer, "assets/Shaders/simple.vert", "assets/Shaders/simple.frag");

	GeometryID = TRenderer_Create_Plane_Geometry(Renderer, 1, 1, 1.0f);
	TTransform_Identity(&Transform);
	TTransform_Translate_XYZ(&Transform, -0.5f, 0, -0.5f);
	TRenderer_Transform_Geometry(Renderer, GeometryID, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Scale_XYZ(&Transform, -1, 0, -1);
	TRenderer_Transform_Geometry(Renderer, GeometryID, &Transform);

	TTransform_Identity(&Transform);
	TTransform_Rotate_X(&Transform, M_PI * 0.5);
	TRenderer_Transform_Geometry(Renderer, GeometryID, &Transform);

	TArray_Initialize(&Clouds->Impostors, sizeof(TImpostor));
	for (ImpostorIndex = 0; ImpostorIndex < NumImpostors; ImpostorIndex++) {
		TImpostor NewImpostor;

		NewImpostor.Radius = MaxSize * 0.5f;
		NewImpostor.PrevHAngle = -1000;
		NewImpostor.PrevVAngle = -1000;

		NewImpostor.ImpostorTexture = TRenderer_Create_Texture(Renderer, 512, 512);
		MaterialID = TRenderer_Create_Material(Renderer, TShaderID, NewImpostor.ImpostorTexture);
		TRenderer_Set_Material_Culling(Renderer, MaterialID, Culling_None);
		NewImpostor.ImpostorMesh = TRenderer_Create_Mesh(Renderer, GeometryID, MaterialID);

		TVector3D_Set(&NewImpostor.Position, rand() % 100, 20 + rand() % 100, rand() % 100);

		TArray_Initialize(&NewImpostor.Particles, sizeof(TParticle));
		TArray_Initialize(&NewImpostor.ParticlePositions, sizeof(TVector3D));
		TArray_Reserve(&NewImpostor.Particles, NumParticles);
		TArray_Reserve(&NewImpostor.ParticlePositions, NumParticles);

		for (ParticleIndex = 0; ParticleIndex < NumParticles; ParticleIndex++) {
			TParticle NewParticle;

			float HorAng = (rand() % 1000) * 0.001 * M_PI * 2;
			float VertAng = (rand() % 1000) * 0.001 * M_PI * 2;
			float Dist = (rand() % 1000) * 0.001 * Radius;

			NewParticle.Position.X = -Dist * cos(HorAng) * sin(VertAng);
			NewParticle.Position.Y = Dist * cos(VertAng);
			NewParticle.Position.Z = Dist * sin(HorAng) * sin(VertAng);

			TArray_Push(&NewImpostor.ParticlePositions, &NewParticle.Position);
			TArray_Push(&NewImpostor.Particles, &NewParticle);
		}

		TArray_Push(&Clouds->Impostors, &NewImpostor);
	}
}

int SortParticles(void* A, void* B) {
	TParticle* ParticleA = ((TParticle*)A);
	TParticle* ParticleB = ((TParticle*)B);
	if (ParticleB->DistanceToCamera > ParticleA->DistanceToCamera) {
		return 1;
	}
	if (ParticleB->DistanceToCamera < ParticleA->DistanceToCamera) {
		return -1;
	}
	return 0;
}

void TClouds_Render(TClouds* Clouds, TRenderer* Renderer) {
	int Index;
	TVector3D Vector;
	TVector3D CameraPos;
	TVector3D CameraDir;
	TTransform Transform, Transform2;
	TCamera OldCamera, ImpostorCamera;

	TRenderer_GetCamera(Renderer, &OldCamera);

	float RerenderAngleThreshold = 1.0 * M_PI / 180.0;
	float RerenderDistThresholdPercent = 0.01;

	for (Index = 0; Index < Clouds->Impostors.Length; Index++) {

		TImpostor* Impostor = TArray_Get(&Clouds->Impostors, Index);

		TTransform_Get_Translation(&OldCamera.InverseViewMatrix, &CameraPos);
		TVector3D_Copy(&Vector, &Impostor->Position);
		TVector3D_Subtract(&Vector, &CameraPos);
		float ImpostorDistance = TVector3D_Length(&Vector);
		bool RenderImpostor = ImpostorDistance > Impostor->Radius * 1.2;

		float RotX = atan2(Vector.Y, ImpostorDistance);
		float RotY = atan2(-Vector.X, -Vector.Z);

		float RerenderDistThreshold = Impostor->PrevDist * RerenderDistThresholdPercent;
		bool RerenderImpostor =
		    RenderImpostor && (fabs(RotY - Impostor->PrevHAngle) > RerenderAngleThreshold || fabs(RotX - Impostor->PrevVAngle) > RerenderAngleThreshold || fabs(ImpostorDistance - Impostor->PrevDist) > RerenderDistThreshold);

		if (!RenderImpostor || RerenderImpostor) {
			TCamera_Initialize(&ImpostorCamera);
			TCamera_LookAtSphere(&ImpostorCamera, &CameraPos, &Impostor->Position, Impostor->Radius);
			TTransform_Get_Forward(&OldCamera.InverseViewMatrix, &CameraDir);

			for (Index = 0; Index < Impostor->Particles.Length; Index++) {
				TParticle* Particle = TArray_Get(&Impostor->Particles, Index);
				TVector3D_Copy(&Vector, &Particle->Position);
				TVector3D_Subtract(&Vector, &CameraPos);
				Particle->DistanceToCamera = -TVector3D_Dot(&Vector, &CameraDir);
			}

			TArray_Sort(&Impostor->Particles, SortParticles);

			for (Index = 0; Index < Impostor->Particles.Length; Index++) {
				TParticle* Particle = TArray_Get(&Impostor->Particles, Index);
				TVector3D* Position = TArray_Get(&Impostor->ParticlePositions, Index);
				TVector3D_Copy(Position, &Particle->Position);
			}

			TRenderer_Update_Mesh_Instanced_Data(Renderer, Clouds->CloudMesh, "ParticlePosition", RenderDataType_Vector3D, Impostor->ParticlePositions.Data, Impostor->Particles.Length);
		}

		if (RerenderImpostor) {
			TTransform_Copy(&Transform, &OldCamera.InverseViewMatrix);
			TTransform_Set_Translation(&Transform, &Impostor->Position);

			TRenderer_Set_Render_Target(Renderer, Impostor->ImpostorTexture);
			TRenderer_UpdateCamera(Renderer, &ImpostorCamera);
			TRenderer_Render_Mesh(Renderer, Clouds->CloudMesh, &Transform);
			TRenderer_UpdateCamera(Renderer, &OldCamera);
			TRenderer_Set_Render_Target(Renderer, 0);

			Impostor->PrevHAngle = RotX;
			Impostor->PrevVAngle = RotY;
			Impostor->PrevDist = ImpostorDistance;
		}

		if (RenderImpostor) {
			float ParticleSize = 2 * tan(asin(Impostor->Radius / Impostor->PrevDist)) * Impostor->PrevDist;
			TTransform_Identity(&Transform2);
			TTransform_Scale_XYZ(&Transform2, ParticleSize, ParticleSize, 0);
			TTransform_Get_Translation(&OldCamera.InverseViewMatrix, &Vector);
			TTransform_PointAt(&Transform, &Impostor->Position, &Vector);
			TTransform_Multiply(&Transform2, &Transform);
			TRenderer_Render_Mesh(Renderer, Impostor->ImpostorMesh, &Transform2);
		} else {
			TTransform_Copy(&Transform, &OldCamera.InverseViewMatrix);
			TTransform_Set_Translation(&Transform, &Impostor->Position);
			TRenderer_Render_Mesh(Renderer, Clouds->CloudMesh, &Transform);
		}
	}
}

void TClouds_Destroy(TClouds* Clouds, TRenderer* Renderer) {
	// TArray_Destroy(&Clouds->Particles);
	// TArray_Destroy(&Clouds->ParticlePositions);
	TRenderer_Release_Mesh(Renderer, Clouds->CloudMesh);
	// TRenderer_Release_Mesh(Renderer, Clouds->ImpostorMesh);
}