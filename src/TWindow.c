#include "TWindow.h"
#include "TVector2D.h"

#define WINVER 0x0502
#include <stdio.h>
#include <windows.h>

void TWindow_Initialize(TWindow* Window, const uint32_t Width, const uint32_t Height) {
	HINSTANCE AppHandle = GetModuleHandle(0);

	WNDCLASS NewWindow;
	// Check to see if class is already registered
	if (!GetClassInfo(AppHandle, "Window", &NewWindow)) {
		NewWindow.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		NewWindow.lpfnWndProc = DefWindowProc;
		NewWindow.cbClsExtra = 0;
		NewWindow.cbWndExtra = 0;
		NewWindow.hInstance = AppHandle;
		NewWindow.hIcon = LoadIcon(0, IDI_APPLICATION);
		NewWindow.hCursor = LoadCursor(0, IDC_ARROW);
		NewWindow.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		NewWindow.lpszMenuName = 0;
		NewWindow.lpszClassName = "Window";

		assert(RegisterClass(&NewWindow));
	} else {
		assert(false);
	}

	int BorderHeight = GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYFIXEDFRAME) * 2;
	int BorderWidth = GetSystemMetrics(SM_CXFIXEDFRAME) * 2;

	HWND WindowHandle = CreateWindow(NewWindow.lpszClassName, "", WS_OVERLAPPED | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, CW_USEDEFAULT, CW_USEDEFAULT, Width + BorderWidth, Height + BorderHeight, 0, 0, AppHandle, 0);
	assert(WindowHandle);

	ShowWindow(WindowHandle, SW_SHOWNORMAL);
	UpdateWindow(WindowHandle);

	Window->WindowHandle = WindowHandle;
	Window->MouseCaptured = false;
	TVector2D_Set(&Window->ScreenSize, Width, Height);

	// nessesary for high precision Sleeping.
	timeBeginPeriod(1);

	RAWINPUTDEVICE RawInputDevice[1];
	RawInputDevice[0].usUsagePage = 0x01;  // Generic Usage HID Page
	RawInputDevice[0].usUsage = 0x02;      // Generic Mouse HID
	RawInputDevice[0].dwFlags = RIDEV_INPUTSINK;
	RawInputDevice[0].hwndTarget = WindowHandle;
	bool Success = RegisterRawInputDevices(RawInputDevice, 1, sizeof(RawInputDevice[0]));
	assert(Success);
}

void TWindow_Update(TWindow* Window, TMessageStream* WindowMessages) {
	MSG Message;
	while (PeekMessage(&Message, Window->WindowHandle, 0, 0, PM_REMOVE)) {
		if (Message.message == WM_MOUSEMOVE) {
			TVector2D NewPosition;
			TVector2D_Set(&NewPosition, LOWORD(Message.lParam), HIWORD(Message.lParam));
			TMessageStream_PutMessage(WindowMessages, WindowMessage_MousePosChange, &NewPosition, sizeof(TVector2D));
		} else if (Message.message == WM_LBUTTONDOWN || Message.message == WM_LBUTTONUP) {
			uint8_t MouseButton = MouseButton_Left;
			TMessageStream_PutMessage(WindowMessages, Message.message == WM_LBUTTONDOWN ? WindowMessage_MouseDown : WindowMessage_MouseUp, &MouseButton, sizeof(MouseButton));
		} else if (Message.message == WM_RBUTTONDOWN || Message.message == WM_RBUTTONUP) {
			uint8_t MouseButton = MouseButton_Right;
			TMessageStream_PutMessage(WindowMessages, Message.message == WM_RBUTTONDOWN ? WindowMessage_MouseDown : WindowMessage_MouseUp, &MouseButton, sizeof(MouseButton));
		} else if (Message.message == WM_MBUTTONDOWN || Message.message == WM_MBUTTONUP) {
			uint8_t MouseButton = MouseButton_Middle;
			TMessageStream_PutMessage(WindowMessages, Message.message == WM_MBUTTONDOWN ? WindowMessage_MouseDown : WindowMessage_MouseUp, &MouseButton, sizeof(MouseButton));
		} else if (Message.message == WM_KEYDOWN || Message.message == WM_KEYUP) {
			uint8_t Key = Key_None;
			if (Message.wParam >= 0x41 && Message.wParam <= 0x5A) {
				Key = Message.wParam;
			} else {
				switch (Message.wParam) {
					case VK_SPACE:
						Key = Key_Space;
						break;
					case VK_ESCAPE:
						Key = Key_Escape;
						break;
					case VK_RETURN:
						Key = Key_Enter;
						break;
					case VK_TAB:
						Key = Key_Tab;
						break;
				}
			}
			if (Key) {
				TMessageStream_PutMessage(WindowMessages, Message.message == WM_KEYDOWN ? WindowMessage_KeyDown : WindowMessage_KeyUp, &Key, sizeof(Key));
			}
		} else if (Message.message == WM_INPUT) {
			RAWINPUT RawInput;
			unsigned int RawInputSize = sizeof(RawInput);
			GetRawInputData((HRAWINPUT)Message.lParam, RID_INPUT, &RawInput, &RawInputSize, sizeof(RAWINPUTHEADER));
			if (RawInput.header.dwType == RIM_TYPEMOUSE) {
				assert((RawInput.data.mouse.usFlags & 0x1) == 0);
				TVector2D Movement;
				TVector2D_Set(&Movement, RawInput.data.mouse.lLastX, RawInput.data.mouse.lLastY);
				TMessageStream_PutMessage(WindowMessages, WindowMessage_MouseMove, &Movement, sizeof(TVector2D));
			}
		}
		// use TranslateMessage?
		DispatchMessage(&Message);
	}

	if (Window->MouseCaptured) {
		RECT WindowRect;
		GetClientRect(Window->WindowHandle, &WindowRect);
		POINT ScreenPos;
		ScreenPos.x = (WindowRect.right - WindowRect.left) / 2;
		ScreenPos.y = (WindowRect.bottom - WindowRect.top) / 2;
		ClientToScreen(Window->WindowHandle, &ScreenPos);
		SetCursorPos(ScreenPos.x, ScreenPos.y);
	}
}

void TWindow_Capture_Mouse(TWindow* Window, bool Capture) {
	// use SetCapture
	Window->MouseCaptured = Capture;
}

void TWindow_Destroy(const TWindow* Window) {
	timeEndPeriod(1);
	DestroyWindow(Window->WindowHandle);
}

/////////////////////////////////////////
// TThread Functions
/////////////////////////////////////////

bool Thread_PumpMessages() {
	MSG Message;
	while (PeekMessage(&Message, (HWND)-1, 0, 0, PM_REMOVE)) {
		if (Message.message == WM_QUIT) {
			return false;
		}
		DispatchMessage(&Message);
	}
	return true;
}

void Thread_Sleep(const uint32_t Milliseconds) {
	Sleep(Milliseconds);
}

/////////////////////////////////////////
// TInputState Functions
/////////////////////////////////////////

void TInputState_Initialize(TInputState* InputState) {
	memset(InputState, 0, sizeof(TInputState));
}

void TInputState_Update(TInputState* InputState, const TMessageStream* WindowMessages) {
	TVector2D_Initialize(&InputState->MouseMove);
	memset(InputState->MousePressed, 0, sizeof(InputState->MousePressed));
	memset(InputState->MouseReleased, 0, sizeof(InputState->MouseReleased));
	memset(InputState->KeyPressed, 0, sizeof(InputState->KeyPressed));
	memset(InputState->KeyReleased, 0, sizeof(InputState->KeyReleased));

	const uint8_t* WindowMessage = 0;
	while ((WindowMessage = TMessageStream_NextMessage(WindowMessages, WindowMessage))) {
		if (*WindowMessage == WindowMessage_MouseMove) {
			TVector2D_Add(&InputState->MouseMove, TMessage_GetData(WindowMessage));
		} else if (*WindowMessage == WindowMessage_MousePosChange) {
			TVector2D_Copy(&InputState->MousePos, TMessage_GetData(WindowMessage));
		} else if (*WindowMessage == WindowMessage_KeyDown) {
			uint8_t Key = *(uint8_t*)TMessage_GetData(WindowMessage);
			InputState->KeyDown[Key] = true;
			InputState->KeyPressed[Key] = true;
		} else if (*WindowMessage == WindowMessage_KeyUp) {
			uint8_t Key = *(uint8_t*)TMessage_GetData(WindowMessage);
			InputState->KeyDown[Key] = false;
			InputState->KeyReleased[Key] = true;
		} else if (*WindowMessage == WindowMessage_MouseDown) {
			uint8_t MouseButton = *(uint8_t*)TMessage_GetData(WindowMessage);
			InputState->MouseDown[MouseButton] = true;
			InputState->MousePressed[MouseButton] = true;
		} else if (*WindowMessage == WindowMessage_MouseUp) {
			uint8_t MouseButton = *(uint8_t*)TMessage_GetData(WindowMessage);
			InputState->MouseDown[MouseButton] = false;
			InputState->MouseReleased[MouseButton] = true;
		}
	}
}
