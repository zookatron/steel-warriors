#include "TTimer.h"
#include "windows.h"

void TTimer_Initialize(TTimer* Timer) {
	Timer->StartTime = Timer->IntervalTime = Timer->Frequency = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&Timer->Frequency);
	QueryPerformanceCounter((LARGE_INTEGER*)&Timer->IntervalTime);
	Timer->StartTime = Timer->IntervalTime;
}

void TTimer_Restart(TTimer* Timer) {
	QueryPerformanceCounter((LARGE_INTEGER*)&Timer->StartTime);
}

float TTimer_GetTime(TTimer* Timer) {
	long long CurrentTime = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&CurrentTime);
	return (CurrentTime - Timer->StartTime) / (float)Timer->Frequency;
}

float TTimer_Interval(TTimer* Timer) {
	long long CurrentTime = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&CurrentTime);
	float Elapsed = (CurrentTime - Timer->IntervalTime) / (float)Timer->Frequency;
	Timer->IntervalTime = CurrentTime;
	return Elapsed;
}
