#ifndef Hashing_H
#define Hashing_H

typedef unsigned int TStringHash;

TStringHash HashString(const char* String);

#endif