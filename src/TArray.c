#include "TArray.h"
#include <string.h>

void TArray_Initialize(TArray* Array, const size_t ElementSize) {
	Array->ElementSize = ElementSize;
	Array->Length = Array->DataSize = 0;
	Array->Data = 0;
}
void TArray_Destroy(TArray* Array) {
	free(Array->Data);
	Array->Length = Array->DataSize = Array->ElementSize = 0;
	Array->Data = 0;
}

void TArray_Copy(TArray* Dest, TArray* Src) {
	if (Dest == Src) {
		return;
	}
	Dest->ElementSize = Src->ElementSize;
	Dest->Length = Src->Length;
	Dest->DataSize = Src->DataSize;
	Dest->Data = malloc(Src->DataSize);
	memcpy(Dest->Data, Src->Data, Src->DataSize);
}

void TArray_CopyData(TArray* Array, void* Elements, size_t Length) {
	TArray_Reserve(Array, Array->Length + Length);
	memcpy(Array->Data + Array->Length * Array->ElementSize, Elements, Array->ElementSize * Length);
	Array->Length += Length;
}

void TArray_Append(TArray* Dest, TArray* Src) {
	TArray_Reserve(Dest, Dest->Length + Src->Length);
	memcpy(Dest->Data + Dest->Length * Dest->ElementSize, Src->Data, Src->ElementSize * Src->Length);
	Dest->Length += Src->Length;
}

void* TArray_Get(const TArray* Array, const size_t Element) {
	if (Element >= Array->Length) {
		assert(Element < Array->Length);
	}
	return Array->Data + Array->ElementSize * Element;
}

void* TArray_Pop(TArray* Array) {
	assert(Array->Length > 0);
	return Array->Data + Array->ElementSize * --Array->Length;
}

void TArray_Reserve(TArray* Array, size_t Length) {
	if (Length * Array->ElementSize > Array->DataSize) {
		Array->DataSize = max(Array->DataSize << 1, Length * Array->ElementSize);
		Array->Data = realloc(Array->Data, Array->DataSize);
	}
}

void TArray_Push(TArray* Array, void* Element) {
	TArray_Reserve(Array, Array->Length + 1);
	if (Element) {
		memcpy(Array->Data + Array->Length * Array->ElementSize, Element, Array->ElementSize);
	}
	Array->Length++;
}

void TArray_Remove(TArray* Array, size_t Element) {
	void* Dest = Array->Data + Element * Array->ElementSize;
	void* Src = Array->Data + (Element + 1) * Array->ElementSize;
	void* End = Array->Data + Array->Length * Array->ElementSize;
	memcpy(Dest, Src, End - Src);
	Array->Length--;
}

void TArray_Empty(TArray* Array) {
	Array->Length = 0;
}

void SwapMemory(void* A, void* B, int Size) {
	char Temp[Size];
	memcpy(Temp, A, Size);
	memcpy(A, B, Size);
	memcpy(B, Temp, Size);
}

void TArray_Sort(TArray* Array, int (*Comparator)(void*, void*)) {
	// Create an auxiliary stack
	int Stack[Array->Length];

	int Start = 0;
	int End = Array->Length - 1;

	// initialize top of stack
	int Top = -1;

	// push initial values of start and end to stack
	Stack[++Top] = Start;
	Stack[++Top] = End;

	// Keep popping from stack while is not empty
	while (Top >= 0) {
		// Pop end and start
		End = Stack[Top--];
		Start = Stack[Top--];

		// Set pivot element at its correct position in sorted array
		void* PivotValue = Array->Data + End * Array->ElementSize;
		int PivotIndex = (Start - 1);

		int Index;
		for (Index = Start; Index <= End - 1; Index++) {
			if (Comparator(Array->Data + Index * Array->ElementSize, PivotValue) != 1) {
				PivotIndex++;
				if (Index != PivotIndex) {
					SwapMemory(Array->Data + PivotIndex * Array->ElementSize, Array->Data + Index * Array->ElementSize, Array->ElementSize);
				}
			}
		}
		SwapMemory(Array->Data + (PivotIndex + 1) * Array->ElementSize, Array->Data + End * Array->ElementSize, Array->ElementSize);
		int Pivot = (PivotIndex + 1);

		// If there are elements on left side of pivot, then push left
		// side to stack
		if (Pivot - 1 > Start) {
			Stack[++Top] = Start;
			Stack[++Top] = Pivot - 1;
		}

		// If there are elements on right side of pivot, then push right
		// side to stack
		if (Pivot + 1 < End) {
			Stack[++Top] = Pivot + 1;
			Stack[++Top] = End;
		}
	}
}
