#include "TShip.h"
#include "TProjectile.h"
#include "TWindow.h"

#include <math.h>
#include <string.h>

void TShip_Initialize(TShip* Ship, TGameState* GameState) {
	memset(Ship, 0, sizeof(TShip));
	TTransform_Identity(&Ship->Position.Transform);
	TShipInput_Initialize(&Ship->Input);

	TMeshID ChasseMeshID = TResourceManager_GetMesh(&GameState->ResourceManager, "Vehicles/Falcon/Chasse");
	TMeshID WingMeshID = TResourceManager_GetMesh(&GameState->ResourceManager, "Vehicles/Falcon/Wing");

	Ship->SceneNodeID = TRenderScene_Add(&GameState->RenderScene, 0, ChasseMeshID);
	TRenderScene_Add(&GameState->RenderScene, Ship->SceneNodeID, WingMeshID);
	TRenderSceneNodeID TempNodeID = TRenderScene_Add(&GameState->RenderScene, Ship->SceneNodeID, WingMeshID);
	TTransform_Rotate_Y(TRenderScene_GetLocalTransform(&GameState->RenderScene, TempNodeID), 3.14f);
}

void TShip_Destroy(TShip* Ship, TGameState* GameState) {
	TRenderScene_Remove(&GameState->RenderScene, Ship->SceneNodeID);
	Ship->SceneNodeID = 0;
}

void TShipInput_Initialize(TShipInput* ShipInput) {
	memset(ShipInput, 0, sizeof(TShipInput));
}

void GetShipInput(const TInputState* InputState, const TShip* Ship, TShipInput* ShipInput) {
	TVector3D Angles;
	TVector3D_Set(&Angles, -InputState->MouseMove.Y * 0.003f, -InputState->MouseMove.X * 0.003f, 0.0f);
	TVector3D_Add(&ShipInput->LookInput, &Angles);

	TTransform Transform;
	TVector3D Empty;
	TTransform_Copy(&Transform, &Ship->Position.Transform);
	TVector3D_Initialize(&Empty);
	TTransform_Set_Translation(&Transform, &Empty);

	float MoveX = (InputState->KeyDown[Key_A] ? -1.0f : 0.0f) + (InputState->KeyDown[Key_D] ? 1.0f : 0.0f);
	float MoveY = (InputState->KeyDown[Key_Q] ? -1.0f : 0.0f) + (InputState->KeyDown[Key_E] ? 1.0f : 0.0f);
	float MoveZ = (InputState->KeyDown[Key_W] ? -1.0f : 0.0f) + (InputState->KeyDown[Key_S] ? 1.0f : 0.0f);
	TVector3D_Set(&ShipInput->MoveInput, MoveX, MoveY, MoveZ);
	TVector3D_Transform(&ShipInput->MoveInput, &Transform);

	ShipInput->PrimaryFiring = InputState->MouseDown[MouseButton_Left];
	ShipInput->AltFiring = InputState->MouseDown[MouseButton_Left];
}

float GetAngularAccel(const float Value, const float Target, const float Velocity, const float MaxAccel, const float Delta) {
	// if we are already very close to the target and our velocity is very low
	if (fabs(Target - Value) < MaxAccel * Delta * Delta && fabs(Velocity) < MaxAccel * Delta) {
		// if our velocity is non-zero
		if (fabs(Velocity) > 0.0000001f) {
			// get rid of our velocity
			return -Velocity / Delta;
		}
		// otherwise, step the distance to the target
		return (Target - Value) / (Delta * Delta);
	}

	// D is the distance from the target angle that we will be at if we get rid of all our velocity in steps of MaxAccel
	float D = (Target - Value) - sign(Velocity) * Velocity * Velocity / (2 * MaxAccel);

	// if D is less the the maximum closeness we can guarantee at this velocity
	if (fabs(D) < fabs(2.0 * Velocity * Delta + MaxAccel * Delta * Delta)) {
		// start getting rid of our velocity in steps of MaxAccel
		return -sign(Velocity) * MaxAccel;
	}
	// otherwise, try to get closer to the target angle
	return sign(D) * MaxAccel;
}

void UpdateShipPosition(TShipPosition* Ship, const TShipInput* Input, const float Delta) {
	TVector3D Accel, PositionChange, VelocityChange, AngleAccel, AngleChange, AngularVelocityChange, Temp, MaxAccel;
	float MaxAccelScalar = 400.0f;
	float Friction = 2.0f;
	TVector3D_Set(&MaxAccel, MaxAccelScalar, MaxAccelScalar, MaxAccelScalar);

	// TVector3D Accel = Input->MoveInput*MaxAccel;
	TVector3D_Copy(&Accel, &Input->MoveInput);
	TVector3D_Multiply(&Accel, &MaxAccel);

	// TVector3D PositionChange = Velocity*Delta + Accel*(0.5f*Delta*Delta);
	TVector3D_Copy(&PositionChange, &Ship->Velocity);
	TVector3D_Scale(&PositionChange, Delta);
	TVector3D_Copy(&Temp, &Accel);
	TVector3D_Scale(&Temp, 0.5f * Delta * Delta);
	TVector3D_Add(&PositionChange, &Temp);

	// TVector3D VelocityChange = Accel*Delta - Velocity*(1-pow(2.72, -Friction*Delta));
	TVector3D_Copy(&VelocityChange, &Accel);
	TVector3D_Scale(&VelocityChange, Delta);
	TVector3D_Copy(&Temp, &Ship->Velocity);
	TVector3D_Scale(&Temp, 1 - pow(2.72, -Friction * Delta));
	TVector3D_Subtract(&VelocityChange, &Temp);

	float MaxAngleAccel = 5.0f;
	TVector3D_Set(&AngleAccel,
	              GetAngularAccel(Ship->Angle.X, Input->LookInput.X, Ship->AngularVelocity.X, MaxAngleAccel, Delta),
	              GetAngularAccel(Ship->Angle.Y, Input->LookInput.Y, Ship->AngularVelocity.Y, MaxAngleAccel, Delta),
	              GetAngularAccel(Ship->Angle.Z, Input->LookInput.Z, Ship->AngularVelocity.Z, MaxAngleAccel, Delta));

	// TVector3D AngleChange = AngularVelocity*Delta + AngleAccel*(0.5f*Delta*Delta);
	TVector3D_Copy(&AngleChange, &Ship->AngularVelocity);
	TVector3D_Scale(&AngleChange, Delta);
	TVector3D_Copy(&Temp, &AngleAccel);
	TVector3D_Scale(&Temp, 0.5f * Delta * Delta);
	TVector3D_Add(&AngleChange, &Temp);

	// TVector3D AngularVelocityChange = AngleAccel*Delta;
	TVector3D_Copy(&AngularVelocityChange, &AngleAccel);
	TVector3D_Scale(&AngularVelocityChange, Delta);

	TVector3D_Add(&Ship->Position, &PositionChange);
	TVector3D_Add(&Ship->Velocity, &VelocityChange);
	TVector3D_Add(&Ship->Angle, &AngleChange);
	TVector3D_Add(&Ship->AngularVelocity, &AngularVelocityChange);

	TTransform_Identity(&Ship->Transform);
	TTransform_Rotate(&Ship->Transform, &Ship->Angle);
	TTransform_Translate(&Ship->Transform, &Ship->Position);
}

void UpdateShipSceneNode(TRenderSceneNodeID SceneNodeID, TRenderScene* Scene, TShipPosition* ShipPosition) {
	TTransform_Copy(TRenderScene_GetLocalTransform(Scene, SceneNodeID), &ShipPosition->Transform);
}

void PerformShipActions(TShip* Ship, float Delta, TGameState* GameState) {
	if (Delta) {
	}

	if (!GameState->Authority) {
		return;
	}

	if (Ship->Input.PrimaryFiring) {
		TGameState_CreateProjectile(GameState, &Ship->Position.Transform);
	}
}