#ifndef TTerrain_H
#define TTerrain_H

#include "TRenderer.h"

typedef struct {
	TMeshID Grid;
	TMeshID InnerFixup;
	TMeshID RingFixup;
	TMeshID RingLXPiece;
	TMeshID RingLYPiece;
	TMeshID DegenerateTris;
	TMeshID SkySphere;
	TMaterialID MaterialID;
	TMaterialID SkySphereMaterialID;
	int GridWidth;
	int GridSize;
	int MinRingLevel;
	int MaxRingLevel;
} TTerrain;

void TTerrain_Initialize(TTerrain* Terrain, TRenderer* Renderer);
void TTerrain_Render(TTerrain* Terrain, TRenderer* Renderer);
void TTerrain_Destroy(TTerrain* Terrain, TRenderer* Renderer);

#endif