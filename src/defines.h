#ifndef Defines_H
#define Defines_H

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef bool
typedef char bool;
#define true 1
#define false 0
#endif

#ifndef sign
#define sign(x) ((x) == 0 ? 0 : (x) > 0 ? 1 : -1)
#endif

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef clamp
#define clamp(a,b,c) (((a) > (c)) ? (c) : (((a) < (b)) ? (b) : (a)))
#endif

#endif
