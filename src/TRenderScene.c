#include "TRenderScene.h"

typedef struct {
	TRenderSceneNodeID ParentID;
	TMeshID MeshID;
	TTransform LocalTransform;
	TTransform WorldTransform;
} TRenderSceneNode;

void TRenderScene_Initialize(TRenderScene* Scene) {
	TObjectManager_Initialize(&Scene->Nodes, sizeof(TRenderSceneNode));
}

void TRenderScene_Destroy(TRenderScene* Scene) {
	TObjectManager_Destroy(&Scene->Nodes);
}

TRenderSceneNodeID TRenderScene_Add(TRenderScene* Scene, TRenderSceneNodeID ParentID, TMeshID MeshID) {
	TRenderSceneNode SceneNode;

	SceneNode.ParentID = ParentID;
	SceneNode.MeshID = MeshID;
	TTransform_Identity(&SceneNode.LocalTransform);
	TTransform_Identity(&SceneNode.WorldTransform);

	return TObjectManager_Add(&Scene->Nodes, &SceneNode);
}

void TRenderScene_Remove(TRenderScene* Scene, TRenderSceneNodeID NodeID) {
	TObjectManager_Remove(&Scene->Nodes, NodeID);

	TRenderSceneNode* SceneNode = 0;
	while ((SceneNode = TObjectManager_Next(&Scene->Nodes, SceneNode))) {
		if (SceneNode->ParentID == NodeID) {
			TRenderScene_Remove(Scene, TObjectManager_GetID(&Scene->Nodes, SceneNode));
		}
	}
}

TTransform* TRenderScene_GetLocalTransform(TRenderScene* Scene, TRenderSceneNodeID NodeID) {
	TRenderSceneNode* SceneNode = TObjectManager_Get(&Scene->Nodes, NodeID);
	assert(SceneNode);
	return &SceneNode->LocalTransform;
}

TTransform* TRenderScene_GetWorldTransform(TRenderScene* Scene, TRenderSceneNodeID NodeID) {
	TRenderSceneNode* SceneNode = TObjectManager_Get(&Scene->Nodes, NodeID);
	assert(SceneNode);
	return &SceneNode->WorldTransform;
}

void TRenderScene_UpdateWorldTransforms(TRenderScene* Scene) {
	TRenderSceneNode* ParentNode;
	TTransform Temp;

	// TODO: needs to update properly if parent is after child!
	TRenderSceneNode* SceneNode = 0;
	while ((SceneNode = TObjectManager_Next(&Scene->Nodes, SceneNode))) {
		if (!SceneNode->ParentID) {
			TTransform_Copy(&SceneNode->WorldTransform, &SceneNode->LocalTransform);
		} else {
			ParentNode = TObjectManager_Get(&Scene->Nodes, SceneNode->ParentID);
			TTransform_Copy(&Temp, &SceneNode->LocalTransform);
			TTransform_Multiply(&Temp, &ParentNode->WorldTransform);
			TTransform_Copy(&SceneNode->WorldTransform, &Temp);
		}
	}
}

void TRenderScene_Render(TRenderScene* Scene, TRenderer* Renderer) {
	TRenderSceneNode* SceneNode = 0;
	while ((SceneNode = TObjectManager_Next(&Scene->Nodes, SceneNode))) {
		TRenderer_Render_Mesh(Renderer, SceneNode->MeshID, &SceneNode->WorldTransform);
	}
}
