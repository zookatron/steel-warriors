#include "TBuffer.h"
#include <stdio.h>
#include <string.h>

void TBuffer_Initialize(TBuffer* Buffer) {
	Buffer->UsedData = Buffer->DataSize = 0;
	Buffer->Data = 0;
}

void TBuffer_Destroy(TBuffer* Buffer) {
	free(Buffer->Data);
	Buffer->UsedData = Buffer->DataSize = 0;
	Buffer->Data = 0;
}

void TBuffer_Reserve(TBuffer* Buffer, const size_t Amount) {
	if (Amount > Buffer->DataSize) {
		Buffer->DataSize = Buffer->DataSize << 1 > Amount ? Buffer->DataSize << 1 : Amount;
		Buffer->Data = realloc(Buffer->Data, Buffer->DataSize);
	}
}

void TBuffer_Add(TBuffer* Buffer, const size_t Amount, void* Data) {
	TBuffer_Reserve(Buffer, Buffer->UsedData + Amount);
	memcpy(Buffer->Data + Buffer->UsedData, Data, Amount);
	Buffer->UsedData += Amount;
}

void TBuffer_Remove(TBuffer* Buffer, const size_t Amount) {
	assert(Buffer->UsedData > Amount);
	Buffer->UsedData -= Amount;
}

void TBuffer_Empty(TBuffer* Buffer) {
	Buffer->UsedData = 0;
}

void TBuffer_ReadFile(TBuffer* FileBuffer, const char* FilePath) {
	FILE* File;
	File = fopen(FilePath, "r");
	if (!File) {
		printf("Cannot open file: %s\n", FilePath);
	}
	assert(File);

	char CurrentLine[128];
	memset(CurrentLine, 0, sizeof(CurrentLine));

	while (fgets(CurrentLine, 128, File)) {
		TBuffer_Add(FileBuffer, strlen(CurrentLine), CurrentLine);
	}

	CurrentLine[0] = 0;
	TBuffer_Add(FileBuffer, 1, CurrentLine);

	fclose(File);
}