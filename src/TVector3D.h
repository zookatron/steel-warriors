#ifndef TVector3D_H
#define TVector3D_H

#include "defines.h"

typedef struct {
	float X;
	float Y;
	float Z;
} TVector3D;

void TVector3D_Copy(TVector3D* Dest, const TVector3D* Src);
void TVector3D_Initialize(TVector3D* Vector);
void TVector3D_Set(TVector3D* Vector, float X, float Y, float Z);
bool TVector3D_Equals(const TVector3D* First, const TVector3D* Second);
void TVector3D_Add(TVector3D* Vector, const TVector3D* Other);
void TVector3D_Subtract(TVector3D* Vector, const TVector3D* Other);
void TVector3D_Multiply(TVector3D* Vector, const TVector3D* Other);
void TVector3D_Scale(TVector3D* Vector, const float Factor);
void TVector3D_Normalize(TVector3D* Vector);
void TVector3D_Rotate(TVector3D* Vector, const TVector3D* Angles);
void TVector3D_Rotate_X(TVector3D* Vector, const float Angle);
void TVector3D_Rotate_Y(TVector3D* Vector, const float Angle);
void TVector3D_Rotate_Z(TVector3D* Vector, const float Angle);
float TVector3D_Dot(const TVector3D* First, const TVector3D* Second);
float TVector3D_Length(const TVector3D* Vector);
float TVector3D_LengthSquared(const TVector3D* Vector);

#endif
