<img alt="Steel Warriors" src="screenshots/screenshot.png" width="200px">

# Steel Warriors

Steel Warriors is a fast-paced third person action game. I originally programmed it in C++/DirectX, and I started porting it to C/OpenGL.

![First Gameplay Screenshot](screenshots/screenshot1.jpg "First Gameplay Screenshot")
![Second Gameplay Screenshot](screenshots/screenshot2.jpg "Second Gameplay Screenshot")
![Third Gameplay Screenshot](screenshots/screenshot3.jpg "Third Gameplay Screenshot")
